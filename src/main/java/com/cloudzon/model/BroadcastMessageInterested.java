package com.cloudzon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flyrr_broadcast_message_interested")
public class BroadcastMessageInterested extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = BroadcastMessage.class)
	@JoinColumn(name = "broadcast_id", nullable = false)
	private BroadcastMessage broadcastMessage;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
	@JoinColumn(name = "user_id", nullable = true)
	private User user;

	@Column(name = "active", columnDefinition = "BIT DEFAULT TRUE", length = 1, nullable = false)
	private Boolean isActive;

	@Column(name = "is_interested", columnDefinition = "BIT DEFAULT FALSE", length = 1, nullable = true)
	private Boolean isInterested;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BroadcastMessage getBroadcastMessage() {
		return broadcastMessage;
	}

	public void setBroadcastMessage(BroadcastMessage broadcastMessage) {
		this.broadcastMessage = broadcastMessage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsInterested() {
		return isInterested;
	}

	public void setIsInterested(Boolean isInterested) {
		this.isInterested = isInterested;
	}

}

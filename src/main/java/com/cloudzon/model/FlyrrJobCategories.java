package com.cloudzon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flyrr_job_categories")
public class FlyrrJobCategories extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = BroadcastMessage.class)
	@JoinColumn(name = "broadcast_message_id", nullable = false)
	private BroadcastMessage broadcastMessage;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Categories.class)
	@JoinColumn(name = "categorie_id", nullable = false)
	private Categories jobCategory;

	@Column(name = "active", columnDefinition = "BIT DEFAULT 1", length = 1, nullable = false)
	private Boolean isActive;

	public BroadcastMessage getBroadcastMessage() {
		return broadcastMessage;
	}

	public void setBroadcastMessage(BroadcastMessage broadcastMessage) {
		this.broadcastMessage = broadcastMessage;
	}

	public Categories getJobCategory() {
		return jobCategory;
	}

	public void setJobCategory(Categories jobCategory) {
		this.jobCategory = jobCategory;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}

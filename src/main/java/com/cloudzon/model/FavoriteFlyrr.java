package com.cloudzon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flyrr_favorite")
public class FavoriteFlyrr extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@JoinColumn(name = "broadcast_id", nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = BroadcastMessage.class)
	private BroadcastMessage broadcastMessage;

	@JoinColumn(name = "user_id", nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
	private User user;

	@Column(name = "is_favorite", columnDefinition = "BIT DEFAULT 1", length = 1, nullable = false)
	private Boolean isFavorite;

	@Column(name = "active", columnDefinition = "BIT DEFAULT 1", length = 1, nullable = false)
	private Boolean isActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BroadcastMessage getBroadcastMessage() {
		return broadcastMessage;
	}

	public void setBroadcastMessage(BroadcastMessage broadcastMessage) {
		this.broadcastMessage = broadcastMessage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}

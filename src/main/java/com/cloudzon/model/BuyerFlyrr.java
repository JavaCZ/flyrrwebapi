package com.cloudzon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flyrr_buyer")
public class BuyerFlyrr extends BaseEntity {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@JoinColumn(name = "broadcast_id", nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = BroadcastMessage.class)
	private BroadcastMessage broadcastMessage;

	@JoinColumn(name = "user_id", nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
	private User user;

	@Column(name = "active", columnDefinition = "BIT DEFAULT 1", length = 1, nullable = false)
	private Boolean isActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BroadcastMessage getBroadcastMessage() {
		return broadcastMessage;
	}

	public void setBroadcastMessage(BroadcastMessage broadcastMessage) {
		this.broadcastMessage = broadcastMessage;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}

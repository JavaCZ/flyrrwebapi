package com.cloudzon.model;

public enum NotificationTypes {
	ChatNotification(0), PromotionalNotification(1);

	private int numVal;

	NotificationTypes(int numVal) {
		this.numVal = numVal;
	}

	public int getNumVal() {
		return numVal;
	}
}
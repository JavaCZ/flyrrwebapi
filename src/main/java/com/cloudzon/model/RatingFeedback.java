package com.cloudzon.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flyrr_rating_feedback")
public class RatingFeedback {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "feedback", columnDefinition = "TEXT")
	private String feedback;

	@Column(name = "rate")
	private Integer rate;

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
	@JoinColumn(name = "rating_by_user", nullable = false)
	private User ratingByUser;

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
	@JoinColumn(name = "rating_to_user", nullable = false)
	private User ratingToUser;

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = BroadcastMessage.class)
	@JoinColumn(name = "broadcast_id", nullable = false)
	private BroadcastMessage broadcastMessage;

	@Column(name = "created_date")
	private Date createdDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public User getRatingByUser() {
		return ratingByUser;
	}

	public void setRatingByUser(User ratingByUser) {
		this.ratingByUser = ratingByUser;
	}

	public User getRatingToUser() {
		return ratingToUser;
	}

	public void setRatingToUser(User ratingToUser) {
		this.ratingToUser = ratingToUser;
	}

	public BroadcastMessage getBroadcastMessage() {
		return broadcastMessage;
	}

	public void setBroadcastMessage(BroadcastMessage broadcastMessage) {
		this.broadcastMessage = broadcastMessage;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}

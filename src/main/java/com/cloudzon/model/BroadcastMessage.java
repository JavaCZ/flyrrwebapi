package com.cloudzon.model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name = "flyrr_broadcast_message")
public class BroadcastMessage extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "title")
	private String title;

	@Column(name = "description")
	private String description;

	@Column(name = "price")
	private Double price;

	@Column(name = "lattitude")
	private String lattitude;

	@Column(name = "longitude")
	private String longitude;

	@Column(name = "expire_in")
	private Date expireIn;

	@Column(name = "negotiable")
	private Boolean isNegotiable;

	@Column(name = "active", columnDefinition = "BIT DEFAULT true ", length = 1, nullable = false)
	private Boolean isActive;

	@Column(name = "is_delete", columnDefinition = "BIT DEFAULT false ", length = 1, nullable = false)
	private Boolean isDelete;

	@Column(name = "is_buying", columnDefinition = "BIT DEFAULT false ", length = 1, nullable = false)
	private Boolean isBuying;

	/*
	 * @Transient
	 * 
	 * @ManyToOne(fetch = FetchType.EAGER, targetEntity = Categories.class)
	 * 
	 * @JoinColumn(name = "categories_id", nullable = true) private List<String>
	 * categories;
	 */

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = User.class)
	@JoinColumn(name = "user_id", nullable = false)
	private User user;

	// get set method 21-11-2018
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*
	 * public List<MultipartFile> getImages() { return images; }
	 * 
	 * public void setImages(List<MultipartFile> images) { this.images = images; }
	 */

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Date getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(Date expireIn) {
		this.expireIn = expireIn;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Boolean getIsNegotiable() {
		return isNegotiable;
	}

	public void setIsNegotiable(Boolean isNegotiable) {
		this.isNegotiable = isNegotiable;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Boolean getIsBuying() {
		return isBuying;
	}

	public void setIsBuying(Boolean isBuying) {
		this.isBuying = isBuying;
	}

}

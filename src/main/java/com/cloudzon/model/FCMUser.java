package com.cloudzon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "flyrr_fcm_user")
public class FCMUser extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "device_type")
	private String deviceType;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = DeviceToken.class)
	@JoinColumn(name = "device_id", nullable = false)
	private DeviceToken deviceToken;

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = User.class)
	@JoinColumn(name = "user_id", nullable = true)
	private User user;

	@Column(name = "is_active", columnDefinition = "BIT DEFAULT TRUE", length = 1, nullable = false)
	private Boolean isActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public DeviceToken getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(DeviceToken deviceToken) {
		this.deviceToken = deviceToken;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}

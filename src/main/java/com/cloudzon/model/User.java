package com.cloudzon.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "flyrr_user")
@DynamicUpdate
@DynamicInsert
public class User extends BaseEntity {

	private static final long serialVersionUID = 9203227496421303181L;

	@Id
	@Column(name = "user_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "user_name", nullable = false, length = 100)
	private String userName;

	@Column(name = "mobile_number", length = 15, unique = true)
	private String mobileNumber;

	@Column(name = "full_name", length = 50, nullable = true)
	private String fullName;

	@Column(name = "rate", columnDefinition = " DOUBLE DEFAULT 0.0 ", length = 1, nullable = false)
	private Double rate;

	@Column(name = "profile_picture ", length = 100)
	private String profilePicture;

	@Column(name = "is_verified", columnDefinition = "BIT DEFAULT false", length = 1, nullable = false)
	private Boolean isVerified;

	@Column(name = "is_mobile_verified", columnDefinition = "BIT DEFAULT false", length = 1, nullable = true)
	private Boolean isMobileVerified;

	@Column(name = "active", columnDefinition = "BIT DEFAULT true", length = 1, nullable = false)
	private Boolean isActive;

	@Column(name = "bank_name", length = 50)
	private String bankName;

	@Column(name = "bank_account_number", length = 20, unique = true)
	private String bankAccountNumber;

	@Column(name = "card_type")
	private String cardType;

	@Column(name = "lattitude", length = 250)
	private String lattitude;

	@Column(name = "longitude", length = 250)
	private String longitude;

	@Column(name = "dialCode")
	private String dialCode;

	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "osversion")
	private String oSVersion;

	@Column(name = "device_model")
	private String deviceModel;

	@Column(name = "is_complete_profile", columnDefinition = "BIT DEFAULT 0", length = 1, nullable = true)
	private Boolean isCompleteProfile;

	@Column(name = "is_enable_notification", columnDefinition = "BIT DEFAULT true", length = 1, nullable = false)
	private Boolean isEnableNotification;

	@Column(name = "is_xmpp_user", columnDefinition = "BIT DEFAULT false", length = 1, nullable = false)
	private Boolean isXmppUser;

	public Boolean getIsEnableNotification() {
		return isEnableNotification;
	}

	public void setIsEnableNotification(Boolean isEnableNotification) {
		this.isEnableNotification = isEnableNotification;
	}

	/*
	 * @JsonIgnore
	 * 
	 * @ManyToOne(fetch = FetchType.EAGER, targetEntity = Country.class)
	 * 
	 * @JoinColumn(name = "country_id", nullable = true) private Country country;
	 */
	private String accountNumber;

	// get set method

	public Boolean getIsCompleteProfile() {
		return isCompleteProfile;
	}

	public void setIsCompleteProfile(Boolean isCompleteProfile) {
		this.isCompleteProfile = isCompleteProfile;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	// public Country getCountry() {
	// return country;
	// }
	//
	// public void setCountry(Country country) {
	// this.country = country;
	// }

	public String getFullName() {
		return fullName;
	}

	public Boolean getIsXmppUser() {
		return isXmppUser;
	}

	public void setIsXmppUser(Boolean isXmppUser) {
		this.isXmppUser = isXmppUser;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Boolean getIsMobileVerified() {
		return isMobileVerified;
	}

	public void setIsMobileVerified(Boolean isMobileVerified) {
		this.isMobileVerified = isMobileVerified;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDialCode() {
		return dialCode;
	}

	public void setDialCode(String dialCode) {
		this.dialCode = dialCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getoSVersion() {
		return oSVersion;
	}

	public void setoSVersion(String oSVersion) {
		this.oSVersion = oSVersion;
	}

}

package com.cloudzon.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.entity.GzipCompressingEntity;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


public class Http401UnauthorizedEntryPoint implements AuthenticationEntryPoint {

	private final Logger log = LoggerFactory.getLogger(Http401UnauthorizedEntryPoint.class);

	/**
	 * Always returns a 401 error code to the client.
	 */
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException arg2)
			throws IOException, ServletException {

	
		log.debug("Pre-authenticated entry point called. Rejecting access");
		
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String json = "{\n" + 
				"    \"response\": {\n" + 
				"        \"errorMessage\": \"Unauthorized\",\n" + 
				"        \"errorCode\": \"401\",\n" + 
				"        \"developerMessage\": \"Login Again\"\n" + 
				"    },\n" + 
				"    \"isSuccess\": false,\n" + 
				"    \"showMessage\": true\n" + 
				"}";
		response.getOutputStream().println(json);
		
	}
}

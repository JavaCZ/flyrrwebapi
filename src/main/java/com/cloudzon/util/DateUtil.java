package com.cloudzon.util;

import java.time.LocalTime;
import java.time.Month;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Hours;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Seconds;
import org.joda.time.Weeks;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtil {

	public static Date getCurrentDate() {
		return new Date();
	}

	public static Date getDate(String date, String dateFormat) {
		DateTimeFormatter objDateFormat = null;
		LocalDate objLocalDate = null;
		Date objDate = null;
		try {
			objDateFormat = DateTimeFormat.forPattern(dateFormat);
			objLocalDate = LocalDate.parse(date, objDateFormat);
			objDate = objLocalDate.toDate();
		} catch (Exception exception) {
			objDate = null;
		} finally {
			objDateFormat = null;
			objLocalDate = null;
		}
		return objDate;
	}

	public static String getDate(Date date, String dateFormat) {
		DateTimeFormatter objDateFormat = null;
		DateTime objDateTime = null;
		String objFormateDate = null;
		try {
			objDateFormat = DateTimeFormat.forPattern(dateFormat);
			objDateTime = new DateTime(date);
			objFormateDate = objDateFormat.print(objDateTime);
		} catch (Exception exception) {
			objFormateDate = null;
		} finally {
			objDateFormat = null;
			objDateTime = null;
		}
		return objFormateDate;
	}

	public static Date getAddedDate(int seconds, int minutes, int hours) {
		DateTime objDateTime = null;
		objDateTime = new DateTime();
		objDateTime.plusSeconds(seconds);
		objDateTime.plusMinutes(minutes);
		objDateTime.plusHours(hours);
		return objDateTime.toDate();
	}

	public static Date getAddedDate(int hours) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR, hours);
		Date dateBeforeDays = cal.getTime();
		return dateBeforeDays;
	}

	public static Date currentDateWithoutTime() {
		Date currentDate = null;
		Calendar newDate = null;
		try {
			currentDate = new Date();
			newDate = Calendar.getInstance();
			newDate.setLenient(false);
			newDate.setTime(currentDate);
			newDate.set(Calendar.HOUR_OF_DAY, 0);
			newDate.set(Calendar.MINUTE, 0);
			newDate.set(Calendar.SECOND, 0);
			newDate.set(Calendar.MILLISECOND, 0);
			currentDate = newDate.getTime();
		} finally {

		}
		return currentDate;

	}

	private static OffsetDateTime getReferenceDate(int seg) {
		int baseYear = 1970;
		if (seg >= 31 && seg <= 33) {
			baseYear = 1980;
		}

		return OffsetDateTime.of(java.time.LocalDate.of(baseYear, Month.JANUARY, 1), LocalTime.MIDNIGHT,
				ZoneOffset.UTC);

		/*
		 * return OffsetDateTime.of(LocalDate.of(baseYear, Month.JANUARY, 1),
		 * LocalTime.MIDNIGHT, ZoneOffset.UTC);
		 */
	}

	private static OffsetDateTime getDateFormat(int time, int seg) {
		return getReferenceDate(seg).plusSeconds(time);
	}

	public static int daysBetweenUsingJoda(Date d1, Date d2) {
		return Days.daysBetween(new LocalDate(d1.getTime()), new LocalDate(d2.getTime())).getDays();
	}

	public static String hoursDifference(long seconds) {

		/*
		 * Random r = new Random(); int low = 1; int high = 5; int result =
		 * r.nextInt(high-low) + low;
		 * 
		 * String durations= "Within " + String.valueOf(result) + " Hour"; return
		 * durations;
		 */

		Seconds second = Seconds.seconds((int) seconds);
		Hours hours = second.toStandardHours();

		if (hours.getHours() <= 24) {
			if (hours.getHours() <= 1) {
				return "Within " + 1 + " Hour";
			} else {
				return "Within " + hours.getHours() + " Hours";
			}
		} else if (hours.getHours() <= 168) {
			Days days = hours.toStandardDays();
			if (days.getDays() == 1) {
				return "Within " + days.getDays() + " Day";
			} else {
				return "Within " + days.getDays() + " Days";
			}
		} else if (hours.getHours() > 168 && hours.getHours() <= 720) {
			Weeks weeks = hours.toStandardWeeks();
			if (weeks.getWeeks() == 1) {
				return "Within " + weeks.getWeeks() + " Week";
			} else {
				return "Within " + weeks.getWeeks() + " Weeks";
			}
		} else {
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(seconds * 1000);
			int month = cal.get(Calendar.MONTH);
			if (month == 1) {
				return "Within " + month + " Month";
			} else {
				return "Within " + month + " Months";
			}
		}
	}

	public static String hoursDifference(Date date1, Date date2) {
		DateTime startDate = new DateTime(date1.getTime());
		DateTime endDate = new DateTime(date2.getTime());
		Hours hours = Hours.hoursBetween(startDate, endDate);
		if (hours.getHours() <= 24) {
			if (hours.getHours() <= 1) {
				return "Within " + 1 + " Hour";
			} else {
				return "Within " + hours.getHours() + " Hours";
			}
		} else if (hours.getHours() <= 144) {
			Days days = Days.daysBetween(startDate, endDate);
			if (days.getDays() == 1) {
				return "Within " + days.getDays() + " Day";
			} else {
				return "Within " + days.getDays() + " Days";
			}
		} else if (hours.getHours() > 144 && hours.getHours() <= 720) {
			Weeks weeks = Weeks.weeksBetween(startDate, endDate);
			if (weeks.getWeeks() == 1) {
				return "Within " + weeks.getWeeks() + " Week";
			} else {
				return "Within " + weeks.getWeeks() + " Weeks";
			}
		} else {
			Months months = Months.monthsBetween(startDate, endDate);
			if (months.getMonths() == 1) {
				return "Within " + months.getMonths() + " Month";
			} else {
				return "Within " + months.getMonths() + " Months";
			}
		}
	}

}

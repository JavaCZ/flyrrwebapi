package com.cloudzon.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cloudzon.exception.NotFoundException;
import com.cloudzon.exception.NotFoundException.NotFound;
import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.FlyrrDocument;
import com.cloudzon.model.User;

public class ImageUtils {

	private static final Logger logger = LoggerFactory.getLogger(ImageUtils.class);

	public static UUID updateProfilePicture(User user, MultipartFile profilePicture, HttpServletRequest servletRequest)
			throws IOException {

		logger.info("start update Profile picture");

		UUID imageUUID = null;
		byte[] bytes = null;
		BufferedOutputStream outStream = null;
		String webAppRoot = null;
		StringBuilder profilePictureUrl = null;

		try {
			profilePictureUrl = new StringBuilder();

			if (!profilePicture.isEmpty()) {
				/* Convert :- MultipartFile to Bytes */
				bytes = profilePicture.getBytes();

				/* Set :- AbsolutePath */
				webAppRoot = new File(servletRequest.getSession().getServletContext().getRealPath("/"))
						.getAbsolutePath();

				/* Generate :- Random UUID */
				imageUUID = UUID.randomUUID();

				/* Set New :- Image File */
				profilePictureUrl.delete(0, profilePictureUrl.length()).append(webAppRoot).append(File.separator)
						.append("resources").append(File.separator).append("images").append(File.separator)
						.append("profilePicture").append(File.separator).append(imageUUID).append(".png");

				/* Print :- Root Path */
				logger.info("profilePicture url:-" + webAppRoot);

				/* Set :- Image File */
				outStream = new BufferedOutputStream(new FileOutputStream(profilePictureUrl.toString()));

				/* Write :- Image File */
				outStream.write(bytes);
				outStream.close();

				/* Delete :- Existing profile picture */
				if (StringUtils.hasText(user.getProfilePicture())) {

					/* Set Old :- Image File */
					profilePictureUrl.delete(0, profilePictureUrl.length()).append(webAppRoot).append(File.separator)
							.append("resources").append(File.separator).append("images").append(File.separator)
							.append("profilePicture").append(File.separator).append(user.getProfilePicture());

					File file = new File(profilePictureUrl.toString());
					if (file.exists()) {
						/* Delete :- Old Image */
						if (!user.getProfilePicture().equalsIgnoreCase("default.png")) {
							file.delete();
							logger.info("Profile picture deleted");
						}
					}
				}
				/* Return :- imageUUID */
				return imageUUID;
			} else {
				// throw new NotFoundException(NotFound.ImageNotfound);
				throw new NotFoundException(NotFound.ImageNotFound, Boolean.TRUE);
			}

		} finally {
			logger.info("end updateProfilePicture");
			if (bytes != null) {
				bytes = null;
			}
			if (outStream != null) {
				outStream = null;
			}
			if (profilePictureUrl != null) {
				profilePictureUrl.delete(0, profilePictureUrl.length());
				profilePictureUrl = null;
			}
			webAppRoot = null;
		}

	}

	// ExStuff userProfile & Picture upload [ userServiceImpl ]
	/*
	 * public static UUID updateProfilePicture(User user,MultipartFile
	 * profilePicture, HttpServletRequest servletRequest)throws IOException {
	 * 
	 * logger.info("ImagesUtil start uploaduserProfile "); UUID imageUUID = null;
	 * byte[] bytes = null; BufferedOutputStream outStream = null;
	 * 
	 * String webAppRoot = null; StringBuilder userImageUrl = null;
	 * 
	 * try { userImageUrl = new StringBuilder();
	 * 
	 * if (!profilePicture.isEmpty()) { Convert :- MultipartFile to Bytes bytes =
	 * profilePicture.getBytes();
	 * 
	 * Set :- AbsolutePath webAppRoot = new File(servletRequest.getSession()
	 * .getServletContext().getRealPath("/")) .getAbsolutePath();
	 * 
	 * Generate :- Random UUID imageUUID = UUID.randomUUID();
	 * 
	 * Set New :- Image File userImageUrl.delete(0, userImageUrl.length())
	 * .append(webAppRoot).append(File.separator)
	 * .append("resources").append(File.separator)
	 * .append("images").append(File.separator)
	 * .append("broadcastImages").append(File.separator)
	 * .append(imageUUID).append(".png");
	 * 
	 * Print :- Root Path // logger.info("user  url:-" + webAppRoot);
	 * 
	 * Set :- Image File outStream = new BufferedOutputStream(new
	 * FileOutputStream(userImageUrl.toString()));
	 * 
	 * Write :- Image File outStream.write(bytes); outStream.close();
	 * 
	 * Delete :- Existing profile picture
	 * 
	 * if (StringUtils.hasText(user.getProfilePicture())) { Set Old :- Image File
	 * userImageUrl.delete(0, userImageUrl.length())
	 * .append(webAppRoot).append(File.separator)
	 * .append("resources").append(File.separator)
	 * .append("images").append(File.separator)
	 * .append("broadcastImages").append(File.separator)
	 * .append(user.getProfilePicture());
	 * 
	 * File file = new File(userImageUrl.toString()); if (file.exists()) { Delete :-
	 * Old Image file.delete(); // logger.info("User image deleted"); } }
	 * 
	 * Return :- imageUUID return imageUUID; }
	 * 
	 * else { throw new NotFoundException(NotFound.ImageNotFound, Boolean.TRUE); } }
	 * finally { // logger.info("end User Image"); if (bytes != null) { bytes =
	 * null; }
	 * 
	 * if (outStream != null) { outStream = null; }
	 * 
	 * if (userImageUrl != null) { userImageUrl.delete(0, userImageUrl.length());
	 * userImageUrl = null; } webAppRoot = null; }
	 * 
	 * }
	 */

	// ExStuff BroadcastImages upload [ BroadcastServiceImpl ]
	public static UUID uploadProfileImage(FlyrrDocument flyrrDocument, MultipartFile multipartFile,
			HttpServletRequest servletRequest) throws IOException {
		logger.info("ImageUtils_start uploaduserProfile");

		UUID imageUUID = null;
		byte[] bytes = null;
		BufferedOutputStream outStream = null;

		String webAppRoot = null;
		StringBuilder userImageUrl = null;
		try {
			userImageUrl = new StringBuilder();

			if (!multipartFile.isEmpty()) {
				/* Convert :- MultipartFile to Bytes */
				bytes = multipartFile.getBytes();

				/* Set :- AbsolutePath */
				webAppRoot = new File(servletRequest.getSession().getServletContext().getRealPath("/"))
						.getAbsolutePath();

				/* Generate :- Random UUID */
				imageUUID = UUID.randomUUID();

				/* Set New :- Image File */
				userImageUrl.delete(0, userImageUrl.length()).append(webAppRoot).append(File.separator)
						.append("resources").append(File.separator).append("images").append(File.separator)
						.append("broadcastImages").append(File.separator).append(imageUUID).append(".png");

				/* Print :- Root Path */
				// logger.info("user url:-" + webAppRoot);

				/* Set :- Image File */
				outStream = new BufferedOutputStream(new FileOutputStream(userImageUrl.toString()));

				/* Write :- Image File */
				outStream.write(bytes);
				outStream.close();

				/* Delete :- Existing profile picture */

				if (StringUtils.hasText((CharSequence) flyrrDocument.getImagesName())) {
					/* Set Old :- Image File */
					userImageUrl.delete(0, userImageUrl.length()).append(webAppRoot).append(File.separator)
							.append("resources").append(File.separator).append("images").append(File.separator)
							.append("broadcastImages").append(File.separator).append(flyrrDocument.getImagesName());

					File file = new File(userImageUrl.toString());
					if (file.exists()) { /* Delete :- Old Image */
						file.delete();
						// logger.info("User image deleted");
					}
				}

				/* Return :- imageUUID */
				return imageUUID;
			} else {
				throw new NotFoundException(NotFound.ImageNotFound, Boolean.TRUE);
			}
		} finally {
			// logger.info("end User Image");
			if (bytes != null) {
				bytes = null;
			}
			if (outStream != null) {
				outStream = null;
			}
			if (userImageUrl != null) {
				userImageUrl.delete(0, userImageUrl.length());
				userImageUrl = null;
			}
			webAppRoot = null;
		}
	}

}

package com.cloudzon.util;

import java.io.BufferedOutputStream;
import java.io.Console;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.cloudzon.exception.NotFoundException;
import com.cloudzon.exception.NotFoundException.NotFound;

public class FileUtil {

	private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

	private static String getFileExtension(MultipartFile file) {
		String fileName = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);
		return fileName;
	}

	/*
	 * public static String uploadFlyrrDocument(MultipartFile document,
	 * HttpServletRequest servletRequest)
	 * 
	 * throws IllegalStateException, IOException {
	 * logger.info("start uploadGrindDocument");
	 * 
	 * GetMultipartFile multipartFile = null; StringBuilder localDocumentPath =
	 * null; byte[] responseFile = null; UUID documentUUID = null; StringBuilder
	 * fileName = null; try { documentUUID = UUID.randomUUID(); localDocumentPath =
	 * new StringBuilder(); responseFile = document.getBytes(); String webAppRoot =
	 * new File(servletRequest.getSession().getServletContext().getRealPath("/"))
	 * .getAbsolutePath(); logger.info("Project location path:-" + webAppRoot);
	 * fileName = new StringBuilder();
	 * fileName.append(documentUUID).append(".").append(getFileExtension(document));
	 * localDocumentPath.delete(0,
	 * localDocumentPath.length()).append(webAppRoot).append(File.separator)
	 * .append("resources").append(File.separator)
	 * .append("documents").append(File.separator)
	 * .append("flyrrDocs").append(File.separator) .append(fileName.toString());
	 * 
	 * multipartFile = new GetMultipartFile(responseFile);
	 * multipartFile.transferTo(new File(localDocumentPath.toString()));
	 * 
	 * } finally { logger.info("end uploadGrindDocument"); }
	 * logger.info("end uploadGrindDocument"+fileName.toString()); return
	 * fileName.toString(); }
	 */

	// FlyrrImagesStore
	public static UUID updateFlyrrImages(MultipartFile profilePicture, HttpServletRequest servletRequest)
			throws IOException {

		logger.info("start update Profile picture");

		UUID imageUUID = null;
		byte[] bytes = null;
		BufferedOutputStream outStream = null;
		String webAppRoot = null;
		StringBuilder profilePictureUrl = null;

		try {
			profilePictureUrl = new StringBuilder();

			if (!profilePicture.isEmpty()) {
				/* Convert :- MultipartFile to Bytes */
				bytes = profilePicture.getBytes();

				/* Set :- AbsolutePath */
				webAppRoot = new File(servletRequest.getSession().getServletContext().getRealPath("/"))
						.getAbsolutePath();

				/* Generate :- Random UUID */
				imageUUID = UUID.randomUUID();

				/* Set New :- Image File */
				profilePictureUrl.delete(0, profilePictureUrl.length()).append(webAppRoot).append(File.separator)
						.append("resources").append(File.separator).append("documents").append(File.separator)
						.append("flyrrImages").append(File.separator);

				

				if (!new File(profilePictureUrl.toString()).exists()) {
					new File(profilePictureUrl.toString()).mkdirs();
				}
				/* Print :- Root Path */
				logger.info("profilePicture url:-" + webAppRoot);

				/* Set :- Image File */
				outStream = new BufferedOutputStream(new FileOutputStream(profilePictureUrl.append(imageUUID).append(".png").toString()));

				
				/* Set :- Image File */

				/* Write :- Image File */
				outStream.write(bytes);
				outStream.close();

				/* Delete :- Existing profile picture */
				/*
				 * if (StringUtils.hasText(user.getProfilePicture())) { Set Old :- Image File
				 * profilePictureUrl.delete(0, profilePictureUrl.length())
				 * .append(webAppRoot).append(File.separator)
				 * .append("resources").append(File.separator)
				 * .append("images").append(File.separator)
				 * .append("profilePicture").append(File.separator)
				 * .append(user.getProfilePicture());
				 * 
				 * File file = new File(profilePictureUrl.toString()); if (file.exists()) {
				 * Delete :- Old Image
				 * if(!user.getProfilePicture().equalsIgnoreCase("default.png")) {
				 * file.delete(); logger.info("Profile picture deleted"); } } }
				 */
				/* Return :- imageUUID */
				return imageUUID;
			} else {
				// throw new NotFoundException(NotFound.ImageNotfound);
				throw new NotFoundException(NotFound.ImageNotFound, Boolean.TRUE);
			}
		} finally {
			logger.info("end updateProfilePicture");
			if (bytes != null) {
				bytes = null;
			}
			if (outStream != null) {
				outStream = null;
			}
			if (profilePictureUrl != null) {
				profilePictureUrl.delete(0, profilePictureUrl.length());
				profilePictureUrl = null;
			}
			webAppRoot = null;
		}

	}

	public static void deleteFile(Boolean isBidDoc, String documentName, HttpServletRequest servletRequest) {
		StringBuilder localDocumentPath = new StringBuilder();
		logger.info("start deleteFile");
		try {
			String webAppRoot = new File(servletRequest.getSession().getServletContext().getRealPath("/"))
					.getAbsolutePath();
			/* Delete :- Existing document */
			if (StringUtils.hasText(documentName)) {
				/* Set Old :- File */
				if (isBidDoc) {
					localDocumentPath.delete(0, localDocumentPath.length()).append(webAppRoot).append(File.separator)
							.append("resources").append(File.separator).append("documents").append(File.separator)
							.append("flyrrDocs").append(File.separator).append(documentName);
				} else {
					localDocumentPath.delete(0, localDocumentPath.length()).append(webAppRoot).append(File.separator)
							.append("resources").append(File.separator).append("documents").append(File.separator)
							.append("resumeDocs").append(File.separator).append(documentName);
				}

				File file = new File(localDocumentPath.toString());
				if (file.exists()) {
					/* Delete :- Old File */
					file.delete();
					logger.info("Resume document deleted");
				}
			}
		} finally {
			logger.info("end deleteFile");
		}
	}
}

package com.cloudzon.exception;

public class MultipartSizeValidationException  extends CustomParameterizedException{

	public MultipartSizeValidationException(int status, ErrorDto errorDto, Boolean isSuccess, Boolean showMessage) {
		super(status,errorDto, isSuccess, showMessage);

	}

	private static final long serialVersionUID = -3074163624910129280L;

}

package com.cloudzon.exception;

public class NotFoundFlyrrException extends CustomParameterizedException {

	private static final long serialVersionUID = 1L;

	public NotFoundFlyrrException(NotFound2 notFound, Boolean showMessage) {
		super(200, new ErrorDto(notFound.getStatusCode(), notFound.getErrorMessage(), notFound.getDeveloperMessage()),
				Boolean.FALSE, showMessage);

	}

	public enum NotFound2 {
		NotDeletedFlyrr("50401", "This flyrr can not be deleted because it has been sold out by user ",
				"This flyrr can not be deleted because it has been sold out by user"), InEnableNotification("50402",
						"User is not intrested to enable notification",
						"User is not intrested to enable notification"), FlyrrSoldOut("50403", "Flyrr is sold out",
								"Flyrr is sold out"), RatingAndFeedback("50404",
										"You have already rated for this flyrr or user",
										"You have already rated for this flyrr or user"), FlyrrIsExpire("50405",
												"Sorry this flyrr is expire.",
												"Sorry this flyrr is expire"), DeletedFlyrr("50406",
														"This flyrr is deleted.",
														"This flyrr is deleted"), FlyRRNotFound("50409",
																"FlyRR Not Found",
																"Flyrr Deleted Or Not Created"), ReportFlyrr("50407",
																		"You have alredy report for this flyrr..!! ",
																		"You have alredy report for this flyrr..!!"), 
		CountFlyrrDeleted("50408","This flyrr is deleted by admin..!! ","This flyrr is deleted by admin..!!"),
		InternalServerError("500","Internal Server Error","Make Sure Code Have't Error"),
		ProxyAuthenticationRequired("407","ProxyAuthenticationRequired","The client must first authenticate itself with the proxy");
		//SessionExpire("501401","Session Expired","Try To Verify Login");

		private String statusCode;
		private String errorMessage;
		private String developerMessage;

		private NotFound2(String statusCode, String errorMessage, String developerMessage) {
			this.statusCode = statusCode;
			this.errorMessage = errorMessage;
			this.developerMessage = developerMessage;

		}

		private NotFound2(String statusCode, String errorMessage) {
			this.statusCode = statusCode;
			this.errorMessage = errorMessage;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public void setStatusCode(String statusCode) {
			this.statusCode = statusCode;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}

		public String getDeveloperMessage() {
			return developerMessage;
		}

		public void setDeveloperMessage(String developerMessage) {
			this.developerMessage = developerMessage;
		}

	}

}

package com.cloudzon.exception;

public class NotFoundException extends CustomParameterizedException {

	private static final long serialVersionUID = -8691443257932301841L;

	public NotFoundException(NotFound notFound, Boolean showMessage) {
		super(200, new ErrorDto(notFound.getStatusCode(), notFound.getErrorMessage(), notFound.getDeveloperMessage()),
				Boolean.FALSE, showMessage);
	}

	public enum NotFound {
		UserNotFound("40401", "User does not exists", "User Not Found For that ID"), ImageNotFound("40402",
				"Image does not exists", "Image Not Found"), TokenNotFound("40403", "Token does not exists",
						"Token Not Found"), VerificationCodeNotFound("40404", "Invalid verification code",
								"Verification Code Not Matched"), DevelopementNotFound("40405", "Under development",
										"Under development"), BroadCastNotFound("40406", "BroadCast Not Found",
												"Flyrr Deleted"), ParseLatlongToAddress("40407",
														"Not Parse LatLong To Address",
														"Not Parse LatLong To Address"), FieldNotFound("40410",
																"Somthing is missing in your field",
																"Please check your Field variable "),
		InternalServerError("500","Internal Server Error","Make Sure Code Have't Error"),
		ProxyAuthenticationRequired("407","ProxyAuthenticationRequired","The client must first authenticate itself with the proxy"),
		FlyrrNotFoundError("40411","FlyRR Not Found","Please Check Data in Database"),
		FlyrrIsExpire("50405",
				"Sorry this flyrr is expire.",
				"Sorry this flyrr is expire");
		/*
		 * NotDeletedFlyrr(
		 * "40411","This flyrr can not be deleted because it has been sold out by user "
		 * ,"This flyrr can not be deleted because it has been sold out by user"),
		 * InEnableNotification("40412","User is not intrested to enable notification"
		 * ,"User is not intrested to enable notification"),
		 * FlyrrSoldOut("40413","Flyrr is sold out","Flyrr is sold out"),
		 * RatingAndFeedback("40414","You have already rated for this flyrr or user"
		 * ,"You have already rated for this flyrr or user"),
		 * FlyrrIsExpire("40415","Sorry this flyrr is expire."
		 * ,"Sorry this flyrr is expire"),
		 * DeletedFlyrr("40415","This flyrr is deleted.","This flyrr is deleted"),
		 * ReportFlyrr("40416","You have alredy report for this flyrr..!! "
		 * ,"You have alredy report for this flyrr..!!"),
		 * CountFlyrrDeleted("40417","This flyrr is deleted by admin..!! "
		 * ,"This flyrr is deleted by admin..!!");
		 */

		private String statusCode;
		private String errorMessage;
		private String developerMessage;

		private NotFound(String statusCode, String errorMessage, String developerMessage) {

			this.statusCode = statusCode;
			this.errorMessage = errorMessage;
			this.developerMessage = developerMessage;
		}

		private NotFound(String statusCode, String errorMessage) {
			this.statusCode = statusCode;
			this.errorMessage = errorMessage;
		}

		public String getStatusCode() {
			return statusCode;
		}

		public void setStatusCode(String statusCode) {
			this.statusCode = statusCode;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}

		public String getDeveloperMessage() {
			return developerMessage;
		}

		public void setDeveloperMessage(String developerMessage) {
			this.developerMessage = developerMessage;
		}
	}

}

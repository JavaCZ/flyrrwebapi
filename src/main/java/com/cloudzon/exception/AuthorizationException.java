package com.cloudzon.exception;

import com.cloudzon.dto.*;
public class AuthorizationException extends CustomParameterizedException {

	private static final long serialVersionUID = -1949062844270976741L;

	public AuthorizationException(String applicationMessage, Boolean showMessage) {
		super(200, new ErrorDto("40102", applicationMessage, "Not authorized"), Boolean.FALSE, showMessage);
	}

}

/*{
"response": {
"errorMessage": "Field Error",
"errorCode": "400",
"developerMessage": "error.validation"
},
"isSuccess": false,
"showMessage": true
}*/
package com.cloudzon.exception;

public abstract class BaseWebApplicationException extends RuntimeException {

	private static final long serialVersionUID = -4644575486704638686L;

	private final int status;
	private final ErrorDto errorDto;
	private final Boolean isSuccess;
	private final Boolean showMessage;

	public BaseWebApplicationException(int status, ErrorDto errorDto, Boolean isSuccess, Boolean showMessage) {
		super();
		this.status = status;
		this.errorDto = errorDto;
		this.isSuccess = isSuccess;
		this.showMessage = showMessage;
	}

	public int getStatus() {
		return status;
	}

	public ErrorDto getErrorDto() {
		return errorDto;
	}

	public Boolean getIsSuccess() {
		return isSuccess;
	}

	public Boolean getShowMessage() {
		return showMessage;
	}

}

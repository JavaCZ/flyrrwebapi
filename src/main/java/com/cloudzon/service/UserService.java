package com.cloudzon.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import com.cloudzon.dto.AccessTokenContainer;
import com.cloudzon.dto.BroadcastInterestedResponseDTO;
import com.cloudzon.dto.BuyerDTO;
import com.cloudzon.dto.CategoriesDTO;
import com.cloudzon.dto.DeleteDTO;
import com.cloudzon.dto.DeviceTokenDTO;
import com.cloudzon.dto.FavoriteDTO;
import com.cloudzon.dto.FlyrrDetailsListDTO;
import com.cloudzon.dto.BroadcastMessageDTO;
import com.cloudzon.dto.BroadcastMessageInterestedDTO;
import com.cloudzon.dto.FavoriteListDTO;
import com.cloudzon.dto.FcmUserDTO;
import com.cloudzon.dto.FlyRRNotificationDto;
import com.cloudzon.dto.InterestedUserDto;
import com.cloudzon.dto.LoginSignUpResponseDTO;
import com.cloudzon.dto.MessageVerificationDTO;
import com.cloudzon.dto.MyReceiptListDTO;
import com.cloudzon.dto.NotificationDTO;
import com.cloudzon.dto.ProfileDTO;
import com.cloudzon.dto.RatingFeedbackDTO;
import com.cloudzon.dto.ReportDTO;
import com.cloudzon.dto.SearchFlyrrDTO;
import com.cloudzon.dto.SearchFlyrrDetailsResponseDTO;
import com.cloudzon.dto.SellerFlyrrListDTO;
import com.cloudzon.dto.BuyerFlyrrListDTO;
import com.cloudzon.dto.UserDetailsByIdDTO;
import com.cloudzon.dto.UserDetailsResponseDTO;
import com.cloudzon.dto.UserLoginDto;
import com.cloudzon.dto.UserProfileResponseDto;
import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.BroadcastMessageInterested;
import com.cloudzon.model.FCMUser;
import com.cloudzon.model.FavoriteFlyrr;
import com.cloudzon.model.NotificationUserDto;
import com.cloudzon.model.User;

@Service("userService")
public interface UserService {

	public User getUserData(Principal principal);

	public Long signUpOrlogin(UserLoginDto loginDto)
			throws JSONException, FileNotFoundException, IOException, ParseException;

	public User verifyLogin(MessageVerificationDTO verificationDTO);

	public AccessTokenContainer getAccessTokenContainer(User user, String jwt);
	// public Long verificationCall(UserLoginDto loginDto, String string);
	// public void logout(String accessToken);

	@Transactional
	public void updateUserProfile(User user, HttpServletRequest request, ProfileDTO profileDTO) throws IOException;

	public UserProfileResponseDto getUserProfile(User user);

	public BroadcastMessage createBroadcast(BroadcastMessageDTO broadcastMessageDTO, User objUser,
			HttpServletRequest request) throws IOException, MaxUploadSizeExceededException;

	public List<SellerFlyrrListDTO> sellingFlyrr(User user);

	public List<SellerFlyrrListDTO> getOpponentUserFlyrrList(Long userId, HttpServletRequest request);

	public void buyingFlyrr(User user, BuyerDTO buyerDTO);

	public List<BuyerFlyrrListDTO> getBuyingFlyrr(User user);

	public void favorite(User user, FavoriteDTO favoriteDTO);

	public List<FavoriteListDTO> getFavoriteFlyrr(User user);

	public void deactivateAccount(User user);

	public List<SearchFlyrrDetailsResponseDTO> nearByFlyrr(SearchFlyrrDTO searchFlyrrDTO, User user);

	public void deleteFlyrr(DeleteDTO deleteDTO, User user);

	public List<BroadcastInterestedResponseDTO> interestedInBroadcast(User user,
			BroadcastMessageInterestedDTO intrestedInBroadcastDTO);

	public void notification(NotificationDTO notificationDTO, User user);

	public List<String> searchCategories(String searchKeyword, User user, HttpServletRequest request);

	public void ratingAndFeedback(RatingFeedbackDTO ratingFeedbackDTO, User user);

	public void reportFlyrr(ReportDTO reportDTO, User user, HttpServletRequest request);

	public List<RatingFeedbackDTO> getRatingAndFeedbackList(Long rateByUserId, User user, HttpServletRequest request);

	public MyReceiptListDTO getReceiptList(Long broadcastId, User user, HttpServletRequest request);

	public Boolean deviceToken(DeviceTokenDTO deviceTokenDTO, User user);

	public List<InterestedUserDto> getInterestedUserDetails(List<Long> userId, User objUser);

	public List<FavoriteListDTO> getFevorietFlyRRsByUser(User user);
	/* public BroadcastMessage findBrodcastById(Long id); */
	// public BroadcastMessage getDetalisById(SearchFlyrrDTO dto);
	/*
	 * public Boolean sendUserNotification(NotificationUserDto notifyUserDto,User
	 * user); public Boolean SendFlyRRNotification(FlyRRNotificationDto dto);
	 */

	public void setUserLatLong(User user, String lattitude, String longitude);

	public List<String> trandingCategories();
	
	public List<UserDetailsResponseDTO> userDetailsByIdDTO(UserDetailsByIdDTO userDetailsByIdDTO);

}

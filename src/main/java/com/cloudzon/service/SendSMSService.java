package com.cloudzon.service;

public interface SendSMSService {

	public String sendMobileVerificationSMS(String mobileNumber, String verification_token);

	public void sendForgotPasswordSMS(String mobileNumber, String password);

	public void sendUserActivationSMS(String mobileNumber, String userActivationUrl);

	public void sendUserInviteSMS(String mobileNumber, String customMessage);

	public Boolean sendMobileVerificationCall(String mobileNumber, String responseUrl);

	public void sendUserFeedBackMessage(String mobileNumber);

}

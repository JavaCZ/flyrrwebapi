package com.cloudzon.service;

import com.cloudzon.dto.FcmUserDTO;
import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.User;

public interface NotificationService {

	public void NotifyUserByFlyRRCreation(BroadcastMessage broadcastMessage, User user);

	public Boolean chatnotification(User objUser, FcmUserDTO fcmUserDTO) throws Exception;
}

package com.cloudzon.service.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Principal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.logstash.logback.encoder.org.apache.commons.lang.BooleanUtils;
import springfox.documentation.service.ResponseMessage;

import org.hibernate.procedure.internal.Util.ResultClassesResolutionContext;
import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.iqregister.AccountManager;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.jxmpp.jid.DomainBareJid;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Localpart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import com.cloudzon.common.Constants;
import com.cloudzon.config.ApplicationProperties;
import com.cloudzon.dto.AccessTokenContainer;
import com.cloudzon.dto.BroadcastInterestedResponseDTO;
import com.cloudzon.dto.BuyerDTO;
import com.cloudzon.dto.CategoriesDTO;
import com.cloudzon.dto.DeleteDTO;
import com.cloudzon.dto.DeviceTokenDTO;
import com.cloudzon.dto.ErrorDto;
import com.cloudzon.dto.FavoriteDTO;
import com.cloudzon.dto.FlyrrDetailsListDTO;
import com.cloudzon.dto.BroadcastMessageDTO;
import com.cloudzon.dto.BroadcastMessageInterestedDTO;
import com.cloudzon.dto.BroadcastMessageWithCodeDTO;
import com.cloudzon.dto.FavoriteListDTO;
import com.cloudzon.dto.FlyRRNotificationDto;
import com.cloudzon.dto.InterestedUserDto;
import com.cloudzon.dto.MessageVerificationDTO;
import com.cloudzon.dto.MyReceiptListDTO;
import com.cloudzon.dto.NotificationDTO;
import com.cloudzon.dto.ProfileDTO;
import com.cloudzon.dto.RatingFeedbackDTO;
import com.cloudzon.dto.ReportDTO;
import com.cloudzon.dto.ResponseMessageDTO;
import com.cloudzon.dto.SearchFlyrrDTO;
import com.cloudzon.dto.SearchFlyrrDetailsResponseDTO;
import com.cloudzon.dto.SellerFlyrrListDTO;
import com.cloudzon.dto.UserDetailsByIdDTO;
import com.cloudzon.dto.UserDetailsResponseDTO;
import com.cloudzon.dto.UserFlyRRResponseDTO;
import com.cloudzon.dto.BuyerFlyrrListDTO;
import com.cloudzon.dto.UserLoginDto;
import com.cloudzon.dto.UserProfileResponseDto;
import com.cloudzon.exception.AuthorizationException;
import com.cloudzon.exception.NotFoundException;
import com.cloudzon.exception.NotFoundException.NotFound;
import com.cloudzon.exception.NotFoundFlyrrException;
import com.cloudzon.exception.NotFoundFlyrrException.NotFound2;
import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.BroadcastMessageInterested;
import com.cloudzon.model.BuyerFlyrr;
import com.cloudzon.model.Categories;
import com.cloudzon.model.DeviceToken;
import com.cloudzon.model.FCMUser;
import com.cloudzon.model.FavoriteFlyrr;
import com.cloudzon.model.FlyrrDocument;
import com.cloudzon.model.FlyrrJobCategories;
import com.cloudzon.model.NotificationUserDto;
import com.cloudzon.model.RatingFeedback;
import com.cloudzon.model.Report;
import com.cloudzon.model.SMSVerificationToken;
import com.cloudzon.model.User;
import com.cloudzon.repository.BroadcastMessageIntrestedRepository;
import com.cloudzon.repository.BroadcastMessageRepository;
import com.cloudzon.repository.BuyerFlyrrRepository;
import com.cloudzon.repository.CategoriesRepository;
import com.cloudzon.repository.DeviceTokenRepository;
import com.cloudzon.repository.FavoriteFlyrrRepository;
import com.cloudzon.repository.FcmUserRepository;
import com.cloudzon.repository.FlyrrCategoriesRepository;
import com.cloudzon.repository.FlyrrDocumentsRepository;
import com.cloudzon.repository.FlyrrJobCategoriesRepository;
import com.cloudzon.repository.RatingFeedbackRepository;
import com.cloudzon.repository.ReportRepository;
import com.cloudzon.repository.SMSVerificationTokenRepository;
import com.cloudzon.repository.UserRepository;
import com.cloudzon.service.NotificationService;
import com.cloudzon.service.UserService;
import com.cloudzon.util.DateUtil;
import com.cloudzon.util.FileUtil;
import com.cloudzon.util.ImageUtils;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserServiceImpl.class);

	@Resource
	private UserRepository userRepository;

	@Autowired
	private SMSVerificationTokenRepository smsVerificationTokenRepository;
	@Autowired
	private BroadcastMessageRepository broadcastMessageRepository;

	@Autowired
	private FavoriteFlyrrRepository favoriteFlyrrRepository;

	@Autowired
	private BroadcastMessageIntrestedRepository broadcastMessageIntrestedRepository;

	@Autowired
	private FlyrrJobCategoriesRepository flyrrJobCategoriesRepository;

	@Autowired
	private FlyrrCategoriesRepository flyrrCategoriesRepository;

	@Autowired
	private FlyrrDocumentsRepository flyrrDocumentsRepository;

	@Autowired
	private BuyerFlyrrRepository buyerFlyrrRepository;

	@Autowired
	private CategoriesRepository categoriesRepository;

	@Autowired
	private RatingFeedbackRepository ratingFeedbackRepository;

	@Autowired
	private ReportRepository reportRepository;

	@Autowired
	private DeviceTokenRepository deviceTokenRepository;

	@Autowired
	private FcmUserRepository fcmUserRepository;

	static AbstractXMPPConnection connection;
	
	@Value("${xmpp.host}")
	private String xmppHost;
	
	@Value("${xmpp.serviceName}")
	private String xmppServiceName;
	
	@Value("${xmpp.port}")
	private String xmppPort;
	
	@Value("${xmpp.default.password}")
	private String xmppDefaultPassword;

	/*
	 * @Autowired private InMemoryTokenStore tokenStore;
	 */

	@Autowired
	private NotificationService notificationService;

	private static DecimalFormat df2 = new DecimalFormat("#.#");

	// update profile
	@Transactional
	public User getUserData(Principal principal) {
		logger.info("getUserData");
		User user = null;
		if (principal != null && principal.getName() != null)
			System.out.println(principal.getName());
		user = this.userRepository.getUserByMobile(principal.getName());
		return user;
	}

	// signUpOrLogin
	@Override
	@Transactional(rollbackFor = Throwable.class, isolation = Isolation.READ_UNCOMMITTED)
	public Long signUpOrlogin(UserLoginDto loginDto) throws JSONException,
			FileNotFoundException, IOException, ParseException {

		User user = null;

		String mobileNumber = loginDto.getCountryCode()
				+ loginDto.getMobileNumber();

		String countrycode = "";

		try {

			// JSONArray a = (JSONArray) new JSONParser().parse(new
			// InputStreamReader(UserServiceImpl.class.getResourceAsStream("countryCode.txt")));
			// breakLoop:
			// for(Object aa : a){
			// JSONObject person = (JSONObject) aa;
			// String dial_code = (String) person.get("dial_code");
			// if(dial_code.equals(loginDto.getCountryCode().substring(1)))
			// {
			// countrycode = (String) person.get("code");
			// break breakLoop;
			// }
			// }

			// user = this.userRepository.getUserByMobileNumber(mobileNumber);
			user = this.userRepository.getLoginUserByMobileNumber(mobileNumber);
			if (user != null) {
				if (user.getIsActive() == Boolean.FALSE) {
					user.setIsActive(Boolean.TRUE);
					user.setIsVerified(Boolean.FALSE);
					user.setProfilePicture(user.getProfilePicture());
					user.setMobileNumber(mobileNumber);
					user.setUserName(mobileNumber);
					user.setCreatedDate(DateUtil.getCurrentDate());
					user.setDialCode(loginDto.getCountryCode());
					user.setCountryCode(countrycode);
					user.setRate(0.0);
					user.setoSVersion(loginDto.getoSVersion());
					user.setDeviceModel(loginDto.getDeviceModel());

					this.userRepository.save(user);

					SMSVerificationToken objSmsVerificationToken = new SMSVerificationToken(
							user,
							SMSVerificationToken.VerificationTokenType.loginVerification,
							Constants.REGISTER_MESSAGE_LINK_EXPIRE_TIME);

					objSmsVerificationToken.setCreatedBy(user.getId());
					objSmsVerificationToken.setMobileNumber(loginDto
							.getMobileNumber());
					objSmsVerificationToken.setUser(user);
					objSmsVerificationToken.setIsUsed(false);
					objSmsVerificationToken.setCreatedDate(DateUtil
							.getCurrentDate());
					this.smsVerificationTokenRepository
							.save(objSmsVerificationToken);

				} else {

					SMSVerificationToken objSmsVerificationToken = new SMSVerificationToken(
							user,
							SMSVerificationToken.VerificationTokenType.loginVerification,
							Constants.REGISTER_MESSAGE_LINK_EXPIRE_TIME);

					objSmsVerificationToken.setCreatedBy(user.getId());
					objSmsVerificationToken.setMobileNumber(mobileNumber);
					objSmsVerificationToken.setIsUsed(false);
					objSmsVerificationToken.setUser(user);
					objSmsVerificationToken.setCreatedDate(DateUtil
							.getCurrentDate());
					this.smsVerificationTokenRepository
							.save(objSmsVerificationToken);

				}

			} else {

				user = new User();
				user.setIsActive(Boolean.TRUE);
				user.setIsVerified(Boolean.FALSE);
				user.setProfilePicture("default.png");
				user.setDialCode(loginDto.getCountryCode());
				user.setMobileNumber(mobileNumber);
				user.setUserName(mobileNumber);
				user.setCountryCode(countrycode);
				user.setRate(0.0);
				user.setCreatedDate(DateUtil.getCurrentDate());
				user.setoSVersion(loginDto.getoSVersion());
				user.setDeviceModel(loginDto.getDeviceModel());
				this.userRepository.save(user);

				SMSVerificationToken objSmsVerificationToken = new SMSVerificationToken(
						user,
						SMSVerificationToken.VerificationTokenType.loginVerification,
						Constants.REGISTER_MESSAGE_LINK_EXPIRE_TIME);

				objSmsVerificationToken.setCreatedBy(user.getId());
				objSmsVerificationToken.setMobileNumber(mobileNumber);
				objSmsVerificationToken.setIsUsed(false);
				objSmsVerificationToken.setUser(user);
				objSmsVerificationToken.setCreatedDate(DateUtil
						.getCurrentDate());

				this.smsVerificationTokenRepository
						.save(objSmsVerificationToken);

			}
		} finally {
			logger.info("login end");
		}
		return user.getId();
	}

	// verifyLogin

	@Transactional(rollbackFor = Throwable.class, isolation = Isolation.READ_UNCOMMITTED)
	public User verifyLogin(MessageVerificationDTO verificationDTO) {
		User objUser = null;
		objUser = this.userRepository.getUserById(verificationDTO.getUserId());
		// update user verified

		if (objUser != null) {
			objUser.setIsVerified(Boolean.TRUE);
			this.userRepository.save(objUser);

			if (!objUser.getIsXmppUser()) {
				if (createXamppUser(objUser)) {
					objUser.setIsXmppUser(Boolean.TRUE);
				} else {
					objUser.setIsXmppUser(Boolean.FALSE);
				}
				return objUser;
			}

		} else {
			throw new NotFoundException(NotFound.VerificationCodeNotFound,
					Boolean.TRUE);
		}

		// SMSVerificationToken objSmsVerificationToken =
		// this.smsVerificationTokenRepository.getVerifactionTokenByToken(
		// verificationDTO.getVerificationCode(),
		// SMSVerificationToken.VerificationTokenType.loginVerification,objUser);
		// if (objSmsVerificationToken != null)
		// {
		// if (objSmsVerificationToken.getIsUsed())
		// {
		// throw new
		// AlreadyVerifiedException(AlreadyVerifiedExceptionType.Token);
		// }
		// else
		// { //update used token
		// objSmsVerificationToken.setIsUsed(true);
		// this.smsVerificationTokenRepository.save(objSmsVerificationToken); //
		// //update user verified
		// objUser = objSmsVerificationToken.getUser();
		// objUser.setIsVerified(Boolean.TRUE);
		// this.userRepository.save(objUser);
		// return objUser;
		// }
		//
		// } else {
		// throw
		// new NotFoundException(NotFound.VerificationCodeNotFound,
		// Boolean.TRUE);
		// }

		return objUser;
	}

	// verification call Proceess

	// @Transactional(rollbackFor = Throwable.class, isolation =
	// Isolation.READ_UNCOMMITTED)
	// public Long verificationCall(UserLoginDto loginDto, String responseUrl)
	// {
	//
	// logger.info("verificationCall start");
	// User user = null;
	// user =
	// this.userRepository.getUserByMobileNumber(loginDto.getMobileNumber());
	//
	// SMSVerificationToken objSmsVerificationToken = new
	// SMSVerificationToken(user,SMSVerificationToken.VerificationTokenType.messageVerification,Constants.REGISTER_MESSAGE_LINK_EXPIRE_TIME);
	// responseUrl = responseUrl + "?token="+
	// objSmsVerificationToken.getVerification_token();
	// this.sendSMSService.sendMobileVerificationCall(loginDto.getMobileNumber(),
	// responseUrl);
	//
	// objSmsVerificationToken.setCreatedBy(user.getId());
	// objSmsVerificationToken.setMobileNumber(loginDto.getMobileNumber());
	// objSmsVerificationToken.setIsUsed(false);
	// objSmsVerificationToken.setUser(user);
	// objSmsVerificationToken.setCreatedDate(DateUtil.getCurrentDate());
	// this.smsVerificationTokenRepository.save(objSmsVerificationToken);
	// logger.info("verificationCall end");
	// return user.getId();
	// }

	// logout

	/*
	 * public void logout(String accessToken) { User user =
	 * this.tokenStore.readAccessToken(accessToken); // User objUser = if (user
	 * != null) { // this.tokenStore.removeToken(accessToken); } else { throw
	 * new AuthorizationException("Session expired", Boolean.TRUE); }
	 * 
	 * }
	 */
	// getAccessTokenContainer

	private Boolean createXamppUser(User objUser) {
		try {

			XMPPTCPConnectionConfiguration.Builder builder = XMPPTCPConnectionConfiguration
					.builder();
			builder.setSecurityMode(SecurityMode.disabled);
			DomainBareJid domain = JidCreate.domainBareFrom(xmppServiceName);
			builder.setXmppDomain(domain);
			builder.setHost(xmppHost);
			builder.setPort(Integer.valueOf(xmppPort));
			builder.setDebuggerEnabled(true);
			connection = new XMPPTCPConnection(builder.build());
			connection.setPacketReplyTimeout(10000);
			connection.connect();
			AccountManager accountManager = AccountManager
					.getInstance(connection);
			accountManager.sensitiveOperationOverInsecureConnection(true);
			if (accountManager.supportsAccountCreation()) {
				accountManager.sensitiveOperationOverInsecureConnection(true);
				Localpart localpart = Localpart
						.from(objUser.getId().toString());
				accountManager.createAccount(localpart, xmppDefaultPassword);
				connection.login(localpart, xmppDefaultPassword);
				System.out.println("isAuthenticated:----->"
						+ connection.isAuthenticated());
			}
			connection.disconnect();
			return true;
		} catch (SmackException e) {
			logger.error("SmackException: " + e.getLocalizedMessage());
			return false;
		} catch (XMPPException e) {
			logger.error("XMPPException: " + e.getLocalizedMessage());
			return false;
		} catch (IOException e) {
			logger.error("IOException in smack: " + e.getLocalizedMessage());
			return false;
		} catch (InterruptedException e) {
			logger.error("InterruptedException in smack: "
					+ e.getLocalizedMessage());
			return false;
		}
		/*
		 * try { XMPPTCPConnectionConfiguration.Builder builder =
		 * XMPPTCPConnectionConfiguration.builder();
		 * builder.setSecurityMode(SecurityMode.disabled); DomainBareJid domain
		 * = JidCreate.domainBareFrom("localhost");
		 * builder.setXmppDomain(domain); builder.setHost("192.168.1.88");
		 * builder.setPort(Integer.valueOf("5222"));
		 * builder.setDebuggerEnabled(true); connection = new
		 * XMPPTCPConnection(builder.build());
		 * connection.setPacketReplyTimeout(10000); connection.connect();
		 * AccountManager accountManager =
		 * AccountManager.getInstance(connection);
		 * accountManager.sensitiveOperationOverInsecureConnection(true);
		 * Map<String, String> map = new HashMap<String, String>();
		 * map.put("username", objUser.getId().toString()); map.put("name",
		 * objUser.getFullName()); map.put("password", "adminflyrr@server");
		 * map.put("creationDate", DateUtil.getCurrentDate().toString());
		 * Localpart localpart = Localpart.from(objUser.getId().toString());
		 * accountManager.createAccount(localpart, "adminflyrr@server", map);
		 * ChatManager chatManager = ChatManager.getInstanceFor(connection);
		 * EntityBareJid jid = JidCreate.entityBareFrom(localpart.toString());
		 * Chat chat = chatManager.createChat(jid); return true; } catch
		 * (SmackException e) { logger.error("SmackException: " +
		 * e.getLocalizedMessage()); return false; } catch (XMPPException e) {
		 * logger.error("XMPPException: " + e.getLocalizedMessage()); return
		 * false; } catch (IOException e) {
		 * logger.error("IOException in smack: " + e.getLocalizedMessage());
		 * return false; } catch (InterruptedException e) {
		 * logger.error("InterruptedException in smack: " +
		 * e.getLocalizedMessage()); return false; }
		 */

	}

	public AccessTokenContainer getAccessTokenContainer(User user, String jwt) {
		if (user.getFullName() != null && !user.getFullName().isEmpty()
				&& user.getProfilePicture() != null) {
			user.setIsCompleteProfile(Boolean.TRUE);
			this.userRepository.save(user);
		} else {
			user.setIsCompleteProfile(Boolean.FALSE);
			this.userRepository.save(user);
		}
		return new AccessTokenContainer(jwt, user.getId(), user.getFullName(),
				user.getProfilePicture(), user.getRate(),
				user.getIsCompleteProfile(), user.getIsEnableNotification());
	}

	// updateProfile(exstuff)
	@Override
	@Transactional(rollbackFor = Throwable.class, isolation = Isolation.READ_UNCOMMITTED)
	public void updateUserProfile(User user, HttpServletRequest request,
			ProfileDTO profileDTO) throws IOException {

		StringBuilder objSB = null;
		UUID imageUUID = null;
		try {
			// Generate referral code
			// String referralCode = null;

			user.setFullName(profileDTO.getFullName());

			/*
			 * if (profileDTO.getIsEnabledNotification() == null) {
			 * user.setIsEnableNotification(Boolean.TRUE); } else {
			 * user.setIsEnableNotification
			 * (profileDTO.getIsEnabledNotification()); } this.userRe
			 * pository.save(user);
			 */

			if (user.getFullName() != null) {
				user.setIsCompleteProfile(Boolean.TRUE);
				this.userRepository.save(user);
			} else {
				throw new NotFoundException(NotFound.FieldNotFound,
						Boolean.TRUE);
			}

			objSB = new StringBuilder();

			/* update profile picture */

			if (profileDTO.getProfilePicture() != null) {
				imageUUID = ImageUtils.updateProfilePicture(user,
						profileDTO.getProfilePicture(), request);
				if (imageUUID != null) {
					/* update user */
					objSB.delete(0, objSB.length()).append(imageUUID)
							.append(".png");
					user.setProfilePicture(objSB.toString());
					// user.setModifiedBy(user.getId());
					user.setModifiedDate(DateUtil.getCurrentDate());
					this.userRepository.saveAndFlush(user);
				}
			}

		} finally {
			logger.info("end updateProfile");
			if (objSB != null) {
				objSB.delete(0, objSB.length());
				objSB = null;
			}
			if (imageUUID != null) {
				imageUUID = null;
			}
		}
	}

	@Override
	@Transactional
	public MyReceiptListDTO getReceiptList(Long broadcastId, User user,
			HttpServletRequest request) {
		MyReceiptListDTO myReceiptListDTO = new MyReceiptListDTO();

		BroadcastMessage broadcastMessage = this.broadcastMessageRepository
				.findOne(broadcastId);
		if (broadcastMessage != null) {
			BuyerFlyrr buyerFlyrr2 = this.buyerFlyrrRepository
					.getSellerBuyerFlyrrForBroadcastMessage(broadcastMessage);
			myReceiptListDTO.setUserId(buyerFlyrr2.getUser().getId());
			myReceiptListDTO.setFullName(buyerFlyrr2.getUser().getFullName());
			myReceiptListDTO.setProfilePicture(buyerFlyrr2.getUser()
					.getProfilePicture());
			myReceiptListDTO.setPrice(buyerFlyrr2.getBroadcastMessage()
					.getPrice());
			myReceiptListDTO.setCreatedDate(buyerFlyrr2.getBroadcastMessage()
					.getCreateDate());
			myReceiptListDTO.setTitle(buyerFlyrr2.getBroadcastMessage()
					.getTitle());
			return myReceiptListDTO;
		} else {
			throw new NotFoundFlyrrException(NotFound2.FlyRRNotFound,
					Boolean.TRUE);
		}

	}

	// getUserProfile
	@Override
	@Transactional(readOnly = false)
	public UserProfileResponseDto getUserProfile(User user) {
		logger.info("Show getUserProfile");
		try {
			UserProfileResponseDto userProfile = new UserProfileResponseDto();
			userProfile.setUserId(user.getId());
			userProfile.setFullName(user.getFullName());
			userProfile.setProfilePicture(user.getProfilePicture());
			userProfile.setDialCode(user.getDialCode());
			String mobileno = this.removeCountryCode(user.getMobileNumber(),
					user.getDialCode());
			userProfile.setMobileNumber(mobileno);
			userProfile.setBankName(user.getBankName());
			userProfile.setBankAccountNumber(user.getBankAccountNumber());
			userProfile.setEnableNotification(user.getIsEnableNotification());

			if (user.getRate() != null && user.getRate() <= 0) {
				userProfile.setRate("0");
			} else {
				userProfile.setRate(user.getRate().toString());
			}

			userProfile.setEnableNotification(user.getIsEnableNotification());
			return userProfile;
		} finally {
			logger.info("end getUserProfile");
		}

	}

	private String removeCountryCode(String strMobWithCD, String ccode) {
		String strNewMob = null;
		if (strMobWithCD.contains(ccode)) {

			strNewMob = strMobWithCD.replace(ccode, "").toString();
		} else if (strMobWithCD.contains(ccode)) {

			strNewMob = strMobWithCD.replace(ccode, "").toString();
		} else {

			strNewMob = strMobWithCD;
		}

		return strNewMob;

	}

	@Transactional(rollbackFor = Throwable.class, isolation = Isolation.READ_COMMITTED)
	public BroadcastMessage createBroadcast(
			BroadcastMessageDTO broadcastMessageDTO, User user,
			HttpServletRequest request) throws IOException,
			MaxUploadSizeExceededException {
		// TODO Auto-generated method stub

		logger.info("broadcastService CreatedBroadcast");
		BroadcastMessage broadcastMessage = new BroadcastMessage();
		Categories objJobCategory = null;
		FlyrrJobCategories flyrrJobCategories = null;
		FlyrrDocument flyrrDocument = null;
		StringBuilder objSB = null;
		UUID imageUUID = null;
		final long limit = 1 * 1024 * 1024;
		objSB = new StringBuilder();
		try {

			broadcastMessage.setTitle(broadcastMessageDTO.getTitle());
			broadcastMessage.setDescription(broadcastMessageDTO
					.getDescription());
			broadcastMessage.setPrice(broadcastMessageDTO.getPrice());
			broadcastMessage.setLattitude(broadcastMessageDTO.getLattitude());
			broadcastMessage.setLongitude(broadcastMessageDTO.getLongitude());
			broadcastMessage.setExpireIn(broadcastMessageDTO.getExpireIn());
			broadcastMessage.setIsNegotiable(broadcastMessageDTO
					.getIsNegotiable());
			broadcastMessage.setIsActive(Boolean.TRUE);
			broadcastMessage.setIsDelete(Boolean.FALSE);
			broadcastMessage.setUser(user);
			broadcastMessage.setCreatedDate(DateUtil.getCurrentDate());
			broadcastMessage.setModifiedDate(DateUtil.getCurrentDate());
			broadcastMessage.setIsBuying(false);
			this.broadcastMessageRepository.save(broadcastMessage);

			List<FlyrrJobCategories> JobCategories = this.flyrrJobCategoriesRepository
					.findFlyrrJobCategoriesByBroadcastMessage(broadcastMessage);

			// for (FlyrrJobCategories categories : JobCategories) {
			// categories.setIsActive(false);
			// this.flyrrJobCategoriesRepository.save(categories);
			// }

			for (String jobCatagory : broadcastMessageDTO.getCategories()) {
				objJobCategory = this.flyrrCategoriesRepository
						.findByCategories(jobCatagory);

				if (objJobCategory == null) {
					objJobCategory = new Categories();
					objJobCategory.setCreatedDate(DateUtil.getCurrentDate());
					objJobCategory.setActive(Boolean.TRUE);
					objJobCategory.setCategories(jobCatagory.toLowerCase());
					this.flyrrCategoriesRepository.save(objJobCategory);
				}

				flyrrJobCategories = new FlyrrJobCategories();
				flyrrJobCategories.setBroadcastMessage(broadcastMessage);
				flyrrJobCategories.setJobCategory(objJobCategory);
				flyrrJobCategories.setCreatedDate(DateUtil.getCurrentDate());
				flyrrJobCategories.setIsActive(Boolean.TRUE);
				this.flyrrJobCategoriesRepository.save(flyrrJobCategories);
			}

			List<FlyrrDocument> flyrrDocumentList = this.flyrrDocumentsRepository
					.findFlyrrDocumentByBroadcastMessageAndIsActive(
							broadcastMessage, true);
			for (FlyrrDocument flyrrDoc : flyrrDocumentList) {
				flyrrDoc.setIsActive(false);
				this.flyrrDocumentsRepository.save(flyrrDoc);
				FileUtil.deleteFile(Boolean.TRUE, flyrrDoc.getImagesPath(),
						request);
			}

			if (broadcastMessageDTO.getImages() != null
					&& broadcastMessageDTO.getImages().size() > 0) {
				for (MultipartFile images : broadcastMessageDTO.getImages()) {
					imageUUID = FileUtil.updateFlyrrImages(images, request);
					flyrrDocument = new FlyrrDocument();
					flyrrDocument.setBroadcastMessage(broadcastMessage);
					flyrrDocument.setCreatedDate(DateUtil.getCurrentDate());
					flyrrDocument.setImagesName(images.getOriginalFilename());

					objSB.delete(0, objSB.length()).append(imageUUID)
							.append(".png");
					flyrrDocument.setImagesPath(objSB.toString());

					flyrrDocument.setIsActive(true);
					this.flyrrDocumentsRepository.save(flyrrDocument);

				}
			}
			this.notificationService.NotifyUserByFlyRRCreation(
					broadcastMessage, user);
			System.out.println("Users-------->" + user.getId());
			return broadcastMessage;
		} finally {

			logger.info("CreatedBroadcast");
		}
	}

	@Override
	public List<SellerFlyrrListDTO> sellingFlyrr(User user) {
		logger.info("start getSellingFlyrr");
		double rangeInMiles = 0.0;
		List<SellerFlyrrListDTO> listBroadcastMessageUserDTO = new ArrayList<>();
		try {
			List<BroadcastMessage> broadcastMessage = this.broadcastMessageRepository
					.getSellerByBroadcastMessageByUser(user);
			for (BroadcastMessage listBroadcastMessage : broadcastMessage) {
				SellerFlyrrListDTO broadcastMessageAndUserDTO = new SellerFlyrrListDTO();
				broadcastMessageAndUserDTO.setBroadcastId(listBroadcastMessage
						.getId());
				broadcastMessageAndUserDTO.setPrice(listBroadcastMessage
						.getPrice());
				broadcastMessageAndUserDTO.setTitle(listBroadcastMessage
						.getTitle());
				broadcastMessageAndUserDTO.setDescription(listBroadcastMessage
						.getDescription());
				broadcastMessageAndUserDTO.setLattitude(listBroadcastMessage
						.getLattitude());
				broadcastMessageAndUserDTO.setLongitude(listBroadcastMessage
						.getLongitude());
				broadcastMessageAndUserDTO.setExpireIn(listBroadcastMessage
						.getExpireIn());
				broadcastMessageAndUserDTO.setActive(listBroadcastMessage
						.getIsActive());
				broadcastMessageAndUserDTO
						.setCategories(this.flyrrJobCategoriesRepository
								.getCategoriesByBroadcastMessage(listBroadcastMessage));
				broadcastMessageAndUserDTO
						.setImages(this.flyrrDocumentsRepository
								.getFlyrrDocumentNameByBroadcastMessageId(listBroadcastMessage));
				broadcastMessageAndUserDTO.setIsBuying(listBroadcastMessage
						.getIsBuying());
				
				rangeInMiles=this.getDistanceBetweenTwoPoints(Double.valueOf(user.getLattitude()), Double.valueOf(user.getLongitude()),
						Double.valueOf(listBroadcastMessage.getLattitude()), Double.valueOf(listBroadcastMessage.getLongitude()));
				broadcastMessageAndUserDTO.setRangeInMile(rangeInMiles);

				listBroadcastMessageUserDTO.add(broadcastMessageAndUserDTO);
			}
		} finally {

		}
		return listBroadcastMessageUserDTO;
	}

	@Override
	public void buyingFlyrr(User user, BuyerDTO buyerDTO) {
		logger.info("Buying your flyrr Process Start");
		BuyerFlyrr buyerFlyrr = null;
		try {
			BroadcastMessage broadcastMessage = this.broadcastMessageRepository
					.findOne(buyerDTO.getBroadcastId());
			List<BroadcastMessage> broadcastMessageDeletedList = new ArrayList<>();
			List<BroadcastMessage> broadcastMessageExpireInList = new ArrayList<>();
			Date dateCurrentDate = DateUtil.getCurrentDate();
			Date dateExpireIn = (broadcastMessage.getExpireIn());
			List<BroadcastMessage> broadcastMessageBuyingList = new ArrayList<>();

			buyerFlyrr = this.buyerFlyrrRepository
					.findOneByBroadcastMessageAndUser(broadcastMessage, user);

			// check flyrr is buy or not..!!
			broadcastMessageBuyingList = this.broadcastMessageRepository
					.getBroadcastMessageByIsBuying(broadcastMessage);

			// check Flyrr deleted or not..!!
			broadcastMessageDeletedList = this.broadcastMessageRepository
					.getBroadcastMessageByIsDelete(broadcastMessage);

			// check flyrr is Expire or not..!!
			broadcastMessageExpireInList = this.broadcastMessageRepository
					.getBroadcastMessageByIsExpireIn(broadcastMessage,
							dateCurrentDate);

			// delete flyrr
			if (broadcastMessageDeletedList.size() == 0) {
				// expire flyrr
				if (broadcastMessageExpireInList.size() == 0) {
					if (broadcastMessageBuyingList.size() == 0) {
						if (buyerFlyrr == null) {
							buyerFlyrr = new BuyerFlyrr();
							buyerFlyrr
									.setCreatedDate(DateUtil.getCurrentDate());
							buyerFlyrr.setBroadcastMessage(broadcastMessage);
							buyerFlyrr.setUser(user);
							buyerFlyrr.setIsActive(Boolean.TRUE);
							this.buyerFlyrrRepository.save(buyerFlyrr);

							broadcastMessage.setIsBuying(true);
							this.broadcastMessageRepository
									.save(broadcastMessage);

						} else {
							buyerFlyrr.setIsActive(Boolean.TRUE);
							this.buyerFlyrrRepository.save(buyerFlyrr);
						}
					} else {
						throw new NotFoundFlyrrException(
								NotFound2.FlyrrSoldOut, Boolean.TRUE);
					}

				} else {
					throw new NotFoundFlyrrException(NotFound2.FlyrrIsExpire,
							Boolean.TRUE);
				}
			} else {
				throw new NotFoundFlyrrException(NotFound2.DeletedFlyrr,
						Boolean.TRUE);

			}

		} finally {
			logger.info("Buying End");
		}

	}

	@Override
	public List<BuyerFlyrrListDTO> getBuyingFlyrr(User user) {
		logger.info("start getBuyingFlyrr ");
		double rangeInMiles = 0.0;
		List<BuyerFlyrrListDTO> listBroadcastMessageUserDTO = new ArrayList<>();
		UserFlyRRResponseDTO userFlyRRResponseDTO;
		try {
			List<BroadcastMessage> broadcastMessage = this.buyerFlyrrRepository
					.getBuyerByBroadcastMessageByUser(user);

			for (BroadcastMessage listBroadcastMessage : broadcastMessage) {
				BuyerFlyrrListDTO buyerFlyrrListDTO = new BuyerFlyrrListDTO();
				buyerFlyrrListDTO.setCreatedDate(listBroadcastMessage
						.getCreateDate());
				buyerFlyrrListDTO.setBroadcastId(listBroadcastMessage.getId());
				buyerFlyrrListDTO.setPrice(listBroadcastMessage.getPrice());
				buyerFlyrrListDTO.setTitle(listBroadcastMessage.getTitle());
				buyerFlyrrListDTO.setDescription(listBroadcastMessage
						.getDescription());
				buyerFlyrrListDTO.setLattitude(listBroadcastMessage
						.getLattitude());
				buyerFlyrrListDTO.setLongitude(listBroadcastMessage
						.getLongitude());
				buyerFlyrrListDTO.setExpireIn(listBroadcastMessage
						.getExpireIn());
				buyerFlyrrListDTO.setActive(listBroadcastMessage.getIsActive());
				buyerFlyrrListDTO
						.setCategories(this.flyrrJobCategoriesRepository
								.getCategoriesByBroadcastMessage(listBroadcastMessage));
				buyerFlyrrListDTO
						.setImages(this.flyrrDocumentsRepository
								.getFlyrrDocumentNameByBroadcastMessageId(listBroadcastMessage));
				userFlyRRResponseDTO = new UserFlyRRResponseDTO(
						listBroadcastMessage.getUser().getId(),
						listBroadcastMessage.getUser().getMobileNumber(),
						listBroadcastMessage.getUser().getFullName(),
						listBroadcastMessage.getUser().getProfilePicture(),
						listBroadcastMessage.getUser().getRate());
				buyerFlyrrListDTO.setUser(userFlyRRResponseDTO);
				
				rangeInMiles=this.getDistanceBetweenTwoPoints(Double.valueOf(user.getLattitude()), Double.valueOf(user.getLongitude()),
						Double.valueOf(listBroadcastMessage.getLattitude()), Double.valueOf(listBroadcastMessage.getLongitude()));
				buyerFlyrrListDTO.setRangeInMile(rangeInMiles);
				
				listBroadcastMessageUserDTO.add(buyerFlyrrListDTO);
				// Collections.sort(listBroadcastMessageUserDTO,
				// Collections.reverseOrder());
			}
			return listBroadcastMessageUserDTO;
		} finally {

		}

	}

	@Override
	public void favorite(User user, FavoriteDTO favoriteDTO) {
		logger.info("Favorite Flyrr process start");
		FavoriteFlyrr favoriteFlyrr = null;
		try {
			BroadcastMessage broadcastMessage = this.broadcastMessageRepository
					.findOne(favoriteDTO.getBroadcastId());

			BuyerFlyrr buyerFlyrr = null;
			List<BroadcastMessage> broadcastMessageDeletedList = new ArrayList<>();
			List<BroadcastMessage> broadcastMessageExpireInList = new ArrayList<>();
			List<BroadcastMessage> broadcastMessageBuyingList = new ArrayList<>();

			Date dateCurrentDate = DateUtil.getCurrentDate();
			Date dateExpireIn = (broadcastMessage.getExpireIn());

			buyerFlyrr = this.buyerFlyrrRepository
					.findOneByBroadcastMessageAndUser(broadcastMessage, user);

			// check flyrr is buy or not..!!
			broadcastMessageBuyingList = this.broadcastMessageRepository
					.getBroadcastMessageByIsBuying(broadcastMessage);

			// check Flyrr deleted or not..!!
			broadcastMessageDeletedList = this.broadcastMessageRepository
					.getBroadcastMessageByIsDelete(broadcastMessage);

			// check flyrr is Expire or not..!!
			broadcastMessageExpireInList = this.broadcastMessageRepository
					.getBroadcastMessageByIsExpireIn(broadcastMessage,
							dateCurrentDate);

			favoriteFlyrr = this.favoriteFlyrrRepository
					.findOneByBroadcastMessageAndUser(broadcastMessage, user);

			if (broadcastMessageDeletedList.size() == 0) {
				// expire flyrr
				if (broadcastMessageExpireInList.size() == 0) {
					if (broadcastMessageBuyingList.size() == 0) {
						if (favoriteFlyrr == null) {
							favoriteFlyrr = new FavoriteFlyrr();
							favoriteFlyrr.setCreatedDate(DateUtil
									.getCurrentDate());
							favoriteFlyrr.setBroadcastMessage(broadcastMessage);
							favoriteFlyrr.setUser(user);
							favoriteFlyrr.setIsActive(Boolean.TRUE);
							favoriteFlyrr.setIsFavorite(Boolean.TRUE);
							this.favoriteFlyrrRepository.save(favoriteFlyrr);
						} else {
							if (!favoriteFlyrr.getIsFavorite()) {
								favoriteFlyrr.setModifiedDate(DateUtil
										.getCurrentDate());
								favoriteFlyrr.setIsFavorite(favoriteDTO
										.getIsFavorite());
								this.favoriteFlyrrRepository
										.save(favoriteFlyrr);
							} else {
								// favoriteFlyrr.setIsActive(Boolean.TRUE);
								favoriteFlyrr.setIsFavorite(favoriteDTO
										.getIsFavorite());
								favoriteFlyrr.setModifiedDate(DateUtil
										.getCurrentDate());
								this.favoriteFlyrrRepository
										.save(favoriteFlyrr);
							}
						}
					} else {

						throw new NotFoundFlyrrException(
								NotFound2.FlyrrSoldOut, Boolean.TRUE);
					}
				} else {
					throw new NotFoundFlyrrException(NotFound2.FlyrrIsExpire,
							Boolean.TRUE);
				}
			} else {
				throw new NotFoundFlyrrException(NotFound2.DeletedFlyrr,
						Boolean.TRUE);

			}

		} finally {
			logger.info("Favorite End");
		}

	}

	@SuppressWarnings("finally")
	@Override
	@Transactional(readOnly = true)
	public List<FavoriteListDTO> getFavoriteFlyrr(User user) {
		logger.info("Start getFavoriteFlyrr ");
		UserFlyRRResponseDTO userFlyRRResponseDTO = null;
		List<FavoriteListDTO> listFavouriteDTO = new ArrayList<>();
		List<BroadcastMessage> broadcastMessageDeletedList = new ArrayList<>();
		List<BroadcastMessage> broadcastMessageBuyingList = new ArrayList<>();
		try {
			List<BroadcastMessage> broadcastMessage = this.favoriteFlyrrRepository
					.getFavoriteBroadcastMessageByUser(user);

			for (BroadcastMessage listBroadcastMessage : broadcastMessage) {
				broadcastMessageDeletedList = this.broadcastMessageRepository
						.getBroadcastMessageByIsDelete(listBroadcastMessage);
				broadcastMessageBuyingList = this.broadcastMessageRepository
						.getBroadcastMessageByIsBuying(listBroadcastMessage);

				BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
						.getBuyerFlyrrForBroadcastMessage(listBroadcastMessage);

				FavoriteListDTO favoriteListDTO = new FavoriteListDTO();

				if (broadcastMessageDeletedList.size() == 0
						&& broadcastMessageBuyingList.size() == 0) {
					favoriteListDTO
							.setBroadcastId(listBroadcastMessage.getId());
					/*
					 * favoriteListDTO.setUserId(listBroadcastMessage.getUser().
					 * getId());
					 * favoriteListDTO.setFullName(listBroadcastMessage
					 * .getUser().getFullName());
					 */

					favoriteListDTO.setPrice(listBroadcastMessage.getPrice());
					favoriteListDTO.setTitle(listBroadcastMessage.getTitle());
					favoriteListDTO.setDescription(listBroadcastMessage
							.getDescription());
					favoriteListDTO.setLattitude(listBroadcastMessage
							.getLattitude());
					favoriteListDTO.setLongitude(listBroadcastMessage
							.getLongitude());
					favoriteListDTO.setExpireIn(listBroadcastMessage
							.getExpireIn());
					favoriteListDTO
							.setCategories(this.flyrrJobCategoriesRepository
									.getCategoriesByBroadcastMessage(listBroadcastMessage));
					favoriteListDTO
							.setImages(this.flyrrDocumentsRepository
									.getFlyrrDocumentNameByBroadcastMessageId(listBroadcastMessage));

					FavoriteFlyrr favoriteFlyrr = this.favoriteFlyrrRepository
							.getFavoriteFlyrrByBroadcastMessageId(listBroadcastMessage
									.getId());
					favoriteListDTO
							.setIsFavorite(favoriteFlyrr.getIsFavorite());
					userFlyRRResponseDTO = new UserFlyRRResponseDTO(
							listBroadcastMessage.getUser().getId(),
							listBroadcastMessage.getUser().getMobileNumber(),
							listBroadcastMessage.getUser().getFullName(),
							listBroadcastMessage.getUser().getProfilePicture(),
							listBroadcastMessage.getUser().getRate());
					listFavouriteDTO.add(favoriteListDTO);
				} else {
					throw new NotFoundFlyrrException(NotFound2.FlyRRNotFound,
							Boolean.TRUE);
				}
			}

			// Collections.sort(listFavouriteDTO, Collections.reverseOrder());
		} finally {
			return listFavouriteDTO;
		}
	}

	@Override
	@Transactional(rollbackFor = Throwable.class, isolation = Isolation.READ_UNCOMMITTED)
	public void deactivateAccount(User user) {
		logger.info("deactivateAccount of user ");
		User objUser = this.userRepository.getActiveUser(user);
		if (user != null) {
			objUser.setIsActive(false);
			this.userRepository.save(objUser);
		}
	}

	// Google maps distance
	public Double getDistanceBetweenTwoPoints(Double userlat1, Double userlon1,
			Double flyrrlat2, Double flyrrlon2) {
		if ((userlat1 == flyrrlat2) && (userlon1 == flyrrlon2)) {
			return 0.0;
		} else {
			int R = 6371; // Radius of the earth in km
			double dLat = this.deg2rad(flyrrlat2 - userlat1); // deg2rad below
			double dLon = this.deg2rad(flyrrlon2 - userlon1);
			double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
					+ Math.cos(deg2rad(userlat1)) * Math.cos(deg2rad(flyrrlat2))
					* Math.sin(dLon / 2) * Math.sin(dLon / 2);
			double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			double d = R * c; // Distance in km
			return Double.valueOf(this.df2.format(d));
		}
	}

	private double deg2rad(double d) {
		return d * (Math.PI / 180) * 1.5;
	}

	// simple direct Lat long distance
	double distance(double userlat1, double userlon1, double flyrrlat2,
			double flyrrlon2) {
		DecimalFormat df = new DecimalFormat("##.0");
		if ((userlat1 == flyrrlat2) && (userlon1 == flyrrlon2)) {
			return 0;
		} else {
			double theta = userlon1 - flyrrlon2;
			double dist = Math.sin(Math.toRadians(userlat1))
					* Math.sin(Math.toRadians(flyrrlat2))
					+ Math.cos(Math.toRadians(userlat1))
					* Math.cos(Math.toRadians(flyrrlat2))
					* Math.cos(Math.toRadians(theta));
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);
			dist = dist * 60 * 1.1515;

			dist = dist * 1.609344;
			/*
			 * if (unit.equals("K")) { dist = dist * 1.609344; } else if
			 * (unit.equals("N")) { dist = dist * 0.8684; }
			 */

			System.out.println("dist --> " + dist);
			return Double.valueOf(df.format(dist));
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<SearchFlyrrDetailsResponseDTO> nearByFlyrr(
			SearchFlyrrDTO searchFlyrrDTO, User user) {
		logger.info("Near By Flyrr Start");
		List<BroadcastMessage> broadcastMessages = null;
		List<BroadcastMessage> returnBroadCast = null;
		SearchFlyrrDetailsResponseDTO searchFlyrrDetailsResponseDTO = null;
		BroadcastMessage returnBroadcastMessaged = null;
		List<SearchFlyrrDetailsResponseDTO> flyrrDetails = new ArrayList<>();
		BroadcastMessage flyrrDetailsBroadcast = null;
		UserFlyRRResponseDTO userFlyRRResponseDTO = null;

		Map<String, List<SearchFlyrrDetailsResponseDTO>> broadCastMap = new HashMap<>();
		List<BroadcastMessageWithCodeDTO> searchFlyrrDetailsResponseDTOsList = new ArrayList<>();

		Double rangeInMile = 0.0;
		String lattitude = null;
		String longitude = null;
		Boolean isFavorite = false;
		Double minPrice = 0.0;
		Date dateCurrentDate = DateUtil.getCurrentDate();
		Double userRating = 0.0;
		Boolean IsNegotiable = BooleanUtils.toBoolean(searchFlyrrDTO
				.getIsNegotiable());
		try {

			if (searchFlyrrDTO.getBrodcastId() != null
					&& searchFlyrrDTO.getBrodcastId() > 0) {
				BroadcastMessage broadcastMessage = this
						.findBrodcastById(searchFlyrrDTO.getBrodcastId());
				if (broadcastMessage != null) {
					searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
					searchFlyrrDetailsResponseDTO
							.setBroadcastId(broadcastMessage.getId());

					searchFlyrrDetailsResponseDTO.setPrice(broadcastMessage
							.getPrice());
					searchFlyrrDetailsResponseDTO.setTitle(broadcastMessage
							.getTitle());
					searchFlyrrDetailsResponseDTO
							.setDescription(broadcastMessage.getDescription());
					searchFlyrrDetailsResponseDTO.setLattitude(broadcastMessage
							.getLattitude());
					searchFlyrrDetailsResponseDTO.setLongitude(broadcastMessage
							.getLongitude());
					searchFlyrrDetailsResponseDTO.setExpireIn(broadcastMessage
							.getExpireIn());
					searchFlyrrDetailsResponseDTO.setActive(broadcastMessage
							.getIsActive());
					searchFlyrrDetailsResponseDTO
							.setCreatedDate(broadcastMessage.getCreateDate());
					searchFlyrrDetailsResponseDTO
							.setImages(this.flyrrDocumentsRepository
									.getFlyrrDocumentNameByBroadcastMessageId(broadcastMessage));
					searchFlyrrDetailsResponseDTO
							.setCategories(this.flyrrJobCategoriesRepository
									.getCategoriesByBroadcastMessage(broadcastMessage));

					// searchFlyrrDetailsResponseDTO.setFullName(broadcastMessage.getUser().getFullName());
					searchFlyrrDetailsResponseDTO
							.setCreatedDate(broadcastMessage.getCreateDate());
					searchFlyrrDetailsResponseDTO
							.setIsNegotiable(broadcastMessage.getIsNegotiable());
					searchFlyrrDetailsResponseDTO.setIsBuying(broadcastMessage
							.getIsBuying());
					searchFlyrrDetailsResponseDTO.setIsDelete(broadcastMessage
							.getIsDelete());
					userFlyRRResponseDTO = new UserFlyRRResponseDTO(
							broadcastMessage.getUser().getId(),
							broadcastMessage.getUser().getMobileNumber(),
							broadcastMessage.getUser().getFullName(),
							broadcastMessage.getUser().getProfilePicture(),
							broadcastMessage.getUser().getRate());
					searchFlyrrDetailsResponseDTO.setUser(userFlyRRResponseDTO);
					flyrrDetails.add(searchFlyrrDetailsResponseDTO);
					return flyrrDetails;
				} else {

					throw new NotFoundFlyrrException(NotFound2.FlyRRNotFound,
							Boolean.TRUE);

				}
			} else {
				if (searchFlyrrDTO.getRangeInMile() == null
						|| searchFlyrrDTO.getRangeInMile() <= 0.0) {
					if (searchFlyrrDTO.getPrice() != null
							&& !searchFlyrrDTO.getPrice().equals("")) {
						broadcastMessages = this.broadcastMessageRepository
								.findBroadcastMessageByPrice(
										searchFlyrrDTO.getPrice(), user);
					}
				} else if (searchFlyrrDTO.getRangeInMile() != null
						&& searchFlyrrDTO.getRangeInMile() > 0.0) {
					lattitude = searchFlyrrDTO.getLattitude();
					longitude = searchFlyrrDTO.getLongitude();
					rangeInMile = searchFlyrrDTO.getRangeInMile();
					/*
					 * if(searchFlyrrDTO.getRangeInMile() != null) { rangeInMile
					 * = searchFlyrrDTO.getRangeInMile(); } else { rangeInMile =
					 * this
					 * .distance(Double.valueOf(user.getLattitude()),Double.
					 * valueOf (user.getLongitude())
					 * ,Double.valueOf(searchFlyrrDTO.getLattitude
					 * ()),Double.valueOf(searchFlyrrDTO.getLongitude())); }
					 */

				}

				else if (searchFlyrrDTO.getLattitude() != null
						&& searchFlyrrDTO.getLongitude() != null
						&& !searchFlyrrDTO.getLattitude().equals("")
						&& !searchFlyrrDTO.getLongitude().equals("")) {
					lattitude = searchFlyrrDTO.getLattitude();
					longitude = searchFlyrrDTO.getLongitude();
					if (searchFlyrrDTO.getRangeInMile() != null
							&& searchFlyrrDTO.getRangeInMile() > 0.0) {
						rangeInMile = searchFlyrrDTO.getRangeInMile();
					} else {

						rangeInMile = searchFlyrrDTO.getRangeInMile();
						/*
						 * if(searchFlyrrDTO.getRangeInMile() != null) {
						 * 
						 * } else { rangeInMile =
						 * this.distance(Double.valueOf(user
						 * .getLattitude()),Double.valueOf(user.getLongitude())
						 * ,
						 * Double.valueOf(searchFlyrrDTO.getLattitude()),Double.
						 * valueOf(searchFlyrrDTO.getLongitude())); }
						 */
					}
				} else {
					broadcastMessages = null;
				}
				if (lattitude != null && longitude != null
						&& !lattitude.equals("") && !longitude.equals("")) {
					if ((searchFlyrrDTO.getSearchWord() != null && !searchFlyrrDTO
							.getSearchWord().equals(""))
							&& (searchFlyrrDTO.getRangeInMile() == null || searchFlyrrDTO
									.getRangeInMile().compareTo(0.0) <= 0)) {
						broadcastMessages = this.broadcastMessageRepository
								.findBroadcastBySearchWithNoRange(user.getId(),
										dateCurrentDate);
					} else if (searchFlyrrDTO.getCategories().size() > 0
							&& searchFlyrrDTO.getSearchWord() != null) {
						broadcastMessages = this.flyrrJobCategoriesRepository
								.getBroadcastMessageByCategoriesListAndSearchWords(searchFlyrrDTO
										.getCategories(),searchFlyrrDTO
										.getSearchWord(),user);
					}
					else if (searchFlyrrDTO.getSearchWord() != null  && !searchFlyrrDTO.getSearchWord().equals("")) {
						broadcastMessages = this.flyrrJobCategoriesRepository
								.getBroadcastMessageBySearchWords(searchFlyrrDTO
										.getSearchWord(),user);
					}
					else if (searchFlyrrDTO.getPrice() != null
							&& searchFlyrrDTO.getPrice() == -1
							&& searchFlyrrDTO.getRangeInMile() != null
							&& searchFlyrrDTO.getRangeInMile() > 0.0) {
						if (searchFlyrrDTO.getIsNegotiable() == -1) {
							broadcastMessages = this.broadcastMessageRepository
									.getAllBroadcastMessageIdByWithoutPrice(
											lattitude, longitude, rangeInMile,
											user, dateCurrentDate);
						}

						else {
							// all data is coming without filter
							broadcastMessages = this.broadcastMessageRepository
									.getAllBroadcastMessageIdByWithoutPriceWithNegotiable(
											IsNegotiable, lattitude, longitude,
											rangeInMile, user, dateCurrentDate);

						}
					} else if (searchFlyrrDTO.getPrice() != null
							&& searchFlyrrDTO.getPrice() > 0) {
						if ((searchFlyrrDTO.getSearchWord() != null || !searchFlyrrDTO
								.getSearchWord().equals(""))
								&& (searchFlyrrDTO.getRangeInMile() == null || searchFlyrrDTO
										.getRangeInMile().compareTo(0.0) <= 0)) {
							broadcastMessages = this.broadcastMessageRepository
									.findBroadcastMessageByPrice(
											searchFlyrrDTO.getPrice(), user);
						}

						else {

							broadcastMessages = this.broadcastMessageRepository
									.getAllBroadcastMessageIdByPrice(
											searchFlyrrDTO.getPrice(),
											IsNegotiable, lattitude, longitude,
											rangeInMile, user, dateCurrentDate);
						}
					}
				}
			}
			if (broadcastMessages != null && broadcastMessages.size() > 0) {
				returnBroadCast = new ArrayList<>();
				for (BroadcastMessage broadcastMessage2 : broadcastMessages) {
					if (searchFlyrrDTO.getExpireIn() != null
							&& searchFlyrrDTO.getExpireIn() > 0.0) {
						// Date dateCurrentDate=DateUtil.getCurrentDate();
						Date dateExpireIn = new Date(
								searchFlyrrDTO.getExpireIn());

						List<BroadcastMessage> broadcastMessageExpire = this.broadcastMessageRepository
								.findOneByBroadcastMessageByExpireIn(
										dateCurrentDate, broadcastMessage2,
										IsNegotiable);

						if (broadcastMessageExpire != null) {
							returnBroadCast.add(broadcastMessage2);
						}

						else {
							throw new NotFoundException(NotFound.FlyrrIsExpire,
									Boolean.TRUE);

						}

					} else {
						returnBroadCast.add(broadcastMessage2);
					}
				}
			}

			if (returnBroadCast != null && returnBroadCast.size() > 0) {

				flyrrDetails = new ArrayList<>();
				// returnSearchFlyrrDetailsResponseDTO = new ArrayList<>();
				BroadcastMessage objBroadCastMessage = null;
				for (ListIterator<BroadcastMessage> broadcastList = returnBroadCast
						.listIterator(); broadcastList.hasNext();) {
					BroadcastMessage broadcastMessage = broadcastList.next();

					FavoriteFlyrr favoriteFlyrr = this.favoriteFlyrrRepository
							.findOneByBroadcastMessageAndUserAndIsFavoriteAndIsActive(
									broadcastMessage, user, Boolean.TRUE,
									Boolean.TRUE);

					if (favoriteFlyrr != null) {
						isFavorite = Boolean.TRUE;
					} else {
						isFavorite = Boolean.FALSE;
					}

					if (searchFlyrrDTO.getCategories() != null
							&& searchFlyrrDTO.getCategories().size() > 0
							&& searchFlyrrDTO.getSearchWord() != null
							&& !searchFlyrrDTO.getSearchWord().equals("")) {

						for (String category : searchFlyrrDTO.getCategories()) {

							Categories categories = this.flyrrCategoriesRepository
									.findByCategories(category);
							double rangeInMiles = 0.0;
							if (categories != null) {

								String searchWords = searchFlyrrDTO
										.getSearchWord().replaceAll("\\s", "");

								objBroadCastMessage = this.flyrrJobCategoriesRepository
										.getBroadcastMessageByCategoriesAndSearch(
												broadcastMessage, categories,
												searchWords);

								if (objBroadCastMessage != null) {
									if (IsNegotiable == true) {
										BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
												.getBuyerFlyrrForNearbyAndBroadcastMessage(
														objBroadCastMessage,
														user);
										if (buyerFlyrr == null) {
											searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
											searchFlyrrDetailsResponseDTO
													.setBroadcastId(objBroadCastMessage
															.getId());
											searchFlyrrDetailsResponseDTO
													.setPrice(objBroadCastMessage
															.getPrice());
											searchFlyrrDetailsResponseDTO
													.setTitle(objBroadCastMessage
															.getTitle());
											searchFlyrrDetailsResponseDTO
													.setDescription(objBroadCastMessage
															.getDescription());
											searchFlyrrDetailsResponseDTO
													.setLattitude(objBroadCastMessage
															.getLattitude());
											searchFlyrrDetailsResponseDTO
													.setLongitude(objBroadCastMessage
															.getLongitude());
											searchFlyrrDetailsResponseDTO
													.setImages(this.flyrrDocumentsRepository
															.getFlyrrDocumentNameByBroadcastMessageId(objBroadCastMessage));
											searchFlyrrDetailsResponseDTO
													.setExpireIn(objBroadCastMessage
															.getExpireIn());

											searchFlyrrDetailsResponseDTO
													.setCreatedDate(objBroadCastMessage
															.getCreateDate());

											searchFlyrrDetailsResponseDTO
													.setCategories(this.flyrrJobCategoriesRepository
															.getCategoriesByBroadcastMessage(objBroadCastMessage));
											searchFlyrrDetailsResponseDTO
													.setIsNegotiable(objBroadCastMessage
															.getIsNegotiable());
											searchFlyrrDetailsResponseDTO
													.setIsFavorite(isFavorite);

											if (objBroadCastMessage.getUser()
													.getRate() != null
													&& objBroadCastMessage
															.getUser()
															.getRate() <= 0) {
												userRating = 0.0;
												// searchFlyrrDetailsResponseDTO.setRate(0.0);
											} else {
												userRating = objBroadCastMessage
														.getUser().getRate();
											}
											userFlyRRResponseDTO = new UserFlyRRResponseDTO(
													objBroadCastMessage
															.getUser().getId(),
													objBroadCastMessage
															.getUser()
															.getMobileNumber(),
													objBroadCastMessage
															.getUser()
															.getFullName(),
													objBroadCastMessage
															.getUser()
															.getProfilePicture(),
													userRating);

											rangeInMiles = this
													.getDistanceBetweenTwoPoints(
															Double.valueOf(user
																	.getLattitude()),
															Double.valueOf(user
																	.getLongitude()),
															Double.valueOf(objBroadCastMessage
																	.getLattitude()),
															Double.valueOf(objBroadCastMessage
																	.getLongitude()));

											searchFlyrrDetailsResponseDTO
													.setRangeInMile(rangeInMiles);
											searchFlyrrDetailsResponseDTO
													.setUser(userFlyRRResponseDTO);
											flyrrDetails
													.add(searchFlyrrDetailsResponseDTO);
										}
									} else {
										BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
												.getBuyerFlyrrForNearbyAndBroadcastMessageAndIsNegotiable(
														objBroadCastMessage,
														user);
										if (buyerFlyrr == null) {
											searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
											searchFlyrrDetailsResponseDTO
													.setBroadcastId(objBroadCastMessage
															.getId());
											searchFlyrrDetailsResponseDTO
													.setPrice(objBroadCastMessage
															.getPrice());
											searchFlyrrDetailsResponseDTO
													.setTitle(objBroadCastMessage
															.getTitle());
											searchFlyrrDetailsResponseDTO
													.setDescription(objBroadCastMessage
															.getDescription());
											searchFlyrrDetailsResponseDTO
													.setLattitude(objBroadCastMessage
															.getLattitude());
											searchFlyrrDetailsResponseDTO
													.setLongitude(objBroadCastMessage
															.getLongitude());
											searchFlyrrDetailsResponseDTO
													.setImages(this.flyrrDocumentsRepository
															.getFlyrrDocumentNameByBroadcastMessageId(objBroadCastMessage));
											searchFlyrrDetailsResponseDTO
													.setExpireIn(objBroadCastMessage
															.getExpireIn());

											searchFlyrrDetailsResponseDTO
													.setCreatedDate(objBroadCastMessage
															.getCreateDate());

											searchFlyrrDetailsResponseDTO
													.setCategories(this.flyrrJobCategoriesRepository
															.getCategoriesByBroadcastMessage(objBroadCastMessage));
											searchFlyrrDetailsResponseDTO
													.setIsNegotiable(objBroadCastMessage
															.getIsNegotiable());
											searchFlyrrDetailsResponseDTO
													.setIsFavorite(isFavorite);

											if (objBroadCastMessage.getUser()
													.getRate() != null
													&& objBroadCastMessage
															.getUser()
															.getRate() <= 0) {
												userRating = 0.0;
											} else {
												userRating = objBroadCastMessage
														.getUser().getRate();
											}

											rangeInMiles = this
													.getDistanceBetweenTwoPoints(
															Double.valueOf(user
																	.getLattitude()),
															Double.valueOf(user
																	.getLongitude()),
															Double.valueOf(objBroadCastMessage
																	.getLattitude()),
															Double.valueOf(objBroadCastMessage
																	.getLongitude()));
											searchFlyrrDetailsResponseDTO
													.setRangeInMile(rangeInMiles);
											userFlyRRResponseDTO = new UserFlyRRResponseDTO(
													objBroadCastMessage
															.getUser().getId(),
													objBroadCastMessage
															.getUser()
															.getMobileNumber(),
													objBroadCastMessage
															.getUser()
															.getFullName(),
													objBroadCastMessage
															.getUser()
															.getProfilePicture(),
													userRating);
											searchFlyrrDetailsResponseDTO
													.setUser(userFlyRRResponseDTO);
											flyrrDetails
													.add(searchFlyrrDetailsResponseDTO);
										}
									}

								}

							}
						}

						// flyrrDetails.add(searchFlyrrDetailsResponseDTO);
					}

					else if ((searchFlyrrDTO.getSearchWord() == null || searchFlyrrDTO
							.getSearchWord().equals(""))
							&& searchFlyrrDTO.getCategories() != null
							&& searchFlyrrDTO.getCategories().size() > 0) {

						for (String category : searchFlyrrDTO.getCategories()) {

							Categories categories = this.flyrrCategoriesRepository
									.findByCategories(category);
							if (categories != null) {
								objBroadCastMessage = this.flyrrJobCategoriesRepository
										.getBroadcastMessageByCategories(
												broadcastMessage, categories);
								double rangeInMiles = 0.0;
								if (objBroadCastMessage != null) {

									if (IsNegotiable == true) {

										BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
												.getBuyerFlyrrForNearbyAndBroadcastMessage(
														objBroadCastMessage,
														user);
										if (buyerFlyrr == null) {
											searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
											searchFlyrrDetailsResponseDTO
													.setBroadcastId(objBroadCastMessage
															.getId());
											searchFlyrrDetailsResponseDTO
													.setPrice(objBroadCastMessage
															.getPrice());
											searchFlyrrDetailsResponseDTO
													.setTitle(objBroadCastMessage
															.getTitle());
											searchFlyrrDetailsResponseDTO
													.setDescription(objBroadCastMessage
															.getDescription());
											searchFlyrrDetailsResponseDTO
													.setLattitude(objBroadCastMessage
															.getLattitude());
											searchFlyrrDetailsResponseDTO
													.setLongitude(objBroadCastMessage
															.getLongitude());
											searchFlyrrDetailsResponseDTO
													.setImages(this.flyrrDocumentsRepository
															.getFlyrrDocumentNameByBroadcastMessageId(objBroadCastMessage));
											searchFlyrrDetailsResponseDTO
													.setExpireIn(objBroadCastMessage
															.getExpireIn());

											searchFlyrrDetailsResponseDTO
													.setCategories(this.flyrrJobCategoriesRepository
															.getCategoriesByBroadcastMessage(objBroadCastMessage));
											searchFlyrrDetailsResponseDTO
													.setIsNegotiable(objBroadCastMessage
															.getIsNegotiable());
											searchFlyrrDetailsResponseDTO
													.setIsFavorite(isFavorite);

											if (objBroadCastMessage.getUser()
													.getRate() != null
													&& objBroadCastMessage
															.getUser()
															.getRate() <= 0) {
												userRating = 0.0;
											} else {
												userRating = objBroadCastMessage
														.getUser().getRate();
											}
											searchFlyrrDetailsResponseDTO
													.setActive(objBroadCastMessage
															.getIsActive());
											searchFlyrrDetailsResponseDTO
													.setCreatedDate(objBroadCastMessage
															.getCreateDate());
											searchFlyrrDetailsResponseDTO
													.setIsBuying(objBroadCastMessage
															.getIsBuying());
											searchFlyrrDetailsResponseDTO
													.setIsDelete(broadcastMessage
															.getIsDelete());
											rangeInMiles = this
													.getDistanceBetweenTwoPoints(
															Double.valueOf(user
																	.getLattitude()),
															Double.valueOf(user
																	.getLongitude()),
															Double.valueOf(objBroadCastMessage
																	.getLattitude()),
															Double.valueOf(objBroadCastMessage
																	.getLongitude()));
											searchFlyrrDetailsResponseDTO
													.setRangeInMile(rangeInMiles);
											userFlyRRResponseDTO = new UserFlyRRResponseDTO(
													objBroadCastMessage
															.getUser().getId(),
													objBroadCastMessage
															.getUser()
															.getMobileNumber(),
													objBroadCastMessage
															.getUser()
															.getFullName(),
													objBroadCastMessage
															.getUser()
															.getProfilePicture(),
													userRating);
											searchFlyrrDetailsResponseDTO
													.setUser(userFlyRRResponseDTO);
											flyrrDetails
													.add(searchFlyrrDetailsResponseDTO);

										}
									} else {
										BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
												.getBuyerFlyrrForNearbyAndBroadcastMessageAndIsNegotiable(
														objBroadCastMessage,
														user);
										if (buyerFlyrr == null) {
											searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
											searchFlyrrDetailsResponseDTO
													.setBroadcastId(objBroadCastMessage
															.getId());
											searchFlyrrDetailsResponseDTO
													.setPrice(objBroadCastMessage
															.getPrice());
											searchFlyrrDetailsResponseDTO
													.setTitle(objBroadCastMessage
															.getTitle());
											searchFlyrrDetailsResponseDTO
													.setDescription(objBroadCastMessage
															.getDescription());
											searchFlyrrDetailsResponseDTO
													.setLattitude(objBroadCastMessage
															.getLattitude());
											searchFlyrrDetailsResponseDTO
													.setLongitude(objBroadCastMessage
															.getLongitude());
											searchFlyrrDetailsResponseDTO
													.setImages(this.flyrrDocumentsRepository
															.getFlyrrDocumentNameByBroadcastMessageId(objBroadCastMessage));
											searchFlyrrDetailsResponseDTO
													.setExpireIn(objBroadCastMessage
															.getExpireIn());

											searchFlyrrDetailsResponseDTO
													.setCreatedDate(objBroadCastMessage
															.getCreateDate());

											searchFlyrrDetailsResponseDTO
													.setCategories(this.flyrrJobCategoriesRepository
															.getCategoriesByBroadcastMessage(objBroadCastMessage));
											searchFlyrrDetailsResponseDTO
													.setIsNegotiable(objBroadCastMessage
															.getIsNegotiable());
											searchFlyrrDetailsResponseDTO
													.setIsFavorite(isFavorite);

											if (objBroadCastMessage.getUser()
													.getRate() != null
													&& objBroadCastMessage
															.getUser()
															.getRate() <= 0) {
												userRating = 0.0;
											} else {
												userRating = objBroadCastMessage
														.getUser().getRate();
											}
											rangeInMiles = this
													.getDistanceBetweenTwoPoints(
															Double.valueOf(user
																	.getLattitude()),
															Double.valueOf(user
																	.getLongitude()),
															Double.valueOf(objBroadCastMessage
																	.getLattitude()),
															Double.valueOf(objBroadCastMessage
																	.getLongitude()));
											searchFlyrrDetailsResponseDTO
													.setRangeInMile(rangeInMiles);
											userFlyRRResponseDTO = new UserFlyRRResponseDTO(
													objBroadCastMessage
															.getUser().getId(),
													objBroadCastMessage
															.getUser()
															.getMobileNumber(),
													objBroadCastMessage
															.getUser()
															.getFullName(),
													objBroadCastMessage
															.getUser()
															.getProfilePicture(),
													userRating);
											searchFlyrrDetailsResponseDTO
													.setUser(userFlyRRResponseDTO);
											flyrrDetails
													.add(searchFlyrrDetailsResponseDTO);
										}

									}
									// flyrrDetails.add(searchFlyrrDetailsResponseDTO);

								}

							}

						}

					}

					else if (searchFlyrrDTO.getSearchWord() != null
							&& !searchFlyrrDTO.getSearchWord().equals("")
							&& (searchFlyrrDTO.getCategories() == null || searchFlyrrDTO
									.getCategories().size() == 0)) {
						double rangeInMiles = 0.0;
						String searchWords = searchFlyrrDTO.getSearchWord()
								.replaceAll("\\s", "");
						objBroadCastMessage = this.flyrrJobCategoriesRepository
								.getBroadcastMessageBySearch(broadcastMessage,
										searchWords);

						if (objBroadCastMessage != null) {
							if (IsNegotiable == true) {
								BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
										.getBuyerFlyrrForNearbyAndBroadcastMessage(
												objBroadCastMessage, user);
								if (buyerFlyrr == null) {
									searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
									searchFlyrrDetailsResponseDTO
											.setBroadcastId(objBroadCastMessage
													.getId());

									searchFlyrrDetailsResponseDTO
											.setPrice(objBroadCastMessage
													.getPrice());
									searchFlyrrDetailsResponseDTO
											.setTitle(objBroadCastMessage
													.getTitle());
									searchFlyrrDetailsResponseDTO
											.setDescription(objBroadCastMessage
													.getDescription());
									searchFlyrrDetailsResponseDTO
											.setLattitude(objBroadCastMessage
													.getLattitude());
									searchFlyrrDetailsResponseDTO
											.setLongitude(objBroadCastMessage
													.getLongitude());
									searchFlyrrDetailsResponseDTO
											.setImages(this.flyrrDocumentsRepository
													.getFlyrrDocumentNameByBroadcastMessageId(objBroadCastMessage));
									searchFlyrrDetailsResponseDTO
											.setExpireIn(objBroadCastMessage
													.getExpireIn());

									searchFlyrrDetailsResponseDTO
											.setCreatedDate(objBroadCastMessage
													.getCreateDate());

									searchFlyrrDetailsResponseDTO
											.setCategories(this.flyrrJobCategoriesRepository
													.getCategoriesByBroadcastMessage(objBroadCastMessage));
									searchFlyrrDetailsResponseDTO
											.setIsNegotiable(objBroadCastMessage
													.getIsNegotiable());
									searchFlyrrDetailsResponseDTO
											.setIsFavorite(isFavorite);

									if (objBroadCastMessage.getUser().getRate() != null
											&& objBroadCastMessage.getUser()
													.getRate() <= 0) {
										userRating = 0.0;
									} else {
										userRating = objBroadCastMessage
												.getUser().getRate();
									}
									rangeInMiles = this
											.getDistanceBetweenTwoPoints(
													Double.valueOf(user
															.getLattitude()),
													Double.valueOf(user
															.getLongitude()),
													Double.valueOf(objBroadCastMessage
															.getLattitude()),
													Double.valueOf(objBroadCastMessage
															.getLongitude()));
									searchFlyrrDetailsResponseDTO
											.setRangeInMile(rangeInMiles);
									userFlyRRResponseDTO = new UserFlyRRResponseDTO(
											objBroadCastMessage.getUser()
													.getId(),
											objBroadCastMessage.getUser()
													.getMobileNumber(),
											objBroadCastMessage.getUser()
													.getFullName(),
											objBroadCastMessage.getUser()
													.getProfilePicture(),
											userRating);
									searchFlyrrDetailsResponseDTO
											.setUser(userFlyRRResponseDTO);
									flyrrDetails
											.add(searchFlyrrDetailsResponseDTO);
								}
							} else {
								BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
										.getBuyerFlyrrForNearbyAndBroadcastMessageAndIsNegotiable(
												objBroadCastMessage, user);
								if (buyerFlyrr == null) {
									searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
									searchFlyrrDetailsResponseDTO
											.setBroadcastId(objBroadCastMessage
													.getId());

									searchFlyrrDetailsResponseDTO
											.setPrice(objBroadCastMessage
													.getPrice());
									searchFlyrrDetailsResponseDTO
											.setTitle(objBroadCastMessage
													.getTitle());
									searchFlyrrDetailsResponseDTO
											.setDescription(objBroadCastMessage
													.getDescription());
									searchFlyrrDetailsResponseDTO
											.setLattitude(objBroadCastMessage
													.getLattitude());
									searchFlyrrDetailsResponseDTO
											.setLongitude(objBroadCastMessage
													.getLongitude());
									searchFlyrrDetailsResponseDTO
											.setImages(this.flyrrDocumentsRepository
													.getFlyrrDocumentNameByBroadcastMessageId(objBroadCastMessage));
									searchFlyrrDetailsResponseDTO
											.setExpireIn(objBroadCastMessage
													.getExpireIn());

									searchFlyrrDetailsResponseDTO
											.setCreatedDate(objBroadCastMessage
													.getCreateDate());

									searchFlyrrDetailsResponseDTO
											.setCategories(this.flyrrJobCategoriesRepository
													.getCategoriesByBroadcastMessage(objBroadCastMessage));

									searchFlyrrDetailsResponseDTO
											.setIsNegotiable(objBroadCastMessage
													.getIsNegotiable());
									searchFlyrrDetailsResponseDTO
											.setIsFavorite(isFavorite);

									if (objBroadCastMessage.getUser().getRate() != null
											&& objBroadCastMessage.getUser()
													.getRate() <= 0) {
										userRating = 0.0;
									} else {
										userRating = objBroadCastMessage
												.getUser().getRate();
									}
									rangeInMiles = this
											.getDistanceBetweenTwoPoints(
													Double.valueOf(user
															.getLattitude()),
													Double.valueOf(user
															.getLongitude()),
													Double.valueOf(objBroadCastMessage
															.getLattitude()),
													Double.valueOf(objBroadCastMessage
															.getLongitude()));
									searchFlyrrDetailsResponseDTO
											.setRangeInMile(rangeInMiles);
									userFlyRRResponseDTO = new UserFlyRRResponseDTO(
											objBroadCastMessage.getUser()
													.getId(),
											objBroadCastMessage.getUser()
													.getMobileNumber(),
											objBroadCastMessage.getUser()
													.getFullName(),
											objBroadCastMessage.getUser()
													.getProfilePicture(),
											userRating);
									searchFlyrrDetailsResponseDTO
											.setUser(userFlyRRResponseDTO);

									flyrrDetails
											.add(searchFlyrrDetailsResponseDTO);
								}
							}
						}
					}

					else {
						objBroadCastMessage = this.flyrrJobCategoriesRepository
								.getFlyrrJobCategoriesByBroadcastMessage(broadcastMessage);
						double rangeInMiles = 0.0;
						if (IsNegotiable == true) {
							BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
									.getBuyerFlyrrForNearbyAndBroadcastMessage(
											objBroadCastMessage, user);

							if (buyerFlyrr == null) {
								searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
								searchFlyrrDetailsResponseDTO
										.setBroadcastId(objBroadCastMessage
												.getId());

								searchFlyrrDetailsResponseDTO
										.setPrice(objBroadCastMessage
												.getPrice());
								searchFlyrrDetailsResponseDTO
										.setTitle(objBroadCastMessage
												.getTitle());
								searchFlyrrDetailsResponseDTO
										.setDescription(objBroadCastMessage
												.getDescription());
								searchFlyrrDetailsResponseDTO
										.setLattitude(objBroadCastMessage
												.getLattitude());
								searchFlyrrDetailsResponseDTO
										.setLongitude(objBroadCastMessage
												.getLongitude());
								searchFlyrrDetailsResponseDTO
										.setImages(this.flyrrDocumentsRepository
												.getFlyrrDocumentNameByBroadcastMessageId(objBroadCastMessage));
								searchFlyrrDetailsResponseDTO
										.setExpireIn(objBroadCastMessage
												.getExpireIn());
								searchFlyrrDetailsResponseDTO
										.setActive(objBroadCastMessage
												.getIsActive());
								searchFlyrrDetailsResponseDTO
										.setIsBuying(objBroadCastMessage
												.getIsBuying());
								searchFlyrrDetailsResponseDTO
										.setIsDelete(objBroadCastMessage
												.getIsDelete());
								searchFlyrrDetailsResponseDTO
										.setCreatedDate(objBroadCastMessage
												.getCreateDate());
								searchFlyrrDetailsResponseDTO
										.setCategories(this.flyrrJobCategoriesRepository
												.getCategoriesByBroadcastMessage(objBroadCastMessage));
								searchFlyrrDetailsResponseDTO
										.setIsNegotiable(objBroadCastMessage
												.getIsNegotiable());
								searchFlyrrDetailsResponseDTO
										.setIsFavorite(isFavorite);

								if (objBroadCastMessage.getUser().getRate() != null
										&& objBroadCastMessage.getUser()
												.getRate() <= 0) {
									userRating = 0.0;
								} else {
									userRating = objBroadCastMessage.getUser()
											.getRate();
								}
								rangeInMiles = this
										.getDistanceBetweenTwoPoints(
												Double.valueOf(user
														.getLattitude()),
												Double.valueOf(user
														.getLongitude()),
												Double.valueOf(objBroadCastMessage
														.getLattitude()),
												Double.valueOf(objBroadCastMessage
														.getLongitude()));
								searchFlyrrDetailsResponseDTO
										.setRangeInMile(rangeInMiles);
								userFlyRRResponseDTO = new UserFlyRRResponseDTO(
										objBroadCastMessage.getUser().getId(),
										objBroadCastMessage.getUser()
												.getMobileNumber(),
										objBroadCastMessage.getUser()
												.getFullName(),
										objBroadCastMessage.getUser()
												.getProfilePicture(),
										userRating);
								searchFlyrrDetailsResponseDTO
										.setUser(userFlyRRResponseDTO);
								flyrrDetails.add(searchFlyrrDetailsResponseDTO);
							}
						} else {
							BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
									.getBuyerFlyrrForNearbyAndBroadcastMessageAndIsNegotiable(
											objBroadCastMessage, user);
							if (buyerFlyrr == null) {
								searchFlyrrDetailsResponseDTO = new SearchFlyrrDetailsResponseDTO();
								searchFlyrrDetailsResponseDTO
										.setBroadcastId(objBroadCastMessage
												.getId());

								searchFlyrrDetailsResponseDTO
										.setPrice(objBroadCastMessage
												.getPrice());
								searchFlyrrDetailsResponseDTO
										.setTitle(objBroadCastMessage
												.getTitle());
								searchFlyrrDetailsResponseDTO
										.setDescription(objBroadCastMessage
												.getDescription());
								searchFlyrrDetailsResponseDTO
										.setLattitude(objBroadCastMessage
												.getLattitude());
								searchFlyrrDetailsResponseDTO
										.setLongitude(objBroadCastMessage
												.getLongitude());
								searchFlyrrDetailsResponseDTO
										.setImages(this.flyrrDocumentsRepository
												.getFlyrrDocumentNameByBroadcastMessageId(objBroadCastMessage));
								searchFlyrrDetailsResponseDTO
										.setExpireIn(objBroadCastMessage
												.getExpireIn());
								searchFlyrrDetailsResponseDTO
										.setCreatedDate(objBroadCastMessage
												.getCreateDate());
								searchFlyrrDetailsResponseDTO
										.setCategories(this.flyrrJobCategoriesRepository
												.getCategoriesByBroadcastMessage(objBroadCastMessage));
								searchFlyrrDetailsResponseDTO
										.setActive(objBroadCastMessage
												.getIsActive());
								searchFlyrrDetailsResponseDTO
										.setIsBuying(objBroadCastMessage
												.getIsBuying());
								searchFlyrrDetailsResponseDTO
										.setIsDelete(objBroadCastMessage
												.getIsDelete());

								searchFlyrrDetailsResponseDTO
										.setIsNegotiable(objBroadCastMessage
												.getIsNegotiable());
								searchFlyrrDetailsResponseDTO
										.setIsFavorite(isFavorite);

								if (objBroadCastMessage.getUser().getRate() != null
										&& objBroadCastMessage.getUser()
												.getRate() <= 0) {
									userRating = 0.0;
								} else {
									userRating = objBroadCastMessage.getUser()
											.getRate();
								}
								rangeInMiles = this
										.getDistanceBetweenTwoPoints(
												Double.valueOf(user
														.getLattitude()),
												Double.valueOf(user
														.getLongitude()),
												Double.valueOf(objBroadCastMessage
														.getLattitude()),
												Double.valueOf(objBroadCastMessage
														.getLongitude()));
								searchFlyrrDetailsResponseDTO
										.setRangeInMile(rangeInMiles);
								userFlyRRResponseDTO = new UserFlyRRResponseDTO(
										objBroadCastMessage.getUser().getId(),
										objBroadCastMessage.getUser()
												.getMobileNumber(),
										objBroadCastMessage.getUser()
												.getFullName(),
										objBroadCastMessage.getUser()
												.getProfilePicture(),
										userRating);
								searchFlyrrDetailsResponseDTO
										.setUser(userFlyRRResponseDTO);
								flyrrDetails.add(searchFlyrrDetailsResponseDTO);
							}
						}
					}
				}
			}
			HashSet<Object> seen = new HashSet<>();
			flyrrDetails.removeIf(e -> !seen.add(e.getBroadcastId()));
			return flyrrDetails;
			// return new
			// FlyrrDetailsListDTO(flyrrDetails.stream().distinct().collect(Collectors.toList()));
			/*
			 * if(flyrrDetails != null && flyrrDetails.size() > 0) {
			 * broadCastMap=flyrrDetails.stream()
			 * .collect(Collectors.groupingBy(
			 * SearchFlyrrDetailsResponseDTO::getLattitude));
			 * 
			 * }
			 * 
			 * broadCastMap.entrySet().forEach(entry -> { BroadcastMessage
			 * broadcastMessage=this.broadcastMessageRepository.
			 * findOneByBroadcastMessage(entry.getKey(),entry.getKey());
			 * 
			 * BroadcastMessageWithCodeDTO searchFlyrrDetailsResponseTempDTO =
			 * new BroadcastMessageWithCodeDTO();
			 * searchFlyrrDetailsResponseTempDTO.setLattitude(entry.getKey());
			 * searchFlyrrDetailsResponseTempDTO.setLongitude(entry.getKey());
			 * searchFlyrrDetailsResponseTempDTO
			 * .setFlyrrDetailsDTOs(entry.getValue());
			 * searchFlyrrDetailsResponseDTOsList
			 * .add(searchFlyrrDetailsResponseTempDTO);
			 * 
			 * });
			 */

		} finally {
			logger.info("end getUserByIds");
		}
		// return new FlyrrDetailsListDTO(searchFlyrrDetailsResponseDTOsList);
	}

	@Override
	public void deleteFlyrr(DeleteDTO deleteDTO, User user) {

		logger.info("Delete Flyrr Process start");
		try {
			BroadcastMessage broadcastMessage = null;
			broadcastMessage = this.broadcastMessageRepository
					.findOne(deleteDTO.getBroadcastId());

			BuyerFlyrr buyerFlyrr = this.buyerFlyrrRepository
					.getBuyerFlyrrForBroadcastMessage(broadcastMessage);
			if (buyerFlyrr == null) {
				broadcastMessage.setIsActive(Boolean.FALSE);
				broadcastMessage.setIsDelete(Boolean.TRUE);
				broadcastMessage.setModifiedDate(DateUtil.getCurrentDate());
				this.broadcastMessageRepository.save(broadcastMessage);
			} else {
				throw new NotFoundFlyrrException(NotFound2.NotDeletedFlyrr,
						Boolean.TRUE);
			}
		} finally {
			logger.info("deleted flyrr end");
		}
	}

	@Override
	@Transactional(rollbackFor = { Exception.class }, isolation = Isolation.READ_COMMITTED)
	public List<BroadcastInterestedResponseDTO> interestedInBroadcast(
			User objUser, BroadcastMessageInterestedDTO intrestedInBroadcastDTO) {
		logger.info("interestedInBroadcast start");

		List<BroadcastInterestedResponseDTO> listBroadcastInterestedResponseDTO = new ArrayList<>();
		try {
			List<FavoriteFlyrr> favoriteFlyrrs = this.favoriteFlyrrRepository
					.getFavoriteFlyrrByBroadcastMessage(intrestedInBroadcastDTO
							.getBroadcastId());

			for (FavoriteFlyrr listBroadcastMessage : favoriteFlyrrs) {
				BroadcastInterestedResponseDTO broadcastInterestedResponseDTO = new BroadcastInterestedResponseDTO();
				broadcastInterestedResponseDTO
						.setBroadcastId(listBroadcastMessage
								.getBroadcastMessage().getId());
				broadcastInterestedResponseDTO.setUserId(listBroadcastMessage
						.getUser().getId());
				broadcastInterestedResponseDTO.setFullName(listBroadcastMessage
						.getUser().getFullName());
				broadcastInterestedResponseDTO
						.setProfilePicture(listBroadcastMessage.getUser()
								.getProfilePicture());
				broadcastInterestedResponseDTO.setDialCode(listBroadcastMessage
						.getUser().getDialCode());
				broadcastInterestedResponseDTO
						.setMobileNumber(listBroadcastMessage.getUser()
								.getMobileNumber());
				listBroadcastInterestedResponseDTO
						.add(broadcastInterestedResponseDTO);
			}
			return listBroadcastInterestedResponseDTO;
		}

		finally {
			logger.info("interestedInBroadcast end");
		}

	}

	@Override
	public void notification(NotificationDTO notificationDTO, User user) {
		// TODO Auto-generated method stub
		logger.info("Notification process start");

		try {
			user = this.userRepository.getUserByIdAndActiveNotifiaction(user
					.getId());

			if (user != null) {
				user.setIsEnableNotification(notificationDTO
						.getIsNotification());
				this.userRepository.save(user);
			} else {
				throw new NotFoundFlyrrException(
						NotFound2.InEnableNotification, Boolean.TRUE);
			}
		} finally {
			logger.info("Notification end");
		}

	}

	@Override
	public List<String> searchCategories(String searchKeyword, User user,
			HttpServletRequest request) {
		logger.info("Search categories Process start");
		List<String> categories = null;
		try {
			if (searchKeyword != null && searchKeyword != "") {
				categories = this.categoriesRepository
						.findCategoriesBySearch(searchKeyword);

			} else {
				categories = this.categoriesRepository.findCategories();
			}
			return categories;
		} finally {
			logger.info("Search categories Process  end");
		}

	}

	@Override
	@Transactional
	public void ratingAndFeedback(RatingFeedbackDTO ratingFeedbackDTO, User user) {
		// TODO Auto-generated method stub

		logger.info("Rating and Feedback process start ..!!");
		try {
			BroadcastMessage broadcastMessage = this.broadcastMessageRepository
					.findOne(ratingFeedbackDTO.getBroadcastId());

			RatingFeedback ratingFeedback = this.ratingFeedbackRepository
					.getRatingFeedbackByBroadcastMessageAndRatingByUser(
							broadcastMessage, user);

			if (ratingFeedback != null) {
				throw new NotFoundFlyrrException(NotFound2.RatingAndFeedback,
						Boolean.TRUE);
			}

			User rateToUser = this.userRepository.findOne(ratingFeedbackDTO
					.getUserId());
			ratingFeedback = new RatingFeedback();
			ratingFeedback.setBroadcastMessage(broadcastMessage);
			ratingFeedback.setRate(ratingFeedbackDTO.getRate());
			ratingFeedback.setFeedback(ratingFeedbackDTO.getFeedback());
			ratingFeedback.setRatingByUser(user);
			ratingFeedback.setRatingToUser(rateToUser);
			ratingFeedback.setCreatedDate(DateUtil.getCurrentDate());

			this.ratingFeedbackRepository.save(ratingFeedback);

			/* Update :- User Rate */
			Double averageRate = this.ratingFeedbackRepository
					.getAverageRatingByUser(rateToUser);
			if (averageRate != null) {
				this.userRepository.updateUserByRate(
						rateToUser.getId(),
						new BigDecimal(averageRate).setScale(1,
								RoundingMode.HALF_EVEN).doubleValue());

				// new BigDecimal(averageRate).setScale(1,
				// RoundingMode.HALF_EVEN).doubleValue());

			}
		} finally {
			logger.info("Rating and Feedback process end");
		}
	}

	@Override
	public List<RatingFeedbackDTO> getRatingAndFeedbackList(Long rateToUserId,
			User user, HttpServletRequest request) {
		List<RatingFeedbackDTO> listRatingFeedbackDTO = new ArrayList<>();
		try {
			User rateToUser = this.userRepository.findOne(rateToUserId);
			List<RatingFeedback> ratingFeedback = this.ratingFeedbackRepository
					.getRatingByUser(rateToUser);
			for (RatingFeedback listRatingFeedback : ratingFeedback) {
				RatingFeedbackDTO ratingFeedbackDTO = new RatingFeedbackDTO();
				ratingFeedbackDTO.setBroadcastId(listRatingFeedback
						.getBroadcastMessage().getId());
				ratingFeedbackDTO.setFeedback(listRatingFeedback.getFeedback());
				ratingFeedbackDTO.setRate(listRatingFeedback.getRate());
				ratingFeedbackDTO.setUserId(listRatingFeedback
						.getRatingByUser().getId());
				ratingFeedbackDTO.setFullName(listRatingFeedback
						.getRatingByUser().getFullName());
				ratingFeedbackDTO.setProfilePicture(listRatingFeedback
						.getRatingByUser().getProfilePicture());
				listRatingFeedbackDTO.add(ratingFeedbackDTO);
			}
		} finally {
		}
		return listRatingFeedbackDTO;
	}

	@Override
	public void reportFlyrr(ReportDTO reportDTO, User user,
			HttpServletRequest request) {
		// TODO Auto-generated method stub
		logger.info("Report process start");
		List<BroadcastMessage> broadcastMessageDeletedList = new ArrayList<>();
		List<BroadcastMessage> broadcastMessageExpireInList = new ArrayList<>();
		List<BroadcastMessage> broadcastMessageBuyingList = new ArrayList<>();
		Date dateCurrentDate = DateUtil.getCurrentDate();
		try {
			BroadcastMessage broadcastMessage = this.broadcastMessageRepository
					.findOne(reportDTO.getBroadcastId());

			Date dateExpireIn = (broadcastMessage.getExpireIn());

			// check Flyrr deleted or not..!!
			broadcastMessageDeletedList = this.broadcastMessageRepository
					.getBroadcastMessageByIsDelete(broadcastMessage);

			// check flyrr is Expire or not..!!
			broadcastMessageExpireInList = this.broadcastMessageRepository
					.getBroadcastMessageByIsExpireIn(broadcastMessage,
							dateCurrentDate);

			Integer broadcastCount = this.reportRepository
					.getBroadcastMessageCountById(broadcastMessage);

			User user1 = this.userRepository.findOne(reportDTO.getUserId());
			Report report = null;

			List<Report> reportList = this.reportRepository
					.getReportByBroadcastMessageAndUser(broadcastMessage, user1);

			if (broadcastMessageDeletedList.size() == 0) {
				if (broadcastMessageExpireInList.size() == 0) {

					if (broadcastCount <= 5) {
						if (reportList.size() != 0) {
							throw new NotFoundFlyrrException(
									NotFound2.ReportFlyrr, Boolean.TRUE);
						} else {
							report = new Report();
							report.setBroadcastMessage(broadcastMessage);
							report.setUser(user1);
							report.setCreatedDate(DateUtil.getCurrentDate());
							report.setModifiedDate(DateUtil.getCurrentDate());
							report.setComment(reportDTO.getComment());
							this.reportRepository.save(report);
						}
					} else {
						broadcastMessage.setIsDelete(true);
						this.broadcastMessageRepository.save(broadcastMessage);

						throw new NotFoundFlyrrException(
								NotFound2.CountFlyrrDeleted, Boolean.TRUE);
					}
				} else {
					throw new NotFoundFlyrrException(NotFound2.FlyrrIsExpire,
							Boolean.TRUE);
				}
			} else {
				throw new NotFoundFlyrrException(NotFound2.DeletedFlyrr,
						Boolean.TRUE);
			}

		} finally {
			logger.info("Report Process end..");
		}
	}

	@Override
	@Transactional
	public List<SellerFlyrrListDTO> getOpponentUserFlyrrList(Long userId,
			HttpServletRequest request) {
		double rangeInMiles = 0.0;
		List<SellerFlyrrListDTO> listBroadcastMessageUserDTO = new ArrayList<>();
		try {
			User objUser = this.userRepository.findOne(userId);
			List<BroadcastMessage> broadcastMessage = this.broadcastMessageRepository
					.getSellerByBroadcastMessageByUser(objUser);

			for (BroadcastMessage listBroadcastMessage : broadcastMessage) {
				SellerFlyrrListDTO broadcastMessageAndUserDTO = new SellerFlyrrListDTO();
				broadcastMessageAndUserDTO.setBroadcastId(listBroadcastMessage
						.getId());
				broadcastMessageAndUserDTO.setPrice(listBroadcastMessage
						.getPrice());
				broadcastMessageAndUserDTO.setTitle(listBroadcastMessage
						.getTitle());
				broadcastMessageAndUserDTO.setDescription(listBroadcastMessage
						.getDescription());
				broadcastMessageAndUserDTO.setLattitude(listBroadcastMessage
						.getLattitude());
				broadcastMessageAndUserDTO.setLongitude(listBroadcastMessage
						.getLongitude());
				broadcastMessageAndUserDTO.setExpireIn(listBroadcastMessage
						.getExpireIn());
				broadcastMessageAndUserDTO.setActive(listBroadcastMessage
						.getIsActive());
				broadcastMessageAndUserDTO
						.setCategories(this.flyrrJobCategoriesRepository
								.getCategoriesByBroadcastMessage(listBroadcastMessage));
				broadcastMessageAndUserDTO
						.setImages(this.flyrrDocumentsRepository
								.getFlyrrDocumentNameByBroadcastMessageId(listBroadcastMessage));
				broadcastMessageAndUserDTO.setIsBuying(listBroadcastMessage
						.getIsBuying());
				

				rangeInMiles=this.getDistanceBetweenTwoPoints(Double.valueOf(objUser.getLattitude()), Double.valueOf(objUser.getLongitude()),
						Double.valueOf(listBroadcastMessage.getLattitude()), Double.valueOf(listBroadcastMessage.getLongitude()));
				broadcastMessageAndUserDTO.setRangeInMile(rangeInMiles);
				listBroadcastMessageUserDTO.add(broadcastMessageAndUserDTO);
			}
		} finally {
			logger.info("OpponentUserFlyrr Process end..");
		}
		return listBroadcastMessageUserDTO;
	}

	@Override
	@Transactional(rollbackFor = Throwable.class, isolation = Isolation.READ_UNCOMMITTED)
	public Boolean deviceToken(DeviceTokenDTO deviceTokenDTO, User user) {
		logger.info("Device token Impl process start..!!");

		DeviceToken objDeviceToken = null;
		List<DeviceToken> listDeivceToken = null;
		try {
			listDeivceToken = this.deviceTokenRepository.getAllDeviceToken();
			if (listDeivceToken != null && listDeivceToken.size() > 0) {
				for (DeviceToken deviceTokens : listDeivceToken) {
					if (deviceTokens.getDeviceToken().equals(
							deviceTokenDTO.getDeviceToken())) {
						deviceTokens.setActive(false);
						deviceTokens.setCreatedDate(DateUtil.getCurrentDate());
						deviceTokens.setModifiedDate(DateUtil.getCurrentDate());
						this.deviceTokenRepository.save(deviceTokens);
					}

				}
			}

			objDeviceToken = this.deviceTokenRepository
					.getAllDeviceTokenByUser(user);

			if (objDeviceToken != null) {
				objDeviceToken.setCreatedDate(DateUtil.getCurrentDate());
				objDeviceToken.setModifiedDate(DateUtil.getCurrentDate());
				objDeviceToken.setActive(true);
				objDeviceToken.setDeviceToken(deviceTokenDTO.getDeviceToken());
				objDeviceToken.setDeviceType(deviceTokenDTO.getDeviceType());
				this.deviceTokenRepository.save(objDeviceToken);
				return true;
			} else {

				objDeviceToken = new DeviceToken();
				objDeviceToken.setCreatedDate(DateUtil.getCurrentDate());
				objDeviceToken.setModifiedDate(DateUtil.getCurrentDate());
				objDeviceToken.setDeviceToken(deviceTokenDTO.getDeviceToken());
				objDeviceToken.setDeviceType(deviceTokenDTO.getDeviceType());
				objDeviceToken.setActive(true);
				objDeviceToken.setUser(user);
				this.deviceTokenRepository.save(objDeviceToken);
				return false;
			}
		} finally {

		}

	}

	public BroadcastMessage findBrodcastById(Long id) {

		return broadcastMessageRepository.findBroadcastMessageById(id);
	}

	@Override
	public List<InterestedUserDto> getInterestedUserDetails(List<Long> userId,
			User user) {

		List<InterestedUserDto> profileDTOList = new ArrayList<InterestedUserDto>();
		try {
			logger.info("UserService_interestedUserDetail");
			InterestedUserDto profileDTO;

			for (Long id : userId) {
				profileDTO = this.userRepository.getInterestedUserProfile(id);
				profileDTOList.add(profileDTO);
			}
			if (profileDTOList != null && profileDTOList.size() > 0) {
				ListIterator<InterestedUserDto> userProfileDTOIterator = null;
				userProfileDTOIterator = profileDTOList.listIterator();
				while (userProfileDTOIterator.hasNext()) {
					InterestedUserDto objUserProfileDto = userProfileDTOIterator
							.next();
					objUserProfileDto.getProfilePicture();
				}
			} else {
				throw new NotFoundException(NotFound.UserNotFound, Boolean.TRUE);
			}
		} finally {

		}
		return profileDTOList;

	}

	/*
	 * @Override public BroadcastMessage getDetalisById(SearchFlyrrDTO dto) {
	 * 
	 * return
	 * this.broadcastMessageRepository.findBroadcastMessageById(dto.getId()); }
	 */
	/*
	 * @SuppressWarnings("unchecked") public void sendAndroidNotification(String
	 * title, String message,Long broId) { try { java.net.URL url = new
	 * URL(applicationProperties.getApiUrl()); HttpURLConnection connection =
	 * (HttpURLConnection) url.openConnection(); connection.setUseCaches(false);
	 * connection.setDoInput(true); connection.setDoOutput(true);
	 * 
	 * connection.setRequestMethod("POST");
	 * connection.setRequestProperty("Authorization", "key=" +
	 * applicationProperties.getApiKey());
	 * connection.setRequestProperty("Content-Type", "application/json");
	 * 
	 * JSONObject info = new JSONObject(); info.put("notificationTitle", title);
	 * info.put("message", message); info.put("NearFlyrr",broId);
	 * OutputStreamWriter wr = new
	 * OutputStreamWriter(connection.getOutputStream());
	 * System.out.println(info); wr.write(info.toString()); wr.flush();
	 * connection.getOutputStream(); } catch (Exception e) {
	 * 
	 * e.printStackTrace(); } }
	 */
	/*
	 * @Transactional
	 * 
	 * @Override public Boolean sendUserNotification(NotificationUserDto
	 * notifyUserDto,User user) { try { List<FCMUser> fcmUsers = null;
	 * List<BroadcastMessage> nearMeFlyrr = null; nearMeFlyrr =
	 * broadcastMessageRepository
	 * .findBroadcastMessageNearMe(user.getLattitude(), user.getLongitude());
	 * List<DeviceToken> devices = deviceTokenRepository.getAllDeviceToken(); if
	 * (devices != null) { DeviceToken token =
	 * deviceTokenRepository.getAllDeviceToken1(); fcmUsers =
	 * this.fcmUserRepository.getAllDeviceToken(); if
	 * (token.getDeviceType().equalsIgnoreCase("android")) {
	 * for(BroadcastMessage broadcastMessage : nearMeFlyrr) {
	 * System.out.println("Id------------>"+broadcastMessage.getId());
	 * this.sendAndroidNotification(notifyUserDto.getTitle(),
	 * notifyUserDto.getMessage(),broadcastMessage.getId()); return true;
	 * 
	 * } } }
	 * 
	 * } catch (Exception e) { System.out.println("error------------>" +
	 * e.getMessage()); return false; } return false; }
	 * 
	 * 
	 * @Override public Boolean SendFlyRRNotification(FlyRRNotificationDto dto)
	 * {
	 * 
	 * return null; }
	 */

	@Override
	public List<FavoriteListDTO> getFevorietFlyRRsByUser(User user) {
		double rangeInMiles = 0.0;
		List<FavoriteListDTO> listFavouriteDTO = new ArrayList<>();
		UserFlyRRResponseDTO userFlyRRResponseDTO = null;
		System.out.println(DateUtil.getCurrentDate());
		List<FavoriteFlyrr> broadcastMessage = this.favoriteFlyrrRepository
				.getFavoriteFlyyRRByUser(DateUtil.getCurrentDate(),
						user.getId());

		for (int i = 0; i < broadcastMessage.size(); i++) {

			System.out.println("Brodcast Id-------->"
					+ broadcastMessage.get(i).getBroadcastMessage().getId());

			FavoriteListDTO favoriteListDTO = new FavoriteListDTO();
			favoriteListDTO.setBroadcastId(broadcastMessage.get(i)
					.getBroadcastMessage().getId());
			favoriteListDTO.setCategories(this.flyrrJobCategoriesRepository
					.getCategoriesByBroadcastMessage(broadcastMessage.get(i)
							.getBroadcastMessage()));
			favoriteListDTO.setActive(broadcastMessage.get(i).getIsActive());
			favoriteListDTO.setDescription(broadcastMessage.get(i)
					.getBroadcastMessage().getDescription());
			favoriteListDTO.setImages(this.flyrrDocumentsRepository
					.getFlyrrDocumentNameByBroadcastMessageId(broadcastMessage
							.get(i).getBroadcastMessage()));
			favoriteListDTO.setExpireIn(broadcastMessage.get(i)
					.getBroadcastMessage().getExpireIn());
			favoriteListDTO.setActive(broadcastMessage.get(i).getIsActive());
			favoriteListDTO.setTitle(broadcastMessage.get(i)
					.getBroadcastMessage().getTitle());
			favoriteListDTO.setPrice(broadcastMessage.get(i)
					.getBroadcastMessage().getPrice());
			favoriteListDTO.setLattitude(broadcastMessage.get(i)
					.getBroadcastMessage().getLattitude());
			favoriteListDTO.setLongitude(broadcastMessage.get(i)
					.getBroadcastMessage().getLongitude());
			favoriteListDTO.setIsFavorite(broadcastMessage.get(i)
					.getIsFavorite());
			/*
			 * favoriteListDTO.setUserId(broadcastMessage.get(i).getBroadcastMessage
			 * ().getUser().getId());
			 * favoriteListDTO.setFullName(broadcastMessage
			 * .get(i).getBroadcastMessage().getUser().getFullName());
			 */
			userFlyRRResponseDTO = new UserFlyRRResponseDTO(broadcastMessage
					.get(i).getBroadcastMessage().getUser().getId(),
					broadcastMessage.get(i).getBroadcastMessage().getUser()
							.getMobileNumber(), broadcastMessage.get(i)
							.getBroadcastMessage().getUser().getFullName(),
					broadcastMessage.get(i).getBroadcastMessage().getUser()
							.getProfilePicture(), broadcastMessage.get(i)
							.getBroadcastMessage().getUser().getRate());
			favoriteListDTO.setUser(userFlyRRResponseDTO);
			
			rangeInMiles=this.getDistanceBetweenTwoPoints(Double.valueOf(user.getLattitude()), Double.valueOf(user.getLongitude()),
					Double.valueOf(broadcastMessage.get(i)
							.getBroadcastMessage().getLattitude()), Double.valueOf(broadcastMessage.get(i)
									.getBroadcastMessage().getLongitude()));
			favoriteListDTO.setRangeInMile(rangeInMiles);
			
			listFavouriteDTO.add(favoriteListDTO);
			System.out.println("Users------>"
					+ broadcastMessage.get(i).getBroadcastMessage().getUser()
							.getId());
			System.out.println("loop-------->" + i);
		}

		return listFavouriteDTO;
	}

	@Override
	public void setUserLatLong(User user, String lattitude, String longitude) {
		User user2 = userRepository.getUserById(user.getId());
		if (user2 != null) {
			user2.setLattitude(lattitude);
			user2.setLongitude(longitude);
			user2.setModifiedDate(new Date());
			System.out.println("------------------------------------->"
					+ userRepository.save(user2));
		}

	}

	@Override
	public List<String> trandingCategories() {

		return flyrrJobCategoriesRepository
				.trandingJobs(new PageRequest(0, 10));
	}

	@Override
	public List<UserDetailsResponseDTO> userDetailsByIdDTO(
			UserDetailsByIdDTO userDetailsByIdDTO) {
		logger.info("UserService_interestedUserDetail");

		List<UserDetailsResponseDTO> userDetailsByIdDTOs = new ArrayList<UserDetailsResponseDTO>();
		try {

			if (userDetailsByIdDTO != null) {
				for (Long id : userDetailsByIdDTO.getUserId()) {
					User objUser = this.userRepository.getUserById(id);

					if (objUser != null) {
						UserDetailsResponseDTO userDetailsResponseDTO = new UserDetailsResponseDTO();
						userDetailsResponseDTO.setUserId(objUser.getId());
						// userDetailsResponseDTO.setCreatedDate(objUser.getCreateDate());
						userDetailsResponseDTO.setFullName(objUser
								.getFullName());
						userDetailsResponseDTO.setRate(objUser.getRate());
						userDetailsResponseDTO.setMobileNumber(objUser
								.getMobileNumber());
						userDetailsResponseDTO.setProfilePicture(objUser
								.getProfilePicture());

						userDetailsByIdDTOs.add(userDetailsResponseDTO);
					} else {
						// throw new NotFoundException(NotFound.UserNotFound,
						// Boolean.TRUE);
					}
				}

			} else {
				throw new NotFoundException(NotFound.UserNotFound, Boolean.TRUE);
			}
		} finally {

		}

		return userDetailsByIdDTOs;
	}
}

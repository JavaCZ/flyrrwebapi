package com.cloudzon.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.tomcat.jni.Multicast;
import org.hibernate.validator.internal.util.privilegedactions.GetAnnotationParameter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.xnio.channels.MulticastMessageChannel;

import com.cloudzon.common.Constants;
import com.cloudzon.config.ApplicationProperties;
import com.cloudzon.dto.FcmUserDTO;
import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.BroadcastMessageInterested;
import com.cloudzon.model.DeviceToken;
import com.cloudzon.model.FCMUser;
import com.cloudzon.model.NotificationTypes;
import com.cloudzon.model.User;
import com.cloudzon.repository.BroadcastMessageIntrestedRepository;
import com.cloudzon.repository.DeviceTokenRepository;
import com.cloudzon.repository.FcmUserRepository;
import com.cloudzon.service.NotificationService;
import com.cloudzon.service.UserService;
import com.notnoop.apns.APNS;
import com.notnoop.apns.ApnsService;
import com.notnoop.apns.ApnsServiceBuilder;

@Service
public class NotificationServiceImpl implements NotificationService {

	private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

	@Autowired
	private BroadcastMessageIntrestedRepository interestedRepo;

	@Autowired
	private DeviceTokenRepository deviceTokenRepository;

	@Autowired
	private FcmUserRepository fcmUserRepository;

	@Autowired
	private BroadcastMessageIntrestedRepository broadcastMessageIntrestedRepository;

	@Value("${apns.type}")
	String apnsType;

	@Value("${apns.password}")
	String apnsPassword;

	@Value("${apns.cert}")
	String apnsCirti;

	@Transactional
	@Override
	public void NotifyUserByFlyRRCreation(BroadcastMessage broadcastMessage, User user) {

		ArrayList<String> registrationTokens = new ArrayList<>();
		ArrayList<String> iosTokens = new ArrayList<>();
		ApplicationProperties appProperties = new ApplicationProperties();
		String server_key = appProperties.apiKey;
		List<DeviceToken> fcmusers = null;
		fcmusers = this.deviceTokenRepository.getAllUserNearMe(broadcastMessage.getLattitude(),
				broadcastMessage.getLongitude(), Constants.RANGE_IN_MILES_FOR_NEARME, user);

		if (fcmusers != null) {

			for (DeviceToken objfcmUser : fcmusers) {

				System.out.println("User Id Comape---->" + objfcmUser.getUser().getId().equals(user.getId()));
				System.out.println("Notify USers--------->" + objfcmUser.getUser().getId());
				BroadcastMessageInterested broadcastMessageInterested = new BroadcastMessageInterested();
				broadcastMessageInterested.setBroadcastMessage(broadcastMessage);
				broadcastMessageInterested.setIsActive(true);
				broadcastMessageInterested.setUser(objfcmUser.getUser());
				broadcastMessageInterested.setIsInterested(false);
				broadcastMessageInterested.setCreatedBy(user.getId());
				broadcastMessageInterested.setCreatedDate(new Date());
				broadcastMessageInterested.setModifiedDate(null);
				this.broadcastMessageIntrestedRepository.save(broadcastMessageInterested);
				if (objfcmUser.getDeviceType().equalsIgnoreCase("Android")) {
					registrationTokens.add(objfcmUser.getDeviceToken());
					System.out.println("Tokens------>" + registrationTokens);
					// this.sendAndroidMessage(broadcastMessage, objfcmUser);
				} else if (objfcmUser.getDeviceType().equalsIgnoreCase("iOS")) {
					iosTokens.add(objfcmUser.getDeviceToken());
				}
			}
			if (registrationTokens.size() > 0 && registrationTokens != null) {
				send_FCM_NotificationMulti(registrationTokens, server_key, broadcastMessage);
				
			}
			else
			{
				logger.info("Android Devices Not Found");
			}
			if (iosTokens.size() > 0 && iosTokens != null) {
				this.sendiOSMessage(broadcastMessage, iosTokens);
			}
			else
			{
				logger.info("Ios Devices Not Found");
			}

		} else {
			logger.info("No Users");
		}
	}

	private Boolean sendiOSMessage(BroadcastMessage broadcastMessage, ArrayList<String> objfcmUser) {
		logger.info("UserService_sendiOSMessage");	
		System.out.println("ios Devivces->" + objfcmUser);
		String type = apnsType;
		try {
			ApnsServiceBuilder apnsServiceBuilder = APNS.newService();

			if (type.equals("prod")) {
				InputStream certPath = NotificationServiceImpl.class.getResourceAsStream(apnsCirti);
				apnsServiceBuilder.withCert(certPath, apnsPassword).withProductionDestination();
			} else if (type.equals("dev")) {
				InputStream certPath = NotificationServiceImpl.class.getResourceAsStream(apnsCirti);
				apnsServiceBuilder.withCert(certPath, apnsPassword).withSandboxDestination();
			} else {
				logger.error("unknown API type " + type);
			}
			ApnsService service = apnsServiceBuilder.build();

			String payload = APNS.newPayload().alertBody(broadcastMessage.getDescription())
					.alertTitle(broadcastMessage.getTitle()).customField("broadCastId", broadcastMessage.getId())
					.customField("priority", "10").badge(1).build();
			logger.info("payload: " + payload);
			service.push(objfcmUser, payload);
		} finally {
			logger.info("sendiOSMessage end");
		}
		return true;

	}

	private boolean sendAndroidMessageNotification(User objUser, DeviceToken objfcmUser, String sentMessage) {
		logger.info("UserService_createBroadcast");
		ApplicationProperties appProperties = new ApplicationProperties();
		String FCMUrl = appProperties.apiUrl;
		System.out.println("Fcm Url***************>" + FCMUrl);
		String server_key = appProperties.apiKey;
		try {
			URL url = new URL(FCMUrl);
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);

			conn.setRequestMethod("POST");
			conn.setRequestProperty("Authorization", "key=" + server_key);
			conn.setRequestProperty("Content-Type", "application/json");

			JSONObject infoJson = new JSONObject();
			infoJson.put("sentMessage", sentMessage);
			infoJson.put("byWhom", objUser.getId());

			JSONObject json = new JSONObject();
			json.put("to", objfcmUser.getDeviceToken());
			json.put("data", infoJson);

			System.out.println("Token------------->" + objfcmUser.getDeviceToken());
			System.out.println("Data------------->" + infoJson);
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			System.out.println(json);
			wr.write(json.toString());
			wr.flush();
			conn.getInputStream();
			int status = 0;
			if (null != conn) {
				status = conn.getResponseCode();
			}

			if (status != 0) {
				if (status == 200) {
					// SUCCESS message
					logger.info("success Notification");
				} else if (status == 401) {
					// client side error
					logger.info(
							"Notification Response : TokenId : " + objfcmUser.getDeviceToken() + " Error occurred :");

				} else if (status == 501) {
					// server side error
					logger.info("Notification Response : [ errorCode=ServerError ] TokenId : "
							+ objfcmUser.getDeviceToken());
				} else if (status == 503) {
					// server side error
					logger.info("Notification Response : FCM Service is Unavailable  TokenId : "
							+ objfcmUser.getDeviceToken());
				}
			}
		} catch (MalformedURLException mlfexception) {
			// Prototcal Error
			logger.info("Error occurred while sending push Notification!.." + mlfexception.getMessage());
		} catch (IOException mlfexception) {
			// URL problem
			logger.info("Reading URL, Error occurred while sending push Notification!.." + mlfexception.getMessage());
		} catch (JSONException jsonexception) {
			// Message format error
			logger.info(
					"Message Format, Error occurred while sending push Notification!.." + jsonexception.getMessage());
		} catch (Exception exception) {
			// General Error or exception.
			logger.info("Error occurred while sending push Notification!.." + exception.getMessage());
		}
		return true;

	}

	/*
	 * public static void sendPushNotification(List<String> deviceTokenList) {
	 * ApplicationProperties appProperties = new String server_key =
	 * appProperties.apiKey; Sender sender = new Sender(AUTH_KEY_FCM); Message msg =
	 * new Message.Builder().addData("message", "Message body").build(); try {
	 * MulticastResult result = sender.send(msg, deviceTokenList, 5); for (Result r
	 * : result.getResults()) { if (r.getMessageId() != null)
	 * System.out.println("Push Notification Sent Successfully"); else
	 * System.out.println("ErrorCode " + r.getErrorCodeName()); } } catch
	 * (IOException e) { System.out.println("Error " + e.getLocalizedMessage()); } }
	 */

	static void send_FCM_NotificationMulti(List<String> putIds2, String server_key, BroadcastMessage message) {
		try {
			// Create URL instance.
			ApplicationProperties appProperties = new ApplicationProperties();
			String FCMUrl = appProperties.apiUrl;
			URL url = new URL(FCMUrl);
			// create connection.
			HttpURLConnection conn;
			conn = (HttpURLConnection) url.openConnection();
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			// set method as POST or GET
			conn.setRequestMethod("POST");
			// pass FCM server key
			conn.setRequestProperty("Authorization", "key=" + server_key);
			// Specify Message Format
			conn.setRequestProperty("Content-Type", "application/json");
			// Create JSON Object & pass value

			JSONArray regId = null;
			JSONObject objData = null;
			JSONObject data = null;
			JSONObject notif = null;

			regId = new JSONArray(putIds2);
			System.out.println("Tokens----->" + regId);
			/*
			 * for (int i = 0; i < putIds2.size(); i++) { regId.put(putIds2.get(i)); }
			 */
			JSONObject infoJson = new JSONObject();
			infoJson.put("title", message.getTitle());
			infoJson.put("message", message.getDescription());
			infoJson.put("broadcast_message_id", message.getId());
			objData = new JSONObject();
			objData.put("registration_ids", regId);
			objData.put("data", infoJson);
			objData.put("notification", notif);
			System.out.println("Data Pass" + objData.toString());

			/*
			 * JSONObject infoJson = new JSONObject(); infoJson.put("title",
			 * broadcastMessage.getTitle()); infoJson.put("message",
			 * broadcastMessage.getDescription()); infoJson.put("broadcast_message_id",
			 * broadcastMessage.getId());
			 * 
			 * JSONObject json = new JSONObject();
			 * System.out.println(objfcmUser.getDeviceToken()); json.put("to",
			 * objfcmUser.getDeviceToken().toString()); json.put("data", infoJson);
			 * System.out.println("Json---------->" + json);
			 */
			System.out.println("json :" + objData.toString());
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

			wr.write(objData.toString());
			wr.flush();
			int status = 0;
			if (null != conn) {
				status = conn.getResponseCode();
			}
			if (status != 0) {

				if (status == 200) {
					// SUCCESS message
					BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
					System.out.println("Android Notification Response : " + reader.readLine());
				} else if (status == 401) {
					// client side error
					System.out.println("Notification Response : TokenId :  Error occurred :");
				} else if (status == 501) {
					// server side error
					System.out.println("Notification Response : [ errorCode=ServerError ] TokenId : ");
				} else if (status == 503) {
					// server side error
					System.out.println("Notification Response : FCM Service is Unavailable TokenId ");
				}
			}
		} catch (MalformedURLException mlfexception) {
			// Prototcal Error
			System.out.println("Error occurred while sending push Notification!.." + mlfexception.getMessage());
		} catch (IOException mlfexception) {
			// URL problem
			System.out.println(
					"Reading URL, Error occurred while sending push  Notification!.." + mlfexception.getMessage());
		} catch (Exception exception) {
			// General Error or exception.
			System.out.println("Error occurred while sending push Notification!.." + exception.getMessage());
		}
	}

	@Override
	public Boolean chatnotification(User objUser, FcmUserDTO fcmUserDTO) throws Exception {
		logger.info("UserService_sendNotificationstart");
		DeviceToken objfcmUser = null;
		objfcmUser = this.fcmUserRepository.getReceverUser(fcmUserDTO.getReciverId());

		if (objfcmUser != null) {
			if (objfcmUser.getDeviceType().equalsIgnoreCase("Android")) {
				this.sendAndroidMessageNotification(objUser, objfcmUser, fcmUserDTO.getSentMessage());
				return true;
			} else if (objfcmUser.getDeviceType().equalsIgnoreCase("iOS")) {
				this.sendiOSMessage(objUser, objfcmUser.getDeviceToken(), fcmUserDTO.getSentMessage(),NotificationTypes.ChatNotification,0L,Boolean.FALSE);
				System.out.println("IOS");
				return true;
			}
		} else {
			logger.info("User Not Found");
			return false;
		}
		return false;

	}

	private void sendiOSMessage(User objUser, String deviceToken, String sentMessage,NotificationTypes notificationType,Long flyrrId,Boolean isSilent) {

		logger.info("sendiOSMessage start");
		String type = apnsType;
		try {

			ApnsServiceBuilder serviceBuilder = APNS.newService();

			if (type.equals("prod")) {
				InputStream certPath = NotificationServiceImpl.class.getResourceAsStream(apnsCirti);
				serviceBuilder.withCert(certPath, apnsPassword).withProductionDestination();
			} else if (type.equals("dev")) {
				InputStream certPath = NotificationServiceImpl.class.getResourceAsStream(apnsCirti);
				serviceBuilder.withCert(certPath, apnsPassword).withSandboxDestination();
			} else {
				logger.error("unknown API type " + type);
			}
			ApnsService service = serviceBuilder.build();
			String payload ;
			if(isSilent)
			{
				payload = APNS.newPayload().instantDeliveryOrSilentNotification().alertBody(sentMessage).alertTitle(objUser.getFullName())
					.customField("notificationType", notificationType.ordinal())
					.customField("flyerId", flyrrId).customField("senderId", objUser.getId()).customField("priority", "0")
					.badge(1).build();
			}else
			{
				payload = APNS.newPayload().alertBody(sentMessage).alertTitle(objUser.getFullName())
						.customField("notificationType", notificationType.ordinal())
						.customField("flyerId", flyrrId).customField("senderId", objUser.getId()).customField("priority", "0")
						.badge(1).build();
			}
			System.out.println(payload);
			logger.info("payload: " + payload);
			service.push(deviceToken, payload);
		} finally {
			logger.info("sendiOSMessage end");
		}
	}
}

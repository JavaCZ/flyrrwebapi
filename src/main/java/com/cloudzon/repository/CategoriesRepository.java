package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.Categories;
import com.cloudzon.repository.BaseRepository;

public interface CategoriesRepository extends BaseRepository<Categories> {

	@Query("SELECT categories.categories FROM Categories categories where categories.active = TRUE "
			+ "AND categories.categories like %:searchKeyword%")
	public List<String> findCategoriesBySearch(@Param("searchKeyword") String searchKeyword);

	@Query(nativeQuery = true, value = "SELECT categories.categories AS col_0_0_ FROM flyrr_categories categories WHERE categories.active=1 LIMIT 10")
	public List<String> findCategories();

}

package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.DeviceToken;
import com.cloudzon.model.FCMUser;
import com.cloudzon.model.User;
import com.cloudzon.repository.BaseRepository;

public interface DeviceTokenRepository extends BaseRepository<DeviceToken> {

	@Query("SELECT deviceToken FROM DeviceToken AS deviceToken")
	public List<DeviceToken> getAllDeviceToken();

	@Query("SELECT deviceToken FROM DeviceToken AS deviceToken")
	public DeviceToken getAllDeviceToken1();

	@Query("SELECT deviceToken FROM DeviceToken AS deviceToken JOIN deviceToken.user AS user WHERE user =:user")
	public DeviceToken getAllDeviceTokenByUser(@Param("user") User user);

	@Query("SELECT deviceToken FROM DeviceToken AS deviceToken JOIN deviceToken.user AS user WHERE user =:userid")
	public DeviceToken getAllDeviceTokenByUserId(@Param("userid") Long userId);

	@Query("SELECT DISTINCT fcmuser FROM User AS userLatLon,DeviceToken AS fcmuser "
			+ "WHERE 3956*2*ASIN(SQRT(POWER(SIN((:latitude-ABS(userLatLon.lattitude))*PI()/180/2), 2)+COS(:latitude*PI()/180)*COS(ABS(userLatLon.lattitude)*PI()/180)*POWER(SIN((:longitude-userLatLon.longitude)*PI()/180/2), 2))) < "
			+ ":rangeInMile AND userLatLon = fcmuser.user AND fcmuser.user.isEnableNotification=TRUE AND fcmuser.active = TRUE AND fcmuser.user.isActive = TRUE AND fcmuser.user <> :user")
	List<DeviceToken> getAllUserNearMe(@Param("latitude") String latitude, @Param("longitude") String longitude,
			@Param("rangeInMile") Double rangeInMile, @Param("user") User user);

	/*
	 * @Query("SELECT DISTINCT fcmuser FROM DeviceToken AS fcmuser, User AS userLatLon WHERE 6371 * ACOS(COS(RADIANS(:latitude)) * COS(RADIANS(userLatLon.lattitude)) * COS(RADIANS(userLatLon.longitude) - RADIANS(:longitude)) + SIN(RADIANS(:latitude)) * SIN(RADIANS(userLatLon.lattitude))) <  :rangeInMile  AND fcmuser.user.isEnableNotification = TRUE AND fcmuser.user.isActive = TRUE AND fcmuser.user !=:user GROUP BY fcmuser.deviceToken ORDER BY fcmuser.deviceToken DESC"
	 * ) List<DeviceToken> getAllUserNearMe(@Param("latitude") String
	 * latitude, @Param("longitude") String longitude,
	 * 
	 * @Param("rangeInMile") Double rangeInMile, @Param("user") User users); //AND
	 * fcmuser.user != :user ,, @Param("user") User users
	 */}

package com.cloudzon.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.dto.BroadcastInterestedResponseDTO;
import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.FavoriteFlyrr;
import com.cloudzon.model.User;

public interface FavoriteFlyrrRepository extends BaseRepository<FavoriteFlyrr> {

	public FavoriteFlyrr findOneByBroadcastMessageAndUserAndIsFavoriteAndIsActive(BroadcastMessage broadcastMessage,
			User user, Boolean isFavorite, Boolean isActive);

	public FavoriteFlyrr findOneByBroadcastMessageAndUser(BroadcastMessage broadcastMessage, User user);

	@Query("SELECT favoriteFlyrr.broadcastMessage FROM FavoriteFlyrr favoriteFlyrr JOIN favoriteFlyrr.broadcastMessage AS broadcastMessage JOIN favoriteFlyrr.user AS user WHERE favoriteFlyrr.isActive = TRUE AND favoriteFlyrr.isFavorite = TRUE AND user =:user ORDER BY favoriteFlyrr.id DESC")
	public List<BroadcastMessage> getFavoriteBroadcastMessageByUser(@Param("user") User user);

	@Query("SELECT favoriteFlyrr FROM FavoriteFlyrr AS favoriteFlyrr WHERE favoriteFlyrr.isActive = TRUE  AND favoriteFlyrr.isFavorite = TRUE AND favoriteFlyrr.user =:user")
	public List<FavoriteFlyrr> getFavoriteFlyRRByUser(@Param("user") User objUser);

	@Query("SELECT favoriteFlyrr FROM FavoriteFlyrr AS favoriteFlyrr JOIN favoriteFlyrr.broadcastMessage AS broadcastMessage WHERE favoriteFlyrr.isActive = TRUE AND favoriteFlyrr.isFavorite = TRUE AND broadcastMessage.id =:broadcast")
	public List<FavoriteFlyrr> getFavoriteFlyrrByBroadcastMessage(@Param("broadcast") Long broadcast);

	public FavoriteFlyrr findOneByBroadcastMessage(Long broadcastId);

	@Query("SELECT favoriteFlyrr FROM FavoriteFlyrr AS favoriteFlyrr JOIN favoriteFlyrr.broadcastMessage AS broadcastMessage WHERE favoriteFlyrr.isActive = TRUE AND favoriteFlyrr.isFavorite = TRUE AND broadcastMessage.id =:broadcastId")
	public FavoriteFlyrr getFavoriteFlyrrByBroadcastMessageId(@Param("broadcastId") Long broadcastId);

	// @Query("SELECT favoriteFlyrr FROM FavoriteFlyrr AS favoriteFlyrr JOIN
	// favoriteFlyrr.broadcastMessage AS bm WHERE bm.isActive = TRUE AND bm.isDelete
	// = FALSE AND bm.isBuying = FALSE AND bm.expireIn > :curdate AND
	// favoriteFlyrr.user =:user")
	// @Query("SELECT favoriteFlyrr FROM FavoriteFlyrr AS favoriteFlyrr JOIN
	// favoriteFlyrr.broadcastMessage AS bm WHERE bm. isActive = TRUE AND
	// bm.isDelete = FALSE AND bm.isBuying = FALSE AND favoriteFlyrr.user = :user
	// AND favoriteFlyrr.isFavorite = TRUE AND bm.expireIn > :curdate AND
	// favoriteFlyrr.isActive = TRUE")
	@Query("SELECT ff FROM FavoriteFlyrr AS ff JOIN ff.broadcastMessage AS bm WHERE ff.user.id=:user AND ff.isFavorite = TRUE AND bm.isBuying = FALSE AND bm.isDelete = FALSE AND bm.expireIn > :curdate")
	public List<FavoriteFlyrr> getFavoriteFlyyRRByUser(@Param("curdate") Date curdate, @Param("user") Long objUser);

	// FavoriteFlyrr AS favoriteFlyrr favoriteFlyrr.broadcastMessage
}

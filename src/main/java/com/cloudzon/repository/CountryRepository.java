package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.dto.CountryDTO;
import com.cloudzon.dto.RegionsDTO;
import com.cloudzon.model.Country;

public interface CountryRepository extends BaseRepository<Country> {

	// 26-11-2018
	@Query("SELECT NEW com.cloudzon.dto.CountryDTO(country.id,country.name) FROM Country AS country WHERE country.active=TRUE")
	public List<CountryDTO> getcountry();

	// getCountryByCountryCode
	@Query("SELECT country FROM Country AS country WHERE country.countryCode=:countryCode")
	public Country getCountryByCountryCode(@Param("countryCode") String countryCode);

	// getCountries
	/*
	 * @Query("SELECT NEW com.gratzeez.dto.RegionsDTO(country.name,country.countryCode,country.currencySymbol,country.currencyCode)  FROM Country AS country"
	 * ) public List<RegionsDTO> getCountries();
	 */

	// getCountryByObject
	/*
	 * @Query("SELECT NEW com.gratzeez.dto.RegionsDTO(country.name,country.countryCode,country.currencySymbol,country.currencyCode)  FROM Country AS country WHERE country =:country"
	 * ) public RegionsDTO getCountryByObject(@Param("country") Country country);
	 */

}

package com.cloudzon.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.SMSVerificationToken;
import com.cloudzon.model.SMSVerificationToken.VerificationTokenType;
import com.cloudzon.model.User;

public interface SMSVerificationTokenRepository extends BaseRepository<SMSVerificationToken> {

	// getVerificationTokenByToken
	@Query(value = "SELECT smsVerificationToken FROM SMSVerificationToken AS smsVerificationToken WHERE smsVerificationToken.tokenType=:tokenType AND smsVerificationToken.verification_token=:verification_token")
	public SMSVerificationToken getVerifactionTokenByToken(@Param("verification_token") String verification_token,
			@Param("tokenType") VerificationTokenType tokenType);

	@Query(value = "SELECT smsVerificationToken FROM SMSVerificationToken AS smsVerificationToken WHERE smsVerificationToken.tokenType=:tokenType AND smsVerificationToken.verification_token=:verification_token AND smsVerificationToken.user =:user")
	public SMSVerificationToken getVerifactionTokenByToken(@Param("verification_token") String verification_token,
			@Param("tokenType") VerificationTokenType tokenType, @Param("user") User user);

}

package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.BroadcastMessageInterested;
import com.cloudzon.model.Categories;
import com.cloudzon.model.FlyrrJobCategories;
import com.cloudzon.model.User;

public interface FlyrrJobCategoriesRepository extends BaseRepository<FlyrrJobCategories> {

	public List<FlyrrJobCategories> findFlyrrJobCategoriesByBroadcastMessage(BroadcastMessage broadcastMessage);

	@Query("SELECT jobCategory.categories FROM FlyrrJobCategories AS flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage AS broadcastMessage JOIN flyrrJobCategories.jobCategory AS jobCategory WHERE broadcastMessage=:broadcastMessage AND flyrrJobCategories.isActive = TRUE")
	public List<String> getCategoriesByBroadcastMessage(@Param("broadcastMessage") BroadcastMessage broadcastMessage);

	@Query("SELECT DISTINCT broadcastMessage FROM FlyrrJobCategories AS flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage AS broadcastMessage JOIN flyrrJobCategories.jobCategory AS jobCategory WHERE broadcastMessage =:broadcastMessage AND jobCategory =:jobCategory")
	public BroadcastMessage getBroadcastMessageByCategories(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage, @Param("jobCategory") Categories jobCategory);

	@Query("SELECT broadcastMessage FROM FlyrrJobCategories AS flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage AS broadcastMessage JOIN flyrrJobCategories.jobCategory AS jobCategory WHERE broadcastMessage =:broadcastMessage AND (jobCategory.categories like %:searchWord% OR broadcastMessage.title like %:searchWord%) ")
	public BroadcastMessage getBroadcastMessageBySearch(@Param("broadcastMessage") BroadcastMessage broadcastMessage,
			@Param("searchWord") String searchWord);

	// (jobCategory.categories like %:searchWord% OR like %:searchWord% OR like
	// %:searchWord%)
	@Query("SELECT DISTINCT broadcastMessage FROM FlyrrJobCategories AS flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage AS broadcastMessage JOIN flyrrJobCategories.jobCategory AS jobCategory WHERE broadcastMessage =:broadcastMessage AND jobCategory =:jobCategory AND (jobCategory.categories like %:searchWord% OR broadcastMessage.title like %:searchWord%)")
	public BroadcastMessage getBroadcastMessageByCategoriesAndSearch(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage, @Param("jobCategory") Categories categories,
			@Param("searchWord") String searchWord);

	@Query("SELECT broadcastMessage FROM FlyrrJobCategories as flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage as broadcastMessage WHERE broadcastMessage =:broadcastMessage")
	public BroadcastMessage getFlyrrJobCategoriesByBroadcastMessage(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage);

	@Query("SELECT DISTINCT broadcastMessage FROM FlyrrJobCategories AS flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage AS broadcastMessage JOIN flyrrJobCategories.jobCategory AS jobCategory WHERE broadcastMessage =:broadcastMessage AND jobCategory IN (:jobCategory)")
	public BroadcastMessage getBroadcastMessageByCategories(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage,
			@Param("jobCategory") List<Categories> jobCategory);

	@Query("SELECT DISTINCT broadcastMessage FROM FlyrrJobCategories AS flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage AS broadcastMessage JOIN flyrrJobCategories.jobCategory AS jobCategory WHERE broadcastMessage =:broadcastMessage AND jobCategory IN (:jobCategory) AND (jobCategory.categories like %:searchWord% OR broadcastMessage.title like %:searchWord%)")
	public List<BroadcastMessage> getBroadcastMessageByCategoriesAndSearch(BroadcastMessage broadcastMessage,
			List<Categories> categories, String searchWord);

	@Query("SELECT jobcats.jobCategory.categories FROM FlyrrJobCategories AS jobcats GROUP BY jobcats.jobCategory.id ORDER BY COUNT(jobcats.jobCategory.id) DESC")
	public List<String> trandingJobs(Pageable pageable);

	@Query("SELECT DISTINCT broadcastMessage FROM FlyrrJobCategories AS flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage AS broadcastMessage JOIN flyrrJobCategories.jobCategory AS jobCategory WHERE broadcastMessage.isBuying=FALSE AND broadcastMessage.user !=:user AND jobCategory.categories IN (:jobCategory) AND (jobCategory.categories like %:searchWord% OR broadcastMessage.title like %:searchWord%)")
	public List<BroadcastMessage> getBroadcastMessageByCategoriesListAndSearchWords(
			@Param("jobCategory") List<String> jobCategory,@Param("searchWord") String searchWord ,@Param("user") User user);
	
	
	@Query("SELECT DISTINCT broadcastMessage FROM FlyrrJobCategories AS flyrrJobCategories JOIN flyrrJobCategories.broadcastMessage AS broadcastMessage JOIN flyrrJobCategories.jobCategory AS jobCategory WHERE broadcastMessage.isBuying=FALSE AND broadcastMessage.user !=:user AND (jobCategory.categories like %:searchWord% OR broadcastMessage.title like %:searchWord%)")
	public List<BroadcastMessage> getBroadcastMessageBySearchWords(
			@Param("searchWord") String searchWord ,@Param("user") User user);
}

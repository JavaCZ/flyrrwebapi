package com.cloudzon.repository;

import java.util.List;

import com.cloudzon.model.Categories;

public interface FlyrrCategoriesRepository extends BaseRepository<Categories> {

	// @Query(nativeQuery = true, value= "SELECT categories FROM Categories
	// categories WHERE categories.categories like %:queryString%")
	// public Categories findByCatagoryTags(@Param("queryString")String categories);
	// @Query(nativeQuery = true, value= "SELECT categories FROM Categories
	// categories WHERE categories.categories like %:queryString%")

	public Categories findByCategories(String categories);

	public List<Categories> findByCategories(List<String> categories);

}

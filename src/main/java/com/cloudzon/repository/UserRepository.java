package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.dto.InterestedUserDto;
import com.cloudzon.dto.ProfileDTO;
import com.cloudzon.model.User;

public interface UserRepository extends BaseRepository<User> {

	/*
	 * @Query("SELECT user FROM User AS user WHERE user=:user") public User
	 * getUser(@Param("user") User user);
	 */

	// 22-11-2018
	// All table data access ( getUSer)
	@Query("SELECT user FROM User AS user WHERE user=:user")
	public User getUser(@Param("user") User user);

	@Query("SELECT new com.cloudzon.dto.ProfileDTO(user.id,user.fullName,user.profilePicture,user.dialCode,user.mobileNumber) FROM User as user where user.id=:id")
	public ProfileDTO getUserProfile(@Param("id") Long id);

	/*
	 * @Query("SELECT new com.cloudzon.dto.ProfileDTO(user.id, user.fullName, user.profilePicture, user.mobileNumber, user.bankName, user.bankAccountNumber,user.cardType ,user.rate ,user.dialCode) from User as user where user.id=id"
	 * ) public ProfileDTO getUserProfile(@Param("id") Long id);
	 */

	// getUserId
	@Query("SELECT user FROM User AS user WHERE user.id=:userId")
	public User getUserId(@Param("userId") Long userId);

	// getUserById
	@Query("SELECT user FROM User AS user WHERE user.id=:userId  AND user.isActive=true")
	public User getUserById(@Param("userId") Long userId);

	// getUserByMobile
	@Query("SELECT user FROM User AS user WHERE user.mobileNumber=:mobileNumber")
	public User getUserByMobile(@Param("mobileNumber") String mobileNumber);

	// getUserByMobileNumber
	@Query(value = "SELECT user FROM User AS user WHERE user.mobileNumber=:mobileNumber AND user.isActive=true")
	public User getUserByMobileNumber(@Param("mobileNumber") String mobileNumber);

	// getAllUserByMobileNumber
	@Query(value = "SELECT user FROM User AS user WHERE user.mobileNumber=:mobileNumber")
	public User getAllUserByMobileNumber(@Param("mobileNumber") String mobileNumber);

	// getLoginUserByMobileNumber
	@Query(value = "SELECT user FROM User AS user WHERE user.mobileNumber=:mobileNumber")
	public User getLoginUserByMobileNumber(@Param("mobileNumber") String mobileNumber);

	// getUserByUserNameOrMobileNumber
	@Query("SELECT user FROM User AS user WHERE (user.mobileNumber=:userNameOrMobileNumber OR user.userName=:userNameOrMobileNumber) AND user.isActive=true")
	public User getUserByUserNameOrMobileNumber(@Param("userNameOrMobileNumber") String userNameOrMobileNumber);

	// getActiveUserByUserNameOrMobileNumber
	@Query("SELECT user FROM User AS user WHERE (user.mobileNumber=:userNameOrMobileNumber OR user.userName=:mobileNumberOrUserName) AND user.isActive=true")
	public User getActiveUserByUserNameOrMobileNumber(@Param("userNameOrMobileNumber") String userNameOrMobileNumber);

	// getUserByUserNameOrMobileNumber
	@Query("SELECT user From User AS user WHERE (user.mobileNumber=:mobileNumber OR user.userName=:userName) AND  user.isActive=true")
	public User getUserByUserNameOrMobileNumber(@Param("userName") String userName,
			@Param("mobileNumber") String mobileNumber);

	// getUserByUserName
	@Query("SELECT user FROM User AS user WHERE user.userName=:userName AND user.isActive=true")
	public User getUserByUsrName(@Param("userName") String user_Name);

	// getUserNameByMobileNumber
	@Query("SELECT user.userName FROM User AS user WHERE user.mobileNumber=mobileNumber AND user.isActive=true")
	public String getUserNameByMobileNumber(@Param("mobileNumber") String mobileNumber);

	// modify update Rate

	// updateUserRate
	/*
	 * @Query("UPDATE User AS user SET rate =: averageRate WHERE id=:userId AND user.isActive=true"
	 * ) public void updateUserRate(@Param("userId")Long userId
	 * , @Param("averageRate") double averageRate);
	 */

	// isActive
	@Query("SELECT user FROM User as user where user.isActive = TRUE AND user = :user")
	public User getActiveUser(@Param("user") User user);

	// profileDTO
	@Query("SELECT NEW com.cloudzon.dto.ProfileDTO(user.id,user.userName,user.profilePicture,user.mobileNumber,user.bankName,user.bankAccountNumber)FROM User AS user WHERE user.mobileNumber=:mobileNumber AND user.isVerified=TRUE AND user.isActive=true")
	public ProfileDTO getPublicProfileByMobileNumber(@Param("mobileNumber") String mobileNumber);

	// near be flyrr (NearByUserDTO)

	@Query("SELECT user FROM User AS user WHERE user.id=:userId  AND user.isActive=true")
	public User getUserByIdAndActiveNotifiaction(@Param("userId") Long userId);

	@Modifying
	@Query("UPDATE User AS user SET user.rate =:averageRate WHERE user.id =:userId")
	public void updateUserByRate(@Param("userId") Long userId, @Param("averageRate") Double averageRate);
	
	@Query("SELECT new com.cloudzon.dto.InterestedUserDto(user.id,user.fullName,user.profilePicture,user.dialCode,user.mobileNumber) FROM User as user where user.id=:id")
	public InterestedUserDto getInterestedUserProfile(@Param("id")Long id);
}

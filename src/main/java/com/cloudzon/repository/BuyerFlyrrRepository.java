package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.BaseEntity;
import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.BuyerFlyrr;
import com.cloudzon.model.FavoriteFlyrr;
import com.cloudzon.model.User;

public interface BuyerFlyrrRepository extends BaseRepository<BuyerFlyrr> {

	public BuyerFlyrr findOneByBroadcastMessageAndUser(BroadcastMessage broadcastMessage, User user);

	@Query("SELECT buyerFlyrr.broadcastMessage FROM BuyerFlyrr buyerFlyrr JOIN buyerFlyrr.broadcastMessage AS broadcastMessage JOIN buyerFlyrr.user AS user WHERE buyerFlyrr.isActive = TRUE  AND user =:user ORDER BY buyerFlyrr.id DESC")
	public List<BroadcastMessage> getBuyerByBroadcastMessageByUser(@Param("user") User objUser);

	@Query("SELECT buyerFlyrr FROM BuyerFlyrr AS buyerFlyrr JOIN buyerFlyrr.broadcastMessage AS broadcastMessage JOIN buyerFlyrr.user AS user WHERE buyerFlyrr.isActive = TRUE AND broadcastMessage =:broadcastMessage AND user =:user")
	public BuyerFlyrr getBuyerFlyrrForBroadcastMessageAndUser(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage, @Param("user") User user);

	@Query("SELECT buyerFlyrr FROM BuyerFlyrr AS buyerFlyrr JOIN buyerFlyrr.broadcastMessage AS broadcastMessage JOIN buyerFlyrr.user AS user WHERE buyerFlyrr.isActive = TRUE AND broadcastMessage =:broadcastMessage AND broadcastMessage.isNegotiable = TRUE AND user !=:user")
	public BuyerFlyrr getBuyerFlyrrForNearbyAndBroadcastMessage(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage, @Param("user") User user);

	@Query("SELECT buyerFlyrr FROM BuyerFlyrr AS buyerFlyrr JOIN buyerFlyrr.broadcastMessage AS broadcastMessage JOIN buyerFlyrr.user AS user WHERE buyerFlyrr.isActive = TRUE AND broadcastMessage =:broadcastMessage AND broadcastMessage.isNegotiable = FALSE AND user !=:user")
	public BuyerFlyrr getBuyerFlyrrForNearbyAndBroadcastMessageAndIsNegotiable(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage, @Param("user") User user);

	@Query("SELECT buyerFlyrr FROM BuyerFlyrr AS buyerFlyrr JOIN buyerFlyrr.broadcastMessage AS broadcastMessage  WHERE  broadcastMessage =:broadcastMessage AND  broadcastMessage.isBuying = FALSE")
	public BuyerFlyrr getBuyerFlyrrForBroadcastMessage(@Param("broadcastMessage") BroadcastMessage broadcastMessage);

	@Query("SELECT buyerFlyrr FROM BuyerFlyrr AS buyerFlyrr JOIN buyerFlyrr.broadcastMessage AS broadcastMessage  WHERE buyerFlyrr.isActive = TRUE AND broadcastMessage =:broadcastMessage AND  broadcastMessage.isDelete = TRUE ")
	public BuyerFlyrr getBroadcastMessageAndIsDelete(@Param("broadcastMessage") BroadcastMessage broadcastMessage);

	@Query("SELECT buyerFlyrr FROM BuyerFlyrr AS buyerFlyrr WHERE buyerFlyrr.isActive = TRUE AND buyerFlyrr.broadcastMessage =:broadcastMessage")
	public BuyerFlyrr getSellerBuyerFlyrrForBroadcastMessage(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage);
}
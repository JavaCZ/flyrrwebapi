package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.RatingFeedback;
import com.cloudzon.model.User;

public interface RatingFeedbackRepository extends BaseRepository<RatingFeedback> {

	public RatingFeedback getRatingFeedbackByBroadcastMessageAndRatingByUser(BroadcastMessage BroadcastMessage,
			User objUser);

	@Query("SELECT AVG(ratingFeedback.rate) FROM RatingFeedback AS ratingFeedback JOIN ratingFeedback.ratingToUser AS user WHERE user =:user AND ratingFeedback.rate!= -1 AND ratingFeedback.rate!= 0")
	public Double getAverageRatingByUser(@Param("user") User user);

	@Query("SELECT ratingFeedback FROM RatingFeedback ratingFeedback  WHERE  ratingFeedback.ratingToUser =:user ORDER BY ratingFeedback.id DESC")
	public List<RatingFeedback> getRatingByUser(@Param("user") User objUser);
}

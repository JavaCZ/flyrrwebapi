package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.FlyrrDocument;

public interface FlyrrDocumentsRepository extends BaseRepository<FlyrrDocument> {

	public List<FlyrrDocument> findFlyrrDocumentByBroadcastMessageAndIsActive(BroadcastMessage broadcastMessage,
			boolean b);

	@Query("SELECT flyrrDocument.imagesPath FROM FlyrrDocument AS flyrrDocument JOIN flyrrDocument.broadcastMessage AS broadcastMessage WHERE broadcastMessage =:broadcastMessage AND flyrrDocument.isActive = TRUE")
	public List<String> getFlyrrDocumentNameByBroadcastMessageId(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage);

}

package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.BroadcastMessageInterested;
import com.cloudzon.model.User;

public interface BroadcastMessageIntrestedRepository extends BaseRepository<BroadcastMessageInterested> {

	@Query("SELECT broadcastMessageIntrested FROM BroadcastMessageInterested as broadcastMessageIntrested JOIN broadcastMessageIntrested.broadcastMessage as broadcastMessage JOIN broadcastMessageIntrested.user as user WHERE broadcastMessage=:broadcastMessage AND user.id=:user_id")
	public BroadcastMessageInterested getBroadcastMessageInterestedByBroadcastIdUserId(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage, @Param("user_id") Long id);

	public BroadcastMessageInterested findOneByBroadcastMessageAndUser(BroadcastMessage broadcastMessage, User objUser);
	/*
	 * @Query("select broadcastMessageIntrested from BroadcastMessageInterested as broadcastMessageIntrested JOIN broadcastMessageIntrested.broadcastMessage as broadcastMessage JOIN broadcastMessage.user as user WHERE broadcastMessage=:broadcastMessage AND user.id =:user_id"
	 * ) public BroadcastMessageInterested getBroadcastMessageIntrestedById(
	 * 
	 * @Param("broadcastMessage") BroadcastMessage broadcastMessage,
	 * 
	 * @Param("user_id") Long id);
	 * 
	 * @Query("Select COUNT(broadcastMessage) From BroadcastMessageInterested as broadcastMessageInterested where broadcastMessageInterested.broadcastMessage.id = :getIds"
	 * ) public Long getReceversIds(@Param("getIds") Long getIds);
	 * 
	 * @Query("Select broadcastMessageInterested.receiverUser.id From BroadcastMessageInterested as broadcastMessageInterested where broadcastMessageInterested.isInterested =TRUE AND broadcastMessageInterested.broadcastMessage.id = :getIds"
	 * ) public List<Long> getIntredtedIds(@Param("getIds") Long getIds);
	 * 
	 * @Query("SELECT broadcastMessageIntrested FROM BroadcastMessageInterested as broadcastMessageIntrested JOIN broadcastMessageIntrested.broadcastMessage as broadcastMessage JOIN broadcastMessageIntrested.receiverUser as user WHERE broadcastMessage=:broadcastMessage AND user.id=:user_id"
	 * ) public BroadcastMessageInterested
	 * getBroadcastMessageInterestedByBroadcastIdUserId(
	 * 
	 * @Param("broadcastMessage") BroadcastMessage broadcastMessage,
	 * 
	 * @Param("user_id") Long id);
	 * 
	 * @Query("Select broadcastMessageInterested.receiverUser.id From BroadcastMessageInterested as broadcastMessageInterested where broadcastMessageInterested.broadcastMessage.id=:broadcast_message_id"
	 * ) public List<Long> getSimilarData(@Param("broadcast_message_id") Long
	 * broadcast_message_id);
	 * 
	 * @Query("Select broadcastMessageInterested.isInterested From BroadcastMessageInterested as broadcastMessageInterested where broadcastMessageInterested.broadcastMessage.id=:broadcastId AND broadcastMessageInterested.receiverUser.id=:id"
	 * ) public Boolean getIsIntrestedByMe(@Param("broadcastId")Long
	 * broadcastId,@Param("id") Long id);
	 * 
	 * @Query("SELECT broadcastMessageInterested FROM BroadcastMessageInterested as broadcastMessageInterested WHERE broadcastMessageInterested.receiverUser.id=:id AND broadcastMessageInterested.broadcastMessage.id=:broadcast_message_id"
	 * ) BroadcastMessageInterested
	 * getOneBroadcastMessageById(@Param("broadcast_message_id")Long
	 * broadcast_message_id,@Param("id") Long id);
	 * 
	 * @Query("Select broadcastMessageInterested from BroadcastMessageInterested broadcastMessageInterested WHERE broadcastMessageInterested.broadcastMessage.id=:broadcast_message_id AND broadcastMessageInterested.receiverUser.id=:id"
	 * ) public BroadcastMessageInterested
	 * getsimilarValueForResendById(@Param("broadcast_message_id")Long
	 * broadcast_message_id,@Param("id") Long id);
	 */

}

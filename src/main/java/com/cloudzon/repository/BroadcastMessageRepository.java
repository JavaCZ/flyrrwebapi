package com.cloudzon.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.Categories;
import com.cloudzon.model.User;

@Repository("broadcastMessageRepository")
public interface BroadcastMessageRepository extends BaseRepository<BroadcastMessage> {

	// getOneBroadcastMessageById
	@Query("SELECT broadcastMessage FROM BroadcastMessage as broadcastMessage WHERE broadcastMessage.isActive=TRUE AND broadcastMessage.id=:broadcastMessageId ORDER BY broadcastMessage.id DESC")
	public BroadcastMessage getOneBroadcastMessageById(@Param("broadcastMessageId") Long id);

	/*
	 * @Query("SELECT broadcastMessage FROM BroadcastMessage as broadcastMessage WHERE broadcastMessage.isActive=TRUE AND broadcastMessage.id=:broadcast_message_id ORDER BY broadcastMessage.id DESC"
	 * ) public BroadcastMessage
	 * findOneByBroadcastMessageId(@Param("broadcast_message_id") Long id);
	 */

	// getUserByMobile
	@Query("SELECT user FROM User AS user WHERE user.mobileNumber=:mobileNumber")
	public User getUserByMobile(@Param("mobileNumber") String mobileNumber);

	@Query("SELECT broadcastMessage FROM BroadcastMessage as broadcastMessage WHERE broadcastMessage.id =:id")
	public BroadcastMessage findBroadcastMessageById(@Param("id") Long id);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage  WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.user =:user ORDER BY broadcastMessage.id DESC")
	public List<BroadcastMessage> getSellerByBroadcastMessageByUser(@Param("user") User objUser);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage  WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.user =:userId ORDER BY broadcastMessage.id DESC")
	public List<BroadcastMessage> getSellerByBroadcastMessageByUserId(@Param("userId") Long userId);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage  WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.user =:user")
	public List<BroadcastMessage> getBuyerByBroadcastMessageByUser(@Param("user") User objUser);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage  WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.user =:user ORDER BY broadcastMessage.id DESC")
	public List<BroadcastMessage> getFavoriteByBroadcastMessageByUser(@Param("user") User objUser);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage JOIN broadcastMessage.user AS user WHERE broadcastMessage.isActive = TRUE AND user =:userId ORDER BY broadcastMessage.id DESC")
	public BroadcastMessage getBroadcastMessageByUserId(@Param("userId") Long userId);

	/*
	 * @Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage WHERE  broadcastMessage.price >:price AND broadcastMessage.isActive = TRUE  AND broadcastMessage.user !=:user AND broadcastMessage.isBuying = FALSE AND broadcastMessage.isNegotiable =:isNegotiable  AND broadcastMessage.expireIn != CURRENT_DATE  AND 3956*2*ASIN(SQRT(POWER(SIN((:lattitude-ABS(broadcastMessage.lattitude))*PI()/180/2), 2)+COS(:lattitude*PI()/180)*COS(ABS(broadcastMessage.lattitude)*PI()/180)*POWER(SIN((:longitude-broadcastMessage.longitude)*PI()/180/2), 2))) < :rangeInMile "
	 * ) public List<BroadcastMessage> getAllBroadcastMessageIdAndPrice(
	 * 
	 * @Param("lattitude") String lattitude,
	 * 
	 * @Param("longitude") String longitude,
	 * 
	 * @Param("rangeInMile") Double rangeInMile,
	 * 
	 * @Param("user") User user,
	 * 
	 * @Param("isNegotiable")Boolean isNegotiable,
	 * 
	 * @Param("price")Double price);
	 */

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.user !=:user AND broadcastMessage.isBuying = FALSE AND broadcastMessage.isNegotiable =:isNegotiable AND broadcastMessage.expireIn >= CURRENT_DATE AND 3956*2*ASIN(SQRT(POWER(SIN((:lattitude-ABS(broadcastMessage.lattitude))*PI()/180/2), 2)+COS(:lattitude*PI()/180)*COS(ABS(broadcastMessage.lattitude)*PI()/180)*POWER(SIN((:longitude-broadcastMessage.longitude)*PI()/180/2), 2))) < :rangeInMile")
	public List<BroadcastMessage> getAllBroadcastMessageId(@Param("lattitude") String lattitude,
			@Param("longitude") String longitude, @Param("rangeInMile") Double rangeInMile, @Param("user") User use,
			@Param("isNegotiable") Boolean isNegotiable);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.user !=:user AND  broadcastMessage.isBuying = FALSE AND broadcastMessage.price <= :price  AND  broadcastMessage.isNegotiable =:isNegotiable  AND broadcastMessage.expireIn >=:currentDate AND 3956*2*ASIN(SQRT(POWER(SIN((:lattitude-ABS(broadcastMessage.lattitude))*PI()/180/2), 2)+COS(:lattitude*PI()/180)*COS(ABS(broadcastMessage.lattitude)*PI()/180)*POWER(SIN((:longitude-broadcastMessage.longitude)*PI()/180/2), 2))) < :rangeInMile")
	public List<BroadcastMessage> getAllBroadcastMessageIdByPrice(@Param("price") Double price,
			@Param("isNegotiable") Boolean isNegotiable, @Param("lattitude") String lattitude,
			@Param("longitude") String longitude, @Param("rangeInMile") Double rangeInMile, @Param("user") User user,
			@Param("currentDate") Date currentDate);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.user !=:user AND  broadcastMessage.isBuying = FALSE  AND  broadcastMessage.isNegotiable =:isNegotiable  AND broadcastMessage.expireIn >=:currentDate AND 3956*2*ASIN(SQRT(POWER(SIN((:lattitude-ABS(broadcastMessage.lattitude))*PI()/180/2), 2)+COS(:lattitude*PI()/180)*COS(ABS(broadcastMessage.lattitude)*PI()/180)*POWER(SIN((:longitude-broadcastMessage.longitude)*PI()/180/2), 2))) < :rangeInMile")
	public List<BroadcastMessage> getAllBroadcastMessageIdByWithoutPriceWithNegotiable(
			@Param("isNegotiable") Boolean isNegotiable, @Param("lattitude") String lattitude,
			@Param("longitude") String longitude, @Param("rangeInMile") Double rangeInMile, @Param("user") User user,
			@Param("currentDate") Date currentDate);
	


	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.user !=:user AND  broadcastMessage.isBuying = FALSE AND broadcastMessage.expireIn >=:currentDate AND 3956*2*ASIN(SQRT(POWER(SIN((:lattitude-ABS(broadcastMessage.lattitude))*PI()/180/2), 2)+COS(:lattitude*PI()/180)*COS(ABS(broadcastMessage.lattitude)*PI()/180)*POWER(SIN((:longitude-broadcastMessage.longitude)*PI()/180/2), 2))) < :rangeInMile")
	public List<BroadcastMessage> getAllBroadcastMessageIdByWithoutPrice(@Param("lattitude") String lattitude,
			@Param("longitude") String longitude, @Param("rangeInMile") Double rangeInMile, @Param("user") User user,
			@Param("currentDate") Date currentDate);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage JOIN broadcastMessage.user AS user WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.isBuying = FALSE AND broadcastMessage.user != :id AND broadcastMessage.expireIn >= :currentDate ")
	public List<BroadcastMessage> findBroadcastBySearchWithNoRange(@Param("id") Long id,
			@Param("currentDate") Date currentDate);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage JOIN broadcastMessage.user AS user WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.isBuying = FALSE AND broadcastMessage.price <:price AND broadcastMessage.user != :id AND broadcastMessage.expireIn >= CURRENT_DATE ")
	public List<BroadcastMessage> findBroadcastBySearchWithNoRangeAndPrice(@Param("id") Long id,
			@Param("price") Double price);

	// findBroadcastBySearch
	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage  WHERE broadcastMessage.isActive = TRUE AND broadcastMessage.isDelete = FALSE  AND broadcastMessage.expireIn >= CURRENT_DATE AND 3956*2*ASIN(SQRT(POWER(SIN((:lattitude-ABS(broadcastMessage.lattitude))*PI()/180/2), 2)+COS(:lattitude*PI()/180)*COS(ABS(broadcastMessage.lattitude)*PI()/180)*POWER(SIN((:longitude-broadcastMessage.longitude)*PI()/180/2), 2))) < :rangeInMile")
	public List<BroadcastMessage> findBroadcastBySearch(@Param("lattitude") String lattitude,
			@Param("longitude") String longitude, @Param("rangeInMile") Double rangeInMile);

	@Query("SELECT broadcastMessage FROM BroadcastMessage AS broadcastMessage WHERE broadcastMessage.lattitude =:lattitude And broadcastMessage.longitude =:longitude ")
	public BroadcastMessage findOneByBroadcastMessage(@Param("lattitude") String lattitude,
			@Param("longitude") String longitude);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage JOIN broadcastMessage.user AS user WHERE broadcastMessage.isActive = TRUE AND user !=:user AND broadcastMessage.price <=:price")
	public List<BroadcastMessage> findBroadcastMessageByPrice(@Param("price") Double price, @Param("user") User user);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage JOIN broadcastMessage.user AS user WHERE broadcastMessage.isActive = TRUE AND user !=:user")
	public List<BroadcastMessage> findBroadcastMessageByWithoutPrice(@Param("user") User user);

	@Query("SELECT broadcastMessage FROM BroadcastMessage AS broadcastMessage WHERE broadcastMessage.isActive = TRUE AND broadcastMessage =:broadcastMessage AND broadcastMessage.expireIn >=:currentDate  AND  broadcastMessage.isNegotiable =:isNegotiable ")
	public List<BroadcastMessage> findOneByBroadcastMessageByExpireIn(@Param("currentDate") Date currentDate,
			// @Param("dateExpireIn") Date dateExpireIn ,
			@Param("broadcastMessage") BroadcastMessage broadcastMessage, @Param("isNegotiable") Boolean isNegotiable);

	// broadcastMessage.isActive = TRUE
	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage  WHERE  broadcastMessage =:broadcastMessage  AND broadcastMessage.isDelete = TRUE")
	public List<BroadcastMessage> getBroadcastMessageByIsDelete(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage  WHERE  broadcastMessage =:broadcastMessage  AND broadcastMessage.isBuying = TRUE")
	public List<BroadcastMessage> getBroadcastMessageByIsBuying(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage);

	@Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage  WHERE broadcastMessage.isActive = TRUE AND broadcastMessage =:broadcastMessage AND  broadcastMessage.expireIn <=:currentDate  ")
	public List<BroadcastMessage> getBroadcastMessageByIsExpireIn(
			@Param("broadcastMessage") BroadcastMessage broadcastMessage, @Param("currentDate") Date currentDate);

	// @Query("SELECT id, (6371 * ACOS(COS(RADIANS(:lat)) * COS(RADIANS(lattitude))
	// * COS(RADIANS(longitude) - RADIANS(:longi)) + SIN(RADIANS(:lat)) *
	// SIN(RADIANS(lattitude))) AS distance FROM BroadcastMessage HAVING distance
	// <=5")
	// public BroadcastMessage findBroadcastMessage(@Param("lat")String
	// lat,@Param("longi")String longitude);

	@Query("SELECT bm,(6371 * ACOS( COS( RADIANS(:lattitude) ) * COS( RADIANS('lattitude') ) * COS( RADIANS('longitude') - RADIANS(:longitude)) + SIN(RADIANS(:lattitude)) * SIN(RADIANS('lattitude')) )) AS distance FROM BroadcastMessage bm  WHERE bm.isActive = TRUE GROUP BY id HAVING 'distance' < 5 ")
	// @Query("SELECT broadcastMessage FROM BroadcastMessage broadcastMessage WHERE
	// broadcastMessage.isActive = TRUE AND broadcastMessage.isDelete = FALSE AND
	// broadcastMessage.expireIn >= CURRENT_DATE AND
	// 3956*2*ASIN(SQRT(POWER(SIN((:lattitude-ABS(broadcastMessage.lattitude))*PI()/180/2),
	// 2)+COS(:lattitude*PI()/180)*COS(ABS(broadcastMessage.lattitude)*PI()/180)*POWER(SIN((:longitude-broadcastMessage.longitude)*PI()/180/2),
	// 2))) <=0.5")
	public List<BroadcastMessage> findBroadcastMessageNearMe(@Param("lattitude") String lattitude,
			@Param("longitude") String longitude);

}

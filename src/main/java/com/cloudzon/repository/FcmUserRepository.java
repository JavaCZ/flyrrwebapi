package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.DeviceToken;
import com.cloudzon.model.FCMUser;
import com.cloudzon.model.User;
import com.cloudzon.repository.BaseRepository;

public interface FcmUserRepository extends BaseRepository<FCMUser> {

	public List<FCMUser> findFCMUserByDeviceType(String deviceType);

	@Query("SELECT fcmUsers FROM FCMUser AS fcmUsers")
	public List<FCMUser> getAllDeviceToken();

	@Query("SELECT DISTINCT fcmuser FROM User AS userLatLon,FCMUser AS fcmuser "
			+ "WHERE 3956*2*ASIN(SQRT(POWER(SIN((:latitude-ABS(userLatLon.lattitude))*PI()/180/2), 2)+COS(:latitude*PI()/180)*COS(ABS(userLatLon.lattitude)*PI()/180)*POWER(SIN((:longitude-userLatLon.longitude)*PI()/180/2), 2))) < :"
			+ "rangeInMile AND userLatLon = fcmuser.user AND fcmuser.user.isEnableNotification =TRUE AND fcmuser.isActive = TRUE AND fcmuser.user != :user")
	List<FCMUser> getAllUserFromSetlatlong(@Param("latitude") String latitude, @Param("longitude") String longitude,
			@Param("rangeInMile") Double rangeInMile, @Param("user") User user);

	@Query("SELECT DISTINCT fcmuser FROM User AS userLatLon,FCMUser AS fcmuser "
			+ "WHERE 3956*2*ASIN(SQRT(POWER(SIN((:latitude-ABS(userLatLon.lattitude))*PI()/180/2), 2)+COS(:latitude*PI()/180)*COS(ABS(userLatLon.lattitude)*PI()/180)*POWER(SIN((:longitude-userLatLon.longitude)*PI()/180/2), 2))) < :"
			+ "rangeInMile AND userLatLon = fcmuser.user AND fcmuser.user.isEnableNotification =TRUE AND fcmuser.isActive = TRUE AND fcmuser.user != :user")
	List<FCMUser> getAllUserNearMe(@Param("latitude") String latitude, @Param("longitude") String longitude,
			@Param("rangeInMile") Double rangeInMile, @Param("user") User user);

	@Query("SELECT fcmUser FROM DeviceToken AS fcmUser JOIN fcmUser.user As user Where user.id =:reciverId")
	public DeviceToken getReceverUser(@Param("reciverId") Long reciverId);
	

}

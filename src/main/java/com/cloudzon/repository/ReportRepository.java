package com.cloudzon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.BuyerFlyrr;
import com.cloudzon.model.RatingFeedback;
import com.cloudzon.model.Report;
import com.cloudzon.model.User;

public interface ReportRepository extends BaseRepository<Report> {

	@Query("SELECT COUNT(broadcastMessage) FROM Report report JOIN report.broadcastMessage AS broadcastMessage  WHERE broadcastMessage =:broadcastMessage")
	public Integer getBroadcastMessageCountById(@Param("broadcastMessage") BroadcastMessage broadcastMessage);

	public List<Report> getReportByBroadcastMessageAndUser(BroadcastMessage broadcastMessage, User objUser);

	@Query("SELECT report FROM Report AS report JOIN report.broadcastMessage AS broadcastMessage JOIN report.user AS user WHERE broadcastMessage =:broadcastId AND user =:userId")
	public Report getReportFlyrrForBroadcastMessageAndUser(@Param("broadcastId") Long broadcastId,
			@Param("user") Long userId);

}

package com.cloudzon.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

public class UserInterestedIdDTO {

	@NotNull
	private List<Long> userId;

	public UserInterestedIdDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserInterestedIdDTO(List<Long> userId) {
		super();
		this.userId = userId;
	}

	public List<Long> getUserId() {
		return userId;
	}

	public void setUserId(List<Long> userId) {
		this.userId = userId;
	}

}

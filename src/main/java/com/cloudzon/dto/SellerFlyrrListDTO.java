package com.cloudzon.dto;

import java.util.Date;
import java.util.List;

public class SellerFlyrrListDTO {
	private Long broadcastId;
	private String title;
	private String description;
	private Double price;
	private Date expireIn;
	private List<String> categories;
	private String lattitude;
	private String longitude;
	private Boolean active;
	private List<String> images;
	// private Date createdDate;
	private Boolean isBuying;
	private Double rangeInMile;
	
	
	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(Date expireIn) {
		this.expireIn = expireIn;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	/*
	 * public Date getCreatedDate() { return createdDate; } public void
	 * setCreatedDate(Date createdDate) { this.createdDate = createdDate; }
	 */
	public Boolean getIsBuying() {
		return isBuying;
	}

	public void setIsBuying(Boolean isBuying) {
		this.isBuying = isBuying;
	}

	public Double getRangeInMile() {
		return rangeInMile;
	}

	public void setRangeInMile(Double rangeInMile) {
		this.rangeInMile = rangeInMile;
	}
	
	

}

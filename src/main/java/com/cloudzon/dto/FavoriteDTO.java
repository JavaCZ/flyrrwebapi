package com.cloudzon.dto;

import javax.validation.constraints.NotNull;

public class FavoriteDTO {

	@NotNull(message="Brodcast Id Required")
	private Long broadcastId;

	@NotNull(message="Select Your Choise")
	private Boolean isFavorite;

	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

	public Boolean getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

}

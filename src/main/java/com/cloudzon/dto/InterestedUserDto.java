package com.cloudzon.dto;

public class InterestedUserDto {

	private Long userId;
	private String fullName;
	private String profilePicture;
	private String dialcode;
	private String mobileNumber;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getDialcode() {
		return dialcode;
	}

	public void setDialcode(String dialcode) {
		this.dialcode = dialcode;
	}

	public String getMobileNumber() {
		
		return removeCountryCode(mobileNumber, dialcode);
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	
	public InterestedUserDto(Long userId, String fullName, String profilePicture, String dialcode,
			String mobileNumber) {
		super();
		this.userId = userId;
		this.fullName = fullName;
		this.profilePicture = profilePicture;
		this.dialcode = dialcode;
		this.mobileNumber = mobileNumber;
	}

	
	public String removeCountryCode(String strMobWithCD, String ccode) {
		String strNewMob = null;
		if (strMobWithCD.contains(ccode)) {

			strNewMob = strMobWithCD.replace(ccode, "").toString();
		}  else {

			strNewMob = strMobWithCD;
		}

		return strNewMob;
	}
	
}

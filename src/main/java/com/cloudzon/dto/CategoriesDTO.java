package com.cloudzon.dto;

import java.util.List;

public class CategoriesDTO {

	List<String> CategoriesTags;

	public CategoriesDTO(List<String> categoriesTags) {
		super();
		CategoriesTags = categoriesTags;
	}

	public List<String> getCategoriesTags() {
		return CategoriesTags;
	}

	public void setCategoriesTags(List<String> categoriesTags) {
		CategoriesTags = categoriesTags;
	}

}

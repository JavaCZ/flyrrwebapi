package com.cloudzon.dto;

import javax.validation.constraints.NotNull;

public class BuyerDTO {

	@NotNull(message="FlyRR Id Required")
	private Long broadcastId;

	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

}

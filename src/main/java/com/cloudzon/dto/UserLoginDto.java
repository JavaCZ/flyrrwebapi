package com.cloudzon.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class UserLoginDto {

	@NotEmpty(message = "Invalid country code")
	@Size(max = 4, min = 2)
	private String countryCode;

	@NotEmpty(message = "Mobile number can not be null")
	@Size(max = 12, min = 7)
	private String mobileNumber;

	@NotEmpty(message = " Device Model can not be null")
	private String deviceModel;

	@NotEmpty(message = "osVersion can not be null")
	private String oSVersion;

	public String getoSVersion() {
		return oSVersion;
	}

	public void setoSVersion(String oSVersion) {
		this.oSVersion = oSVersion;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getDeviceModel() {
		return deviceModel;
	}

	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

}

package com.cloudzon.dto;

import java.util.List;

public class BroadcastMessageWithCodeDTO {

	private String Lattitude;
	private String longitude;
	private List<SearchFlyrrDetailsResponseDTO> flyrrDetailsDTOs;

	public String getLatitude() {
		return Lattitude;
	}

	public void setLattitude(String lattitude) {
		this.Lattitude = Lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public List<SearchFlyrrDetailsResponseDTO> getFlyrrDetailsDTOs() {
		return flyrrDetailsDTOs;
	}

	public void setFlyrrDetailsDTOs(List<SearchFlyrrDetailsResponseDTO> flyrrDetailsDTOs) {
		this.flyrrDetailsDTOs = flyrrDetailsDTOs;
	}

}

package com.cloudzon.dto;

public class LoginSignUpResponseDTO {

	private Long userId;

	/*
	 * @Size(min = 1, max = 15) private String username;
	 * 
	 * @NotNull private String verificationCode;
	 */

	public LoginSignUpResponseDTO(Long userId) {
		super();
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/*
	 * public String getUsername() { return username; }
	 * 
	 * public void setUsername(String username) { this.username = username; }
	 * 
	 * public String getVerificationCode() { return verificationCode; }
	 * 
	 * public void setVerificationCode(String verificationCode) {
	 * this.verificationCode = verificationCode; }
	 */

}

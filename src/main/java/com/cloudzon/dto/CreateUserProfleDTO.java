package com.cloudzon.dto;

import org.hibernate.validator.constraints.NotEmpty;

public class CreateUserProfleDTO {

	@NotEmpty(message = "Enter user Name")
	private String firstName;

	/*
	 * @NotEmpty(message = " Enter Bank Name ") private String bankName;
	 */

	@NotEmpty(message = "complete your profile picture")
	private String profilePicture;

	private boolean isSkip;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public boolean isSkip() {
		return isSkip;
	}

	public void setSkip(boolean isSkip) {
		this.isSkip = isSkip;
	}

	/*
	 * @NotEmpty(message = " Enter Account Number ") private String accountNumber;
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * //get set method public String getBankName() { return bankName; }
	 * 
	 * public void setBankName(String bankName) { this.bankName = bankName; }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * public String getAccountNumber() { return accountNumber; }
	 * 
	 * public void setAccountNumber(String accountNumber) { this.accountNumber =
	 * accountNumber; }
	 */

}

package com.cloudzon.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ResponseDTO {

	private Boolean isSuccess;
	private Boolean showMessage;
	private Object response;

	/*
	 * public ResponseDTO(Object response) { super(); this.response = response; }
	 */

	public ResponseDTO(Boolean showMessage, Object response) {
		super();
		// this.isSuccess = Boolean.TRUE;
		this.response = response;
		this.showMessage = showMessage;
	}

	public ResponseDTO(Boolean isSuccess, Boolean showMessage, Object response) {
		super();
		this.isSuccess = isSuccess;
		this.showMessage = showMessage;
		this.response = response;
	}
	// get set Method
	public Boolean getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(Boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	public Boolean getShowMessage() {
		return showMessage;
	}

	public void setShowMessage(Boolean showMessage) {
		this.showMessage = showMessage;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

}

package com.cloudzon.dto;

import javax.validation.constraints.NotNull;

public class RatingFeedbackDTO {

	@NotNull
	private Long broadcastId;

	@NotNull
	private Long userId;

	@NotNull
	private Integer rate;

	@NotNull
	private String feedback;

	private String fullName;

	private String profilePicture;

	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

}

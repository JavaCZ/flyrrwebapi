package com.cloudzon.dto;

import javax.validation.constraints.NotNull;

import com.cloudzon.model.BroadcastMessage;

public class BroadcastMessageInterestedDTO {

	@NotNull(message="FlyRR ID Must Required")
	private Long broadcastId;

	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

}

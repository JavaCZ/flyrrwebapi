package com.cloudzon.dto;

import javax.validation.constraints.NotNull;



public class NotificationDTO {

	@NotNull(message="Select Notification Status")
	Boolean isNotification;

	public Boolean getIsNotification() {
		return isNotification;
	}

	public void setIsNotification(Boolean isNotification) {
		this.isNotification = isNotification;
	}

}

package com.cloudzon.dto;

import java.util.Date;
import java.util.List;

import org.hibernate.validator.constraints.NotEmpty;

public class SearchFlyrrDTO {

	private List<String> categories;
	private Double price;
	private Long brodcastId;
	private String searchWord;
	private Long id;
	private Integer isNegotiable;
	@NotEmpty(message="lattitude requierd")
	private String lattitude;
	@NotEmpty(message="longitude Required")
	private String longitude;
	private Long expireIn;
	private Double rangeInMile;

	public Long getBrodcastId() {
		return brodcastId;
	}

	public void setBrodcastId(Long brodcastId) {
		this.brodcastId = brodcastId;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getSearchWord() {
		return searchWord;
	}

	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}

	public Integer getIsNegotiable() {
		return isNegotiable;
	}

	public void setIsNegotiable(Integer isNegotiable) {
		this.isNegotiable = isNegotiable;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Long getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(Long expireIn) {
		this.expireIn = expireIn;
	}

	public Double getRangeInMile() {
		return rangeInMile;
	}

	public void setRangeInMile(Double rangeInMile) {
		this.rangeInMile = rangeInMile;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

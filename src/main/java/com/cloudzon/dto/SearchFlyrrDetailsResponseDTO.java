
package com.cloudzon.dto;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cloudzon.model.BroadcastMessage;

public class SearchFlyrrDetailsResponseDTO {

	private Long broadcastId;
	
	private Double price;
	private String title;
	private String description;
	private String lattitude;
	private String longitude;
	private List<String> images;
	private Date expireIn;
	
	private Date createdDate;
	private Double rangeInMile;
	private List<String> categories;
	private Boolean isNegotiable;
	private Boolean active;
	private Boolean isFavorite;
	private Boolean isDelete;
	private Boolean isBuying;
	private UserFlyRRResponseDTO user;
	
	
	
	// private double rate;

	/*
	 * private BroadcastMessage flyrrList;
	 * 
	 * public SearchFlyrrDetailsResponseDTO() { super(); // TODO Auto-generated
	 * constructor stub }
	 * 
	 * public SearchFlyrrDetailsResponseDTO(BroadcastMessage flyrrList) { super();
	 * this.flyrrList=flyrrList; }
	 */

	/*
	 * public BroadcastMessage getFlyrrList() { return flyrrList; }
	 * 
	 * public void setFlyrrList(BroadcastMessage flyrrList) { this.flyrrList =
	 * flyrrList; }
	 */



	public UserFlyRRResponseDTO getUser() {
		return user;
	}

	public void setUser(UserFlyRRResponseDTO user) {
		this.user = user;
	}

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public Boolean getIsBuying() {
		return isBuying;
	}

	public void setIsBuying(Boolean isBuying) {
		this.isBuying = isBuying;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Boolean getIsNegotiable() {
		return isNegotiable;
	}

	public void setIsNegotiable(Boolean isNegotiable) {
		this.isNegotiable = isNegotiable;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Date getExpireIn() {
		return expireIn;
	}

	public void setExpireIn() {
		this.expireIn = expireIn;
	}

	public Double getRangeInMile() {
		return rangeInMile;
	}

	public void setRangeInMile() {
		this.rangeInMile = rangeInMile;
	}

	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setExpireIn(Date expireIn) {
		this.expireIn = expireIn;
	}

	public void setRangeInMile(Double rangeInMile) {
		this.rangeInMile = rangeInMile;
	}

	public Boolean getIsFavorite() {
		return isFavorite;
	}

	public void setIsFavorite(Boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	/*public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getProfilePicture1() {
		return profilePicture1;
	}

	public void setProfilePicture1(String profilePicture1) {
		this.profilePicture1 = profilePicture1;
	}*/


	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/*public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}*/

}

package com.cloudzon.dto;

import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.User;

public class BroadcastInterestedResponseDTO {

	private Long broadcastid;
	private Long userId;
	private String fullName;
	private String profilePicture;
	private String dialCode;
	private String mobileNumber;

	public String getDialCode() {
		return dialCode;
	}

	public void setDialCode(String dialCode) {
		this.dialCode = dialCode;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public BroadcastInterestedResponseDTO() {
		super();
	}

	public BroadcastInterestedResponseDTO(Long broadcastid, Long user, String fullName, String profilePicture) {
		super();
		this.broadcastid = broadcastid;
		this.userId = user;
		this.fullName = fullName;
		this.profilePicture = profilePicture;
	}

	public Long getBroadcastId() {
		return broadcastid;
	}

	public void setBroadcastId(Long broadcastid) {
		this.broadcastid = broadcastid;
	}



	public Long getBroadcastid() {
		return broadcastid;
	}

	public void setBroadcastid(Long broadcastid) {
		this.broadcastid = broadcastid;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

}

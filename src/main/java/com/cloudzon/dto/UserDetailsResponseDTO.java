package com.cloudzon.dto;

import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

public class UserDetailsResponseDTO {

	private Long userId;
	//private Date createdDate;
	@NotEmpty(message = "FullName is can not be null or Empty")
	private String fullName;
	
	private String profilePicture;
	private Double rate;
	private String mobileNumber;
	
	
	
	
	
	
	
	public UserDetailsResponseDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public UserDetailsResponseDTO(Long userId, String fullName, Double rate, String profilePicture,
			String mobileNumber) {
		super();
		this.userId = userId;
		//this.createdDate=createdDate;
		this.fullName = fullName;
		this.rate = rate;
		this.profilePicture = profilePicture;
		
		this.mobileNumber = mobileNumber;
	
	}
	
	
	/*public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}*/
	
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	
	public Double getRate() {
		return rate;
	}
	public void setRate(Double rate) {
		this.rate = rate;
	}
	
	
	public String getProfilePicture() {
		return profilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
	
	
	
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	
	
	
	
	
	

}

package com.cloudzon.dto;

import java.util.List;

public class UserDetailsByIdDTO {

	
	List<Long> userId;

	public List<Long> getUserId() {
		return userId;
	}

	public void setUserId(List<Long> userId) {
		this.userId = userId;
	}
	
	
}

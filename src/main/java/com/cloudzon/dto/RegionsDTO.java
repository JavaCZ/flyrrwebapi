package com.cloudzon.dto;

public class RegionsDTO {

	private String regionName;
	private String regionCountryCode;
	private String regionCurrencySymbol;
	private String regionCurrencyCode;

	public RegionsDTO() {
		super();
	}

	public RegionsDTO(String regionName, String regionCountryCode, String regionCurrencySymbol,
			String regionCurrencyCode) {
		super();
		this.regionName = regionName;
		this.regionCountryCode = regionCountryCode;
		this.regionCurrencySymbol = regionCurrencySymbol;
		this.regionCurrencyCode = regionCurrencyCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getRegionCountryCode() {
		return regionCountryCode;
	}

	public void setRegionCountryCode(String regionCountryCode) {
		this.regionCountryCode = regionCountryCode;
	}

	public String getRegionCurrencySymbol() {
		return regionCurrencySymbol;
	}

	public void setRegionCurrencySymbol(String regionCurrencySymbol) {
		this.regionCurrencySymbol = regionCurrencySymbol;
	}

	public String getRegionCurrencyCode() {
		return regionCurrencyCode;
	}

	public void setRegionCurrencyCode(String regionCurrencyCode) {
		this.regionCurrencyCode = regionCurrencyCode;
	}

}

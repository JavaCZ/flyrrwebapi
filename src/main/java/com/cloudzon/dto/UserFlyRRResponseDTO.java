package com.cloudzon.dto;

public class UserFlyRRResponseDTO {

	private Long userId;
	private String mobileNumber;
	private String fullName;
	private String profilePicture;
	private Double rate;
	
	public UserFlyRRResponseDTO(Long userId,String mobile,String fullName,String profilePic,Double rate) {
		this.fullName = fullName;
		this.userId = userId;
		this.mobileNumber = mobile;
		this.profilePicture = profilePic;
		this.rate = rate;
	}
	
	
	
	
	public String getProfilePicture() {
		return profilePicture;
	}




	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}




	public Double getRate() {
		return rate;
	}


	public void setRate(Double rate) {
		this.rate = rate;
	}



	

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


}

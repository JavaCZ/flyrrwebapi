package com.cloudzon.dto;

import java.util.List;

import com.cloudzon.model.BroadcastMessage;

public class FlyrrDetailsListDTO {

	private List<SearchFlyrrDetailsResponseDTO> flyrrDetails;

	/*
	 * public BroadcastMessage getBroadcastMessage() { return broadcastMessage; }
	 * 
	 * public void setBroadcastMessage(BroadcastMessage broadcastMessage) {
	 * this.broadcastMessage = broadcastMessage; }
	 */
	public FlyrrDetailsListDTO() {
		super();
	}

	public FlyrrDetailsListDTO(List<SearchFlyrrDetailsResponseDTO> codeDTOs) {
		super();
		this.flyrrDetails = codeDTOs;
	}

	public List<SearchFlyrrDetailsResponseDTO> getflyrrDetails() {
		return flyrrDetails;
	}

	public void setflyrrDetails(List<SearchFlyrrDetailsResponseDTO> flyrrDetails) {
		this.flyrrDetails = flyrrDetails;
	}

}

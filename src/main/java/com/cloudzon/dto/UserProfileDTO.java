package com.cloudzon.dto;

public class UserProfileDTO {

	private ProfileDTO userProfile;
	private RegionsDTO region;

	public UserProfileDTO() {
	}

	public UserProfileDTO(ProfileDTO userProfile, RegionsDTO region) {
		super();
		this.userProfile = userProfile;
		this.region = region;
	}

	public ProfileDTO getUserProfile() {
		return userProfile;
	}

	public void setUserProfile(ProfileDTO userProfile) {
		this.userProfile = userProfile;
	}

	public RegionsDTO getRegion() {
		return region;
	}

	public void setRegion(RegionsDTO region) {
		this.region = region;
	}

	private Long id;
	private String firstName;
	private String profilePicture;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

}

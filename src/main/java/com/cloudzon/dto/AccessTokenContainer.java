package com.cloudzon.dto;

import java.io.Serializable;

public class AccessTokenContainer implements Serializable {

	private static final long serialVersionUID = 1L;
	private String access_token;
	private Long userId;
	private String fullName;
	private String profilePicture;

	// private String expires_in;
	private Double rate;
	private boolean isEnableNotification = false;
	private boolean completeProfile = false;

	public AccessTokenContainer() {

	}

	// public AccessTokenContainer(long id,String profilePicture,String firstName)
	// {
	// this.id=id;
	// this.profilePicture=profilePicture;
	// this.firstName=firstName;
	//
	// }

	public AccessTokenContainer(String access_token, Long userId, String fullName, String profilePicture, Double rate,
			boolean completeProfile, boolean isEnableNotification) {
		super();
		this.userId = userId;
		this.access_token = access_token;
		// this.expires_in = expires_in;
		this.fullName = fullName;
		this.profilePicture = profilePicture;
		this.rate = rate;
		this.completeProfile = completeProfile;
		this.isEnableNotification = isEnableNotification;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	// public String getExpires_in() {
	// return expires_in;
	// }
	//
	// public void setExpires_in(String expires_in) {
	// this.expires_in = expires_in;
	// }

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public boolean isEnableNotification() {
		return isEnableNotification;
	}

	public void setEnableNotification(boolean isEnableNotification) {
		this.isEnableNotification = isEnableNotification;
	}

	public boolean isCompleteProfile() {
		return completeProfile;
	}

	public void setCompleteProfile(boolean completeProfile) {
		this.completeProfile = completeProfile;
	}

}

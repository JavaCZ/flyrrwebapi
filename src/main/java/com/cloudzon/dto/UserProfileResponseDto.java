package com.cloudzon.dto;

import org.hibernate.validator.constraints.NotEmpty;


public class UserProfileResponseDto {

	private Long userId;
	@NotEmpty(message = "FullName is can not be null or Empty")
	private String fullName;
	private String rate;
	private String profilePicture;
	private String dialCode;
	private String mobileNumber;

	private String cardType;
	private String bankName;
	private String bankAccountNumber;
	private Boolean enableNotification;
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getProfilePicture() {
		return profilePicture;
	}
	public void setProfilePicture(String profilePicture) {
		this.profilePicture = profilePicture;
	}
	public String getDialCode() {
		return dialCode;
	}
	public void setDialCode(String dialCode) {
		this.dialCode = dialCode;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public Boolean getEnableNotification() {
		return enableNotification;
	}
	public void setEnableNotification(Boolean enableNotification) {
		this.enableNotification = enableNotification;
	}
	
	
	
	
	
	
	

	
}

package com.cloudzon.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

public class ProfileDTO {

	private Long userId;
	@NotEmpty(message = "FullName is can not be null or Empty")
	private String fullName;
	private String rate;
	private String profilePicture1;
	// private double rate1;
	private String dialCode;
	private String mobileNumber;

	private String cardType;
	private String bankName;
	private String bankAccountNumber;
	private MultipartFile profilePicture;

	private Boolean isEnabledNotification;

	public ProfileDTO() {
		super();
	}

	/*
	 * public ProfileDTO(Long userId, String fullName, MultipartFile profilePicture,
	 * String mobileNumber, String bankName, String bankAccountNumber,String
	 * cardType,double rate) { super(); this.userId = userId; this.fullName =
	 * fullName; this.profilePicture = profilePicture; this.mobileNumber =
	 * mobileNumber; this.bankName = bankName; this.bankAccountNumber =
	 * bankAccountNumber; this.rate=rate; this.cardType=cardType; }
	 */

	public ProfileDTO(Long userId, String fullName, String profilePicture1, String mobileNumber, String bankName,
			String bankAccountNumber, String cardType, String rate, String dialCode) {
		super();
		this.userId = userId;
		this.fullName = fullName;
		this.profilePicture1 = profilePicture1;
		this.mobileNumber = mobileNumber;
		this.bankName = bankName;
		this.bankAccountNumber = bankAccountNumber;
		this.cardType = cardType;
		this.rate = rate;
		this.dialCode = dialCode;
	}

	public ProfileDTO(Long userId, String fullName, String profilePicture1, String dialCode, String mobileNumber) {
		super();
		this.userId = userId;
		this.fullName = fullName;
		this.profilePicture1 = profilePicture1;
		this.dialCode = dialCode;
		this.mobileNumber = mobileNumber;

	}

	public ProfileDTO(Long userId, String fullName, String profilePicture1, String mobileNumber, String bankName,
			String bankAccountNumber) {
		super();
		this.userId = userId;
		this.fullName = fullName;
		this.profilePicture1 = profilePicture1;
		this.mobileNumber = mobileNumber;
		this.bankName = bankName;
		this.bankAccountNumber = bankAccountNumber;

	}

	public Boolean getIsEnabledNotification() {
		return isEnabledNotification;
	}

	public void setIsEnabledNotification(Boolean isEnabledNotification) {
		this.isEnabledNotification = isEnabledNotification;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getProfilePicture1() {
		return profilePicture1;
	}

	public void setProfilePicture1(String profilePicture1) {
		this.profilePicture1 = profilePicture1;
	}

	public MultipartFile getProfilePicture() {
		return profilePicture;
	}

	public void setProfilePicture(MultipartFile profilePicture) {
		this.profilePicture = profilePicture;
	}

	public String getDialCode() {
		return dialCode;
	}

	public void setDialCode(String dialCode) {
		this.dialCode = dialCode;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	/*
	 * public double getRate1() { return rate1; }
	 * 
	 * 
	 * public void setRate1(double rate1) { this.rate1 = rate1; }
	 */

}

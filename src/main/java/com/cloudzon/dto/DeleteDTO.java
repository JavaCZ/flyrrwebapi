package com.cloudzon.dto;

import javax.validation.constraints.NotNull;

public class DeleteDTO {

	@NotNull(message="broadcastId Cannot Be Null")
	private Long broadcastId;

	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

}

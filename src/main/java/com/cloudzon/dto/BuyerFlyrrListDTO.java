package com.cloudzon.dto;

import java.util.Date;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cloudzon.model.User;

public class BuyerFlyrrListDTO {

	private Long broadcastId;
	private String title;
	private String description;
	private Double price;
	private Date expireIn;
	private List<String> categories;
	private String lattitude;
	private String longitude;
	private Boolean active;
	private List<String> images;
	private Date createdDate;
	private UserFlyRRResponseDTO user;
	private Double rangeInMile;
	// private String rate;
	// private String dialCode;
	// private String bankName;
	// private String bankAccountNumber;
	// private MultipartFile profilePicture;
	// private String cardType;
	// private Boolean isEnabledNotification;
	
	

	public Long getBroadcastId() {
		return broadcastId;
	}

	public UserFlyRRResponseDTO getUser() {
		return user;
	}

	public void setUser(UserFlyRRResponseDTO user) {
		this.user = user;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(Date expireIn) {
		this.expireIn = expireIn;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public List<String> getImages() {
		return images;
	}

	public void setImages(List<String> images) {
		this.images = images;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Double getRangeInMile() {
		return rangeInMile;
	}

	public void setRangeInMile(Double rangeInMile) {
		this.rangeInMile = rangeInMile;
	}

	/*
	 * public String getDialCode() { return dialCode; } public void
	 * setDialCode(String dialCode) { this.dialCode = dialCode; } public String
	 * getBankName() { return bankName; } public void setBankName(String bankName) {
	 * this.bankName = bankName; } public String getBankAccountNumber() { return
	 * bankAccountNumber; } public void setBankAccountNumber(String
	 * bankAccountNumber) { this.bankAccountNumber = bankAccountNumber; } public
	 * MultipartFile getProfilePicture() { return profilePicture; } public void
	 * setProfilePicture(MultipartFile profilePicture) { this.profilePicture =
	 * profilePicture; } public String getCardType() { return cardType; } public
	 * void setCardType(String cardType) { this.cardType = cardType; }
	 */
	/*
	 * public Boolean getIsEnabledNotification() { return isEnabledNotification; }
	 * public void setIsEnabledNotification(Boolean isEnabledNotification) {
	 * this.isEnabledNotification = isEnabledNotification; }
	 */

}

package com.cloudzon.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.MultipartFile;

public class BroadcastMessageDTO {

	
	private Long broadcastId;
	@NotEmpty(message = "Title Required")
	private String title;
	
	@NotEmpty(message = "Description Required")
	private String description;
	
	@NotEmpty(message = "Images Required")
	private List<MultipartFile> images;
	
	@NotNull(message = "Price Required")
	private Double price;
	
	@NotEmpty(message = "Price Required")
	private Date expireIn;
	// private int expireInDays;
	@NotEmpty(message = "Category Required")
	private List<String> categories;
	private String lattitude;
	private String longitude;
	private Boolean active;
	private Boolean isNegotiable;
	private Boolean isBuying;

	/*
	 * public BroadcastMessageDTO(Long broadcastId,String title, String description,
	 * String lattitude, String longitude, Boolean active) { super();
	 * this.broadcastId=broadcastId; this.title = title; this.description =
	 * description; this.lattitude = lattitude; this.longitude = longitude;
	 * //this.active = active; }
	 * 
	 * public BroadcastMessageDTO(Long broadcastId, String title, String
	 * description, MultipartFile images, Double price, Calendar expireIn, String
	 * categories, String lattitude, String longitude) { super(); this.broadcastId =
	 * broadcastId; this.title = title; this.description = description; this.images
	 * = images; this.price = price; this.expireIn = expireIn; this.categories =
	 * categories; this.lattitude = lattitude; this.longitude = longitude; }
	 */

	// get set method
	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<MultipartFile> getImages() {
		return images;
	}

	public void setImages(List<MultipartFile> images) {
		this.images = images;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getExpireIn() {
		return expireIn;
	}

	public void setExpireIn(Date expireIn) {
		this.expireIn = expireIn;
	}

	/*
	 * public int getExpireInDays() { return expireInDays; }
	 * 
	 * public void setExpireInDays(int expireInDays) { this.expireInDays =
	 * expireInDays; }
	 */

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(List<String> categories) {
		this.categories = categories;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Boolean getIsNegotiable() {
		return isNegotiable;
	}

	public void setIsNegotiable(Boolean isNegotiable) {
		this.isNegotiable = isNegotiable;
	}

	public Boolean getIsBuying() {
		return isBuying;
	}

	public void setIsBuying(Boolean isBuying) {
		this.isBuying = isBuying;
	}

}

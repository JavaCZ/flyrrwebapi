package com.cloudzon.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class ReportDTO {

	@NotNull(message = "User id can not be null")
	private Long userId;
	
	@NotNull(message = "Brodcast id can not be null")
	private Long broadcastId;
	
	@NotEmpty(message = "Invalid verification code")
	private String comment;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(Long broadcastId) {
		this.broadcastId = broadcastId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}

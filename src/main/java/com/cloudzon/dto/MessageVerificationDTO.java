package com.cloudzon.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class MessageVerificationDTO {

	@NotEmpty(message = "Invalid verification code")
	private String verificationCode;

	@NotNull(message = "User id can not be null")
	private Long userId;

	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}

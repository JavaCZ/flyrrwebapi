package com.cloudzon.dto;

public class FlyRRNotificationDto {

	Long brodcastId;
	String title;
	String message;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Long getBrodcastId() {
		return brodcastId;
	}

	public void setBrodcastId(Long brodcastId) {
		this.brodcastId = brodcastId;
	}
}

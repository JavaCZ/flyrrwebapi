package com.cloudzon.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.Principal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.NotFoundException;

import org.apache.tomcat.util.bcel.classfile.Constant;
import org.json.JSONException;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerMapping;

import com.cloudzon.common.Constants;
import com.cloudzon.dto.AccessTokenContainer;
import com.cloudzon.dto.BroadcastInterestedResponseDTO;
import com.cloudzon.dto.BroadcastMessageDTO;
import com.cloudzon.dto.BroadcastMessageInterestedDTO;
import com.cloudzon.dto.BuyerDTO;
import com.cloudzon.dto.BuyerFlyrrListDTO;
import com.cloudzon.dto.DeleteDTO;
import com.cloudzon.dto.DeviceTokenDTO;
import com.cloudzon.dto.ErrorDto;
import com.cloudzon.dto.FavoriteDTO;
import com.cloudzon.dto.FcmUserDTO;
import com.cloudzon.dto.FlyrrDetailsListDTO;
import com.cloudzon.dto.LoginSignUpResponseDTO;
import com.cloudzon.dto.MessageVerificationDTO;
import com.cloudzon.dto.NotificationDTO;
import com.cloudzon.dto.ProfileDTO;
import com.cloudzon.dto.RatingFeedbackDTO;
import com.cloudzon.dto.ReportDTO;
import com.cloudzon.dto.ResponseDTO;
import com.cloudzon.dto.ResponseMessageDTO;
import com.cloudzon.dto.SearchFlyrrDTO;
import com.cloudzon.dto.SearchFlyrrDetailsResponseDTO;
import com.cloudzon.dto.SellerFlyrrListDTO;
import com.cloudzon.dto.UserDetailsByIdDTO;
import com.cloudzon.dto.UserInterestedIdDTO;
import com.cloudzon.dto.UserLoginDto;
import com.cloudzon.dto.UserProfileDTO;
import com.cloudzon.dto.UserProfileResponseDto;
import com.cloudzon.exception.AuthorizationException;
import com.cloudzon.exception.MultipartSizeValidationException;
import com.cloudzon.exception.NotFoundException.NotFound;
import com.cloudzon.exception.NotFoundFlyrrException;
import com.cloudzon.exception.NotFoundFlyrrException.NotFound2;
import com.cloudzon.model.BroadcastMessage;
import com.cloudzon.model.BroadcastMessageInterested;
import com.cloudzon.model.NotificationUserDto;
import com.cloudzon.model.User;
import com.cloudzon.security.security.jwt.JWTConfigurer;
import com.cloudzon.security.security.jwt.TokenProvider;
import com.cloudzon.service.NotificationService;
import com.cloudzon.service.UserService;

import springfox.documentation.spring.web.json.Json;

@RestController
@RequestMapping("/api/user")

public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	/*
	 * @Autowired private ApplicationProperties config;
	 */

	@Autowired
	private UserService userService;

	@Autowired
	NotificationService notificationService;

	private final TokenProvider tokenProvider;

	private final AuthenticationManager authenticationManager;

	public UserController(TokenProvider tokenProvider, AuthenticationManager authenticationManager) {
		this.tokenProvider = tokenProvider;
		this.authenticationManager = authenticationManager;
	}

	@RequestMapping(value = "/")
	public String index() {
		return "index";
	}

	@PostMapping(value = "/signUpOrlogin", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public ResponseDTO signUpOrlogin(@Valid @RequestBody UserLoginDto loginDto)
			throws JSONException, FileNotFoundException, IOException, ParseException {
		logger.info("signUpOrlogin");
		return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
				new LoginSignUpResponseDTO(this.userService.signUpOrlogin(loginDto)));
	}

	@PostMapping(value = "/verifyLogin", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO verifyLogin(@Valid @RequestBody MessageVerificationDTO verificationDTO,
			HttpServletResponse response) {
		logger.info("verifyLogin");
		try {
			User user = this.userService.verifyLogin(verificationDTO);
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
					user.getUserName(), "NoPassword");
			try {
				Authentication authentication = this.authenticationManager.authenticate(authenticationToken);
				SecurityContextHolder.getContext().setAuthentication(authentication);
				boolean rememberMe = true;
				String jwt = tokenProvider.createToken(authentication, rememberMe);
				response.addHeader(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						this.userService.getAccessTokenContainer(user, jwt));
			} catch (AuthenticationException ae) {
				logger.trace("Authentication exception trace: {}", ae);
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
				// return new ResponseDTO(Boolean.FALSE, Boolean.TRUE,
				// "AuthorizationException");
			}
		} finally {
		}
	}

	// Verification Call

	// @RequestMapping(value = "/verificationCall", method = RequestMethod.POST,
	// produces = { MediaType.APPLICATION_JSON_VALUE })
	// @ResponseStatus(value = HttpStatus.OK)
	//
	// public LoginSignUpResponseDTO verificationCall(@Valid @RequestBody
	// UserLoginDto loginDto,HttpServletRequest request) throws
	// UnsupportedEncodingException {
	// logger.info("verificationCall");
	//
	// StringBuilder responseUrl = new StringBuilder();
	//
	// //responseUrl.append(config.getPlivoCallURL()).append("/plivoCall.xml");
	//
	// return new LoginSignUpResponseDTO(this.userService.verificationCall(
	// loginDto, responseUrl.toString()));
	// }
	//

	// Logout

	/*
	 * @RequestMapping(value = "logout", produces = {
	 * MediaType.APPLICATION_JSON_VALUE })
	 * 
	 * @ResponseStatus(value = HttpStatus.OK)
	 * 
	 * @ResponseBody public ResponseMessageDTO logout(HttpServletRequest request)
	 * throws UnsupportedEncodingException { String access_token = null; try { if
	 * (StringUtils.hasText(request.getHeader("Authorization"))) { access_token =
	 * request.getHeader("Authorization"); } else { access_token =
	 * URLEncoder.encode( request.getParameter("access_token"), Constants.UTF8); }
	 * if (StringUtils.hasText("access_token")) {
	 * this.userService.logout(access_token); return new
	 * ResponseMessageDTO("Logout"); } } finally {
	 * 
	 * } return null; }
	 */
	// Screen 3 UpdateUserProfile &Picture (ExStuff)
	@PostMapping(value = "/updateProfile", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO updateUserProfile(@RequestParam(value = "fullName", required = true) String fullName,
			@RequestParam(value = "images", required = false) MultipartFile profilePicture, Principal principal,
			HttpServletRequest request) throws IOException {

		logger.info("updateProfile");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				ProfileDTO profileDTO = new ProfileDTO();
				if (fullName != null && !fullName.isEmpty()) {
					profileDTO.setFullName(fullName);

				}
				// profileDTO.setFullName(fullName);
				if (profilePicture != null) {
					profileDTO.setProfilePicture(profilePicture);
				}

				this.userService.updateUserProfile(user, request, profileDTO);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						new ResponseMessageDTO(Constants.UpdateProfilePictureMessage));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
	}

	// getUserProfile Screen 5 [24-01-2019]
	@GetMapping(value = "/getUserProfile", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO getUserProfile(Principal principal, HttpServletRequest request) {
		logger.info("userProfile");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				Map<String, UserProfileResponseDto> response = new HashMap<>();
				response.put("user", this.userService.getUserProfile(user));
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE, this.userService.getUserProfile(user));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
				// return new ResponseDTO(Boolean.FALSE, Boolean.TRUE,new
				// AuthorizationException("Session expired ", Boolean.TRUE));
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
	}

	@GetMapping(value = "/getReceiptList", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO getReceiptList(@RequestParam(name = "broadcastId", required = true) Long broadcastId,
			Principal principal, HttpServletRequest request) {
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						this.userService.getReceiptList(broadcastId, user, request));
			}
		} finally {

		}
		return null;

	}

	// created Flyrrr (ExStuff)
	@PostMapping(value = "/createBroadcast", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO createBroadcastMessage(
			@Valid @RequestParam(value = "images", required = false) MultipartFile[] images,
			@RequestParam(value = "title", required = true) String title,
			@RequestParam(value = "description", required = true) String description,
			@RequestParam(value = "lattitude", required = true) String lattitude,
			@RequestParam(value = "longitude", required = true) String longitude,
			@RequestParam(value = "expireIn", required = true) Long expireIn,
			@RequestParam(value = "price", required = true) double price,
			@RequestParam(value = "isNegotiable", required = true) boolean isNegotiable,
			@RequestParam(value = "categories", required = true) String categories, HttpServletRequest request,
			Principal principal) throws Exception, MaxUploadSizeExceededException {
		logger.info("Create Flyrr Message");

		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {

				BroadcastMessageDTO broadcastMessageDTO = new BroadcastMessageDTO();
				broadcastMessageDTO.setTitle(title);
				broadcastMessageDTO.setDescription(description);
				broadcastMessageDTO.setLattitude(lattitude);
				broadcastMessageDTO.setLongitude(longitude);
				broadcastMessageDTO.setExpireIn(new Date(expireIn));
				broadcastMessageDTO.setPrice(price);
				broadcastMessageDTO.setImages(Arrays.asList(images));
				broadcastMessageDTO.setCategories(Arrays.asList(categories.split(",")));
				broadcastMessageDTO.setIsNegotiable(isNegotiable);

				/*
				 * if(images!=null){ broadcastMessageDTO.setImages(Arrays.asList(images)); }
				 * else{ broadcastMessageDTO.setImages(null); }
				 */
				this.userService.createBroadcast(broadcastMessageDTO, user, request);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						new ResponseMessageDTO(Constants.FlyrrCreateMessage));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
	}

	@GetMapping(value = "/getSellingFlyrr", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO sellingFlyrr(Principal principal, HttpServletRequest request) {
		logger.info("Start Selling Flyrr");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				HashMap<String, Object> response = new HashMap<>();
				response.put("SellerBroadcast", this.userService.sellingFlyrr(user));
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE, this.userService.sellingFlyrr(user));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}

	}

	@PostMapping(value = "/buyingFlyrr", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO buyer(@RequestBody @Valid BuyerDTO buyerDTO, Principal principal) {
		logger.info("buying flyrr");
		User user = null;

		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				this.userService.buyingFlyrr(user, buyerDTO);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE, new ResponseMessageDTO(Constants.BuyerMessage));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}

	}

	@GetMapping(value = "/getBuyingFlyrr", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO buyingFlyrr(Principal principal, HttpServletRequest request) {
		logger.info("Start buyingFlyrr ");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				HashMap<String, Object> response = new HashMap<>();
				response.put("BuyerBroadcast", this.userService.getBuyingFlyrr(user));
				this.userService.getBuyingFlyrr(user);

				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE, this.userService.getBuyingFlyrr(user));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}

	}

	@PostMapping(value = "/favorite", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO favorite(@RequestBody @Valid FavoriteDTO favoriteDTO, Principal principal) {
		logger.info("favourite");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				this.userService.favorite(user, favoriteDTO);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						new ResponseMessageDTO(Constants.FavoriteFlyrrMessage));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
	}

	@GetMapping(value = "/getFavoriteFlyrr", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO favoriteFlyrr(Principal principal) {
		logger.info("Start Favorite Flyrr Api Process");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				HashMap<String, Object> response = new HashMap<>();
				response.put("FavoriteBroadcast", this.userService.getFevorietFlyRRsByUser(user));
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE, this.userService.getFevorietFlyRRsByUser(user));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}

	}

	@PostMapping(value = "/deleteFlyrr", consumes = { MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO deleteFlyrr(@Valid @RequestBody DeleteDTO deleteDTO, Principal principal) {
		logger.info("Delete your flyrr");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				this.userService.deleteFlyrr(deleteDTO, user);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						new ResponseMessageDTO(Constants.DeleteFlyrrMessage));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}

	}

	@RequestMapping(value = "/deactivateAccount", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO deactivateAccount(Principal principal, HttpServletRequest request) {

		logger.info("deactivateAccount in flyrr");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				this.userService.deactivateAccount(user);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						new ResponseMessageDTO(Constants.DeactivateFlyrrMessage));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
	}

	@PostMapping(value = "/nearByFlyrr", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO nearByFlyrr(@Valid @RequestBody SearchFlyrrDTO searchFlyrrDTO, Principal principal) {
		logger.info("Search Nearby Flyrr");
		User user = null;
		List<SearchFlyrrDetailsResponseDTO> response = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				userService.setUserLatLong(user, searchFlyrrDTO.getLattitude(), searchFlyrrDTO.getLongitude());
				response = this.userService.nearByFlyrr(searchFlyrrDTO, user);

				if (response.size() != 0) {
					return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
							this.userService.nearByFlyrr(searchFlyrrDTO, user));
				} else {
					return new ResponseDTO(Boolean.FALSE, Boolean.FALSE,
							new ErrorDto("Not Found FlyRRs", "50404", "Please Check Data Or Values"));
				}
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
	}

	@PostMapping(value = "/isNotification", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)

	public ResponseDTO notification(@Valid @RequestBody NotificationDTO notificationDTO, Principal principal,
			HttpServletRequest request) throws IOException {
		logger.info("Notification Process start");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				this.userService.notification(notificationDTO, user);
				if (notificationDTO.getIsNotification() == true) {
					return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
							new ResponseMessageDTO("Your Notification is enable"));
				} else {
					return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
							new ResponseMessageDTO("Your Notification is disable"));
				}
			} else {
				throw new AuthorizationException("Sessioon expired", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
	}

	@RequestMapping(value = "/interestedInBroadcast", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public ResponseDTO interestedInBroadcast(@RequestBody @Valid BroadcastMessageInterestedDTO intrestedInBroadcastDTO,
			Principal principal, HttpServletRequest request) {
		logger.info("interestedInBroadcast");
		User objUser = null;
		try {
			objUser = this.userService.getUserData(principal);

			if (objUser != null) {

				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						this.userService.interestedInBroadcast(objUser, intrestedInBroadcastDTO));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {

		}
	}

	@GetMapping(value = "/getOpponentUserFlyrrList", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO getOpponentUserFlyrrList(@RequestParam(name = "userId", required = true) Long userId,
			Principal principal, HttpServletRequest request) {
		logger.info("start the process for getOpponentUserProfile ....");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				HashMap<String, Object> response = new HashMap<>();
				response.put("OpponentUserList", this.userService.getOpponentUserFlyrrList(userId, request));
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						this.userService.getOpponentUserFlyrrList(userId, request));
			} else {
				return new ResponseDTO(Boolean.FALSE, Boolean.TRUE,
						new AuthorizationException("Session expired ", Boolean.TRUE));
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
		// return null;

	}

	@GetMapping(value = "/getRatingAndFeedbackList", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO getRatingAndFeedbackList(@RequestParam(name = "rateToUserId", required = true) Long rateToUserId,
			Principal principal, HttpServletRequest request) {
		logger.info("getRatingAndFeedbackList");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				HashMap<String, Object> response = new HashMap<>();
				response.put("RatingAndFeedbackList",
						this.userService.getRatingAndFeedbackList(rateToUserId, user, request));
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						this.userService.getRatingAndFeedbackList(rateToUserId, user, request));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}

	}

	@GetMapping(value = "/searchCategories", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO searchCategories(@RequestParam(name = "searchKeyword", required = true) String searchKeyword,
			Principal principal, HttpServletRequest request) {
		logger.info("Search Categories Process start");
		User user = null;
		try {
			user = this.userService.getUserData(principal);

			if (user != null) {
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						this.userService.searchCategories(searchKeyword, user, request));
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
	}

	@PostMapping(value = "/ratingAndFeedback", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO ratingAndFeedback(@RequestBody @Valid RatingFeedbackDTO ratingFeedbackDTO, Principal principal) {
		logger.info("Rating and Feedback Process start");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				this.userService.ratingAndFeedback(ratingFeedbackDTO, user);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						new ResponseMessageDTO("Rating and Feedback submitted sucessfully..!!"));
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}
		return null;
	}

	@PostMapping(value = "/reportFlyrr", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO reportFlyrr(@RequestBody @Valid ReportDTO reportDTO, Principal principal,
			HttpServletRequest request) {
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				this.userService.reportFlyrr(reportDTO, user, request);
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE, new ResponseMessageDTO(Constants.ReportMessage));
			}
		} finally {

		}
		return null;
	}

	@PostMapping(value = "/updateDeviceToken", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO deviceToken(@RequestBody DeviceTokenDTO deviceTokenDTO, Principal principal) {
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				Boolean res = this.userService.deviceToken(deviceTokenDTO, user);
				if (res) {
					return new ResponseDTO(Boolean.TRUE, Boolean.FALSE, new ResponseMessageDTO(Constants.DeviceToken));
				} else {
					return new ResponseDTO(Boolean.FALSE, Boolean.TRUE,
							new ResponseMessageDTO(Constants.DeviceTokenUpdate));

				}
			}
		} finally {

		}
		return null;
	}

	/*
	 * @PostMapping(value = "/sendDeviceNotifications", produces = {
	 * MediaType.APPLICATION_JSON_VALUE })
	 * 
	 * @ResponseStatus(value = HttpStatus.OK) public ResponseDTO
	 * sendDeviceNotification(@RequestBody NotificationUserDto dto, Principal
	 * principal) { User user = null; user =
	 * this.userService.getUserData(principal); if (user != null) {
	 * System.out.println("UserId--------->"+user.getId()); Boolean sendNotify =
	 * userService.sendUserNotification(dto, user); if (sendNotify == Boolean.TRUE)
	 * {
	 * 
	 * return new ResponseDTO(Boolean.TRUE, "Notification Send Successfully"); }
	 * else { return new ResponseDTO(Boolean.FALSE, "Notification Not Send..."); } }
	 * else { throw new AuthorizationException("Session expired ", Boolean.TRUE); }
	 * }
	 */

	/*
	 * @GetMapping(value="/nearbybrodcastId/{brodcastId}",produces = {
	 * MediaType.APPLICATION_JSON_VALUE }, consumes = {
	 * MediaType.APPLICATION_JSON_VALUE })
	 * 
	 * @ResponseStatus(value = HttpStatus.OK) public BroadcastMessage
	 * getBrodcastmessageId(@PathVariable("brodcastId") Long id, Principal
	 * principal) { User user = null; try { user =
	 * this.userService.getUserData(principal); if (user != null) {
	 * 
	 * BroadcastMessage broadcastMessageDetails = userService.findBrodcastById(id);
	 * if(broadcastMessageDetails!=null) { return broadcastMessageDetails; } else {
	 * throw new NotFoundFlyrrException(NotFound2.FlyRRNotFound, Boolean.TRUE); } }
	 * } finally {
	 * 
	 * } return null; }
	 */

	// return userService.findBrodcastById(dto.getBrodcastId());

	@PostMapping(value = "getInterestedUserDetails", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO getInterestedUserDetails(@RequestBody @Valid UserInterestedIdDTO userInterestedIdsDTO,
			Principal principal) throws Exception {
		logger.info("interestedUserDetail");
		User objUser = new User();
		objUser = this.userService.getUserData(principal);
		if (objUser != null) {
			userService.getInterestedUserDetails(userInterestedIdsDTO.getUserId(), objUser);

			return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
					this.userService.getInterestedUserDetails(userInterestedIdsDTO.getUserId(), objUser));

		} else {
			throw new AuthorizationException("Session expired ", Boolean.TRUE);
			/*
			 * return new ResponseDTO(Boolean.FALSE, Boolean.TRUE, new
			 * ErrorDto("Session Expired", "401", "Check User Token Expired Or Not"));
			 */
		}
	}

	@RequestMapping(value = "chatNotification", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO chatNotification(@RequestBody @Valid FcmUserDTO fcmUserDTO, Principal principal,
			HttpServletRequest request) throws Exception {
		logger.info("sendNotification");
		User objUser = null;
		try {
			objUser = this.userService.getUserData(principal);
			if (objUser != null) {
				// return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
				// this.notificationService.chatnotification(objUser, fcmUserDTO));

				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						this.notificationService.chatnotification(objUser, fcmUserDTO));

			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
				/*
				 * return new ResponseDTO(Boolean.FALSE, Boolean.TRUE, new
				 * ErrorDto("Session Expired", "401", "Check User Token Expired Or Not"));
				 */
			}

		} finally {
			if (objUser != null) {
				objUser = null;
			}
		}

	}

	@GetMapping(value = "/trandingCategories", produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO trandingCategories(Principal principal) {
		logger.info("getRatingAndFeedbackList");
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				HashMap<String, Object> response = new HashMap<>();
				response.put("RatingAndFeedbackList", this.userService.trandingCategories());
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE, this.userService.trandingCategories());
			} else {
				throw new AuthorizationException("Session expired ", Boolean.TRUE);
			}
		} finally {
			if (user != null) {
				user = null;
			}
		}

	}

	@PostMapping(value = "/userDetailsById", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {
			MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseDTO userDetailsById(@RequestBody @Valid UserDetailsByIdDTO userDetailsByIdDTO, Principal principal,
			HttpServletRequest request) {
		User user = null;
		try {
			user = this.userService.getUserData(principal);
			if (user != null) {
				return new ResponseDTO(Boolean.TRUE, Boolean.FALSE,
						this.userService.userDetailsByIdDTO(userDetailsByIdDTO));
			}
		} finally {

		}
		return null;
	}

}
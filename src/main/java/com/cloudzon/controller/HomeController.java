package com.cloudzon.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.Map;

@Controller
public class HomeController {
	@RequestMapping("/")
	public String index(Map<String, Object> Model) {
		Model.put("message", "how are you?");
		return "Index";
	}
}

/*
 * @RestController public class HomeController {
 * 
 * private static final Logger logger =
 * LoggerFactory.getLogger(HomeController.class);
 * 
 * 
 * @RequestMapping(value = "terms", method = RequestMethod.GET) public String
 * terms() { logger.info("Terms"); return "Terms"; }
 */

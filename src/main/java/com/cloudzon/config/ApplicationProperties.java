package com.cloudzon.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Properties specific to JHipster.
 * <p>
 * Properties are configured in the application.yml file.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Configuration
public class ApplicationProperties {

	public String xxmpHost;

	public String xxmpServiceName;

	public String xxmpPort;

	public String xxmpPassword;

	public static String apiUrl;

	public static String apiKey;

	public String type;

	public String password;

	public String cert;
	

	public ApplicationProperties() {
		super();
	}

	public String getCert() {
		return cert;
	}

	
	
	
	@Value("${apns.cert}")
	public void setCert(String cert) {
		this.cert = cert;
	}

	public String getXxmpHost() {
		return xxmpHost;
	}

	@Value("${xmpp.host}")
	public void setXxmpHost(String xxmpHost) {
		this.xxmpHost = xxmpHost;
	}

	public String getXxmpServiceName() {
		return xxmpServiceName;
	}

	@Value("${xmpp.serviceName}")
	public void setXxmpServiceName(String xxmpServiceName) {
		this.xxmpServiceName = xxmpServiceName;
	}

	public String getXxmpPort() {
		return xxmpPort;
	}

	@Value("${xmpp.port}")
	public void setXxmpPort(String xxmpPort) {
		this.xxmpPort = xxmpPort;
	}

	public String getXxmpPassword() {
		return xxmpPassword;
	}

	@Value("${xmpp.default.password}")
	public void setXxmpPassword(String xxmpPassword) {
		this.xxmpPassword = xxmpPassword;
	}

	public String getApiKey() {
		return apiKey;
	}

	@Value("${fcm.api.key}")
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	@Value("${fcm.api.url}")
	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getType() {
		return type;
	}

	@Value("${apns.type}")
	public void setType(String type) {
		this.type = type;
	}

	public String getPassword() {
		return password;
	}

	@Value("${apns.password}")
	public void setPassword(String password) {
		this.password = password;
	}

}

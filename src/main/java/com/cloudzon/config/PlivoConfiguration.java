/*
 * package com.cloudzon.config;
 * 
 * import org.springframework.beans.factory.annotation.Value; import
 * org.springframework.context.annotation.Configuration; import
 * org.springframework.context.annotation.PropertySource;
 * 
 * @Configuration
 * 
 * @PropertySource(value = { "classpath:plivo-app.properties" }) public class
 * PlivoConfiguration {
 * 
 * public static String AUTH_ID;
 * 
 * public static String AUTH_TOKEN;
 * 
 * public static String ACCOUNT_MOBILENUMBER;
 * 
 * public static String CALL_BASEURL;
 * 
 * public static String API_URL;
 * 
 * public static String API_KEY;
 * 
 * public static String ACCOUNT_SID;
 * 
 * public static String AUTH_TOKEN_TWILIO;
 * 
 * public static String ACCOUNT_MOBILENUMBER_TWILIO;
 * 
 * 
 * public PlivoConfiguration() { super(); }
 * 
 * public String getAUTH_ID() { return AUTH_ID; }
 * 
 * @Value("${plivo.auth.id}") public void setAUTH_ID(String aUTH_ID) {
 * PlivoConfiguration.AUTH_ID = aUTH_ID; }
 * 
 * public String getAUTH_TOKEN() { return AUTH_TOKEN; }
 * 
 * @Value("${plivo.auth.token}") public void setAUTH_TOKEN(String aUTH_TOKEN) {
 * PlivoConfiguration.AUTH_TOKEN = aUTH_TOKEN; }
 * 
 * public String getACCOUNT_MOBILENUMBER() { return ACCOUNT_MOBILENUMBER; }
 * 
 * @Value("${plivo.account.mobileNumber}") public void
 * setACCOUNT_MOBILENUMBER(String aCCOUNT_MOBILENUMBER) {
 * PlivoConfiguration.ACCOUNT_MOBILENUMBER = aCCOUNT_MOBILENUMBER; }
 * 
 * public String getCALL_BASEURL() { return CALL_BASEURL; }
 * 
 * @Value("${plivo.call.baseurl}") public void setCALL_BASEURL(String
 * cALL_BASEURL) { PlivoConfiguration.CALL_BASEURL = cALL_BASEURL; }
 * 
 * public static String getAPI_URL() { return API_URL; }
 * 
 * @Value("${fcm.api.url}") public static void setAPI_URL(String aPI_URL) {
 * PlivoConfiguration.API_URL = aPI_URL; }
 * 
 * public static String getAPI_KEY() { return API_KEY; }
 * 
 * @Value("${fcm.api.key}") public static void setAPI_KEY(String aPI_KEY) {
 * PlivoConfiguration.API_KEY = aPI_KEY; }
 * 
 * public String getACCOUNT_SID() { return ACCOUNT_SID; }
 * 
 * @Value("${twilio.account.sid}") public void setACCOUNT_SID(String
 * aCCOUNT_SID) { ACCOUNT_SID = aCCOUNT_SID; }
 * 
 * public String getAUTH_TOKEN_TWILIO() { return AUTH_TOKEN_TWILIO; }
 * 
 * @Value("${twilio.auth.token}") public void setAUTH_TOKEN_TWILIO(String
 * aUTH_TOKEN_TWILIO) { AUTH_TOKEN_TWILIO = aUTH_TOKEN_TWILIO; }
 * 
 * public String getACCOUNT_MOBILENUMBER_TWILIO() { return
 * ACCOUNT_MOBILENUMBER_TWILIO; }
 * 
 * @Value("${twilio.account.mobileNumber}") public void
 * setACCOUNT_MOBILENUMBER_TWILIO(String aCCOUNT_MOBILENUMBER_TWILIO) {
 * ACCOUNT_MOBILENUMBER_TWILIO = aCCOUNT_MOBILENUMBER_TWILIO; }
 * 
 * 
 * }
 */
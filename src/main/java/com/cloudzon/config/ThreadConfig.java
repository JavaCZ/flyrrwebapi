package com.cloudzon.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class ThreadConfig {

	@Bean(name = "specificTaskExecutor")
	public TaskExecutor specificTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.initialize();
		return executor;
	}

	/*
	 * @Bean public TaskExecutor threadPoolTaskExecutor() { ThreadPoolTaskExecutor
	 * executor = new ThreadPoolTaskExecutor(); executor.setCorePoolSize(4);
	 * executor.setMaxPoolSize(4); executor.setThreadNamePrefix("sgfgd");
	 * executor.initialize(); return executor; }
	 */

}
